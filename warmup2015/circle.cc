#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int main() {
  double r, x, y;
  double pi = acos(-1);

  while(cin>>r>>x>>y) {

    double a = sqrt(x*x+y*y);

    double b = sqrt(r*r-a*a);

    double angle = 2*asin(b/r);

    double segment = ((angle/(2*pi))*pi*r*r) - (b*a);

    if(a<=r)
      cout << fixed << setprecision(6) << max(segment, (pi*r*r-segment)) << " " << min(segment, (pi*r*r-segment)) << endl;
    else
      cout << "miss" << endl;
  //cout << b/r << endl;
  }


  return 0;
}