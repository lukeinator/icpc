#include <iostream>
#include <cmath>

using namespace std;

int main() {
	int cases;
	cin>>cases;

	while(cases--) {
		long long num;
		cin>>num;
		cout << ((1<<(num))-1) << endl;
	}


	return 0;
}