#include <iostream>
#include <cmath>

using namespace std;

int main() {
	double pi = acos(-1);

	double h, v;
	cin>>h>>v;

	double vRad = pi*v/180;

	double ans = h/sin(vRad);

	cout << ceil(ans) << endl;

	return 0;
}