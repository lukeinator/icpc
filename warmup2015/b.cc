#include <iostream>
#include <map>
#include <set>

using namespace std;

int main() {
	map<string, int> solved;

	map<string, int> probPenalty;

	int a;
	string b, c;

	while(cin>>a>>b>>c&&a!=-1) {
		if(solved.count(b)!=0)
			continue;

		if(c=="right") {
			probPenalty[b] += a;
			solved[b]=a;
		}

		else if(c=="wrong")
			probPenalty[b] += 20;

	}

	int penalty=0;

	for(auto el : probPenalty)
		if(solved.count(el.first)!=0)
			penalty+=el.second;

	cout << solved.size() << " " << penalty << endl; 


	return 0;
}