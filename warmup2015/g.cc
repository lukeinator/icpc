#include <iostream>

using namespace std;

int main() {
	int times[105];

	for(int i=0; i<105; i++)
		times[i] = 0;

	int a, b, c;

	cin>>a>>b>>c;

	for(int i=0; i<3; i++) {
		int st, en;
		cin>>st>>en;

		for(int j=st; j<en; j++)
			times[j]++;
	}

	int sum=0;

	for(int i=1; i<=100; i++) {
		if(times[i]==1)
			sum+=a;

		else if(times[i]==2)
			sum+=b*2;

		else if(times[i]==3)
			sum += c*3;
	}

	cout << sum << endl;

	return 0;
}