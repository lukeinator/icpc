/*
 * Dijkstra's Algorithm for sparse graphs
 *
 * Author: Howard Cheng
 *
 * Given a weight matrix representing a graph and a source vertex, this
 * algorithm computes the shortest distance, as well as path, to each
 * of the other vertices.  The paths are represented by an inverted list,
 * such that if v preceeds immediately before w in a path from the
 * source to vertex w, then the path P[w] is v.  The distances from
 * the source to v is given in D[v] (-1 if not connected).
 *
 * Call get_path to recover the path.
 *
 * Note: Dijkstra's algorithm only works if all weight edges are
 *       non-negative.
 *
 * This version works well if the graph is not dense.  The complexity
 * is O((n + m) log (n + m)) where n is the number of vertices and
 * m is the number of edges.
 *
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>
#include <queue>
#include <map>

using namespace std;


struct Edge {
  int to;
  int weight;       // can be double or other numeric type
  Edge(int t, int w)
    : to(t), weight(w) { }
};
  
typedef vector<Edge>::iterator EdgeIter;

struct Graph {
  vector<Edge> *nbr;
  int num_nodes;
  Graph(int n)
    : num_nodes(n)
  {
    nbr = new vector<Edge>[num_nodes];
  }

  ~Graph()
  {
    delete[] nbr;
  }

  // note: There is no check on duplicate edge, so it is possible to
  // add multiple edges between two vertices
  //
  // If this is an undirected graph, be sure to add an edge both
  // ways
  void add_edge(int u, int v, int weight)
  {
    nbr[u].push_back(Edge(v, weight));
  }
};

/* assume that D and P have been allocated */
void dijkstra(const Graph &G, int src, vector<int> &D, vector<int> &P)
{
  typedef pair<int,int> pii;

  int n = G.num_nodes;
  vector<bool> used(n, false);
  priority_queue<pii, vector<pii>,  greater<pii> > fringe;

  D.resize(n);
  P.resize(n);
  fill(D.begin(), D.end(), -1);
  fill(P.begin(), P.end(), -1);

  D[src] = 0;
  fringe.push(make_pair(D[src], src));

  while (!fringe.empty()) {
    pii next = fringe.top();
    fringe.pop();
    int u = next.second;
    if (used[u]) continue;
    used[u] = true;

    for (EdgeIter it = G.nbr[u].begin(); it != G.nbr[u].end(); ++it) {
      int v = it->to;
      int weight = it->weight + next.first;
      if (used[v]) continue;
      if (D[v] == -1 || weight < D[v]) {
	D[v] = weight;
	P[v] = u;
	fringe.push(make_pair(D[v], v));
      }
    }
  }
}

int get_path(int v, const vector<int> &P, vector<int> &path)
{
  path.clear();
  path.push_back(v);
  while (P[v] != -1) {
    v = P[v];
    path.push_back(v);
  }
  reverse(path.begin(), path.end());
  return path.size();
}



long long tsp(int pos, int mask, long long dists[15][15], int s) {
  long long best = 2000000000000000000;
  int bestInd;
  
  if(mask == (1 << s) -1)
    return dists[pos][0];

  for(int i=0; i<s; i++) {
    if(i == pos || (mask & (1 << i)))
      continue;

    long long dist = dists[pos][i] + tsp(i, mask | (1 << i), dists, s);

    //cout << pos << " " << dists[pos][i] + tsp(i, mask | (1 << i), dists, s) << endl;
    if(dist<best) {
      
      best = dist;
      bestInd = i;
    }
    

  }
  //cout << "pos: " << pos << " bestInd: " << bestInd << endl;
  return best;
}

int main(void)
{

  int cases;
  cin>>cases;

  while(cases--) {
    int n, m;
    cin >> n >> m;

    Graph G(n+100);
    int u, v, w;

    for(int i=0; i<m; i++) {
      cin >> u >> v >> w;
      G.add_edge(u, v, w);
      G.add_edge(v, u, w);
    }

    long long dists[15][15];

    int s;
    cin >> s;

    map<int, int> locs;

    s++;

    for(int i=1; i<s; i++) {
      int num;
      cin >> num;

      locs[i] = num;
    }



    for(int i=0; i<s; i++) {
      vector<int> D, P, path;
      dijkstra(G, locs[i], D, P);
      //cout << endl;
      for(int j=0; j<s; j++) {
        dists[i][j] = D[locs[j]];
        //cout << dists[i][j] << " ";
      }

    }

  cout << tsp(0, 0, dists, s) << endl;


  }
  return 0;
}
