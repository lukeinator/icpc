#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <stack>
#include <cassert>
using namespace std;

const int MAX_N = 1000;
const int MAX_DEG = 1000;

set<int> cameras;

struct Node {
  int deg;
  int nbrs[MAX_DEG];
  int dfs, back;
};

int dfn;

void clear_graph(Node G[], int n)
{
  int i;
  for (i = 0; i < n; i++) {
    G[i].deg = 0;
  }
}

void add_edge(Node G[], int u, int v)
{
  G[u].nbrs[G[u].deg++] = v;
  G[v].nbrs[G[v].deg++] = u;
}

void do_dfs(Node G[], int v, int pred, stack<int> &v_stack, 
	    stack<int> &w_stack)
{
  int i, w, child = 0;

  G[v].dfs = G[v].back = ++dfn;
  for (i = 0; i < G[v].deg; i++) {
    w = G[v].nbrs[i];
    if (G[w].dfs < G[v].dfs && w != pred) {
      /* back edge or unexamined forward edge */
      v_stack.push(v);
      w_stack.push(w);
    }
    if (!G[w].dfs) {
      do_dfs(G, w, v, v_stack, w_stack);
      child++;

      /* back up from recursion */
      if (G[w].back >= G[v].dfs) {
	/* new bicomponent */
	//cout << "edges in new biconnected component:" << endl;
	while (v_stack.top() != v || w_stack.top() != w) {
	  //cout << v_stack.top() << " " << w_stack.top() << endl;
	  v_stack.pop();
	  w_stack.pop();
	}
	//cout << v_stack.top() << " " << w_stack.top() << endl;
	v_stack.pop();
	w_stack.pop();

	if (pred != -1) {
	  cameras.insert(v);
	}
      } else {
	G[v].back = min(G[v].back, G[w].back);
      }
    } else {
      /* w has been examined already */
      G[v].back = min(G[v].back, G[w].dfs);
    }
  }
  if (pred == -1 && child > 1) {
    cameras.insert(v);
  }
}

void bicomp(Node G[], int n)
{
  int i;
  stack<int> v_stack, w_stack;

  dfn = 0;
  for (i = 0; i < n; i++) {
    G[i].dfs = 0;
  }
  //do_dfs(G, 0, -1, v_stack, w_stack);

  // NOTE: if you wish to process all connected components, you can simply
  // run the following code instead of the line above:
  //
   for (int i = 0; i < n; i++) {
     if (G[i].dfs == 0) {
       do_dfs(G, i, -1, v_stack, w_stack);
     }
   }
}

int main() {
	int n;

	int mapNum=1;

	while(cin>>n&&n!=0) {

		cameras.clear();

		if(mapNum!=1)
			cout << endl;

		map<string, int> nums;
		map<int, string> names;

		//map names to integers
		for(int i=0; i<n; i++) {
			string s;
			cin >> s;

			nums[s] = i;
			names[i] = s;
		}

		int m;
		cin>>m;


		Node G[MAX_N];
		  

		
		  clear_graph(G, n+5);
		  
		  for (int i = 0; i < m; i++) {
		  	string uStr, vStr;

		    cin >> uStr >> vStr;
		    add_edge(G, nums[uStr], nums[vStr]);
		    //add_edge(G, nums[vStr], nums[uStr]);
  			}
  			bicomp(G, n);

		

		cout << "City map #" << mapNum++ << ": " << cameras.size() <<  " camera(s) found" << endl;

		vector<string> nList;

		for(int i : cameras) {
			nList.push_back(names[i]);
		}

		sort(nList.begin(), nList.end());

		for(string s : nList)
			cout << s << endl;

	
	}


	return 0;
}
