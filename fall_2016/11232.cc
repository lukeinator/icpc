#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;

double PI = acos(-1.0);


double getVolume(double radius, double width, double height) {
   height -= radius;

   double a = 0;
   double b = 0;
   
   if(width>=radius*2*PI)
       a = height*radius*radius*PI;

   if(height>=radius*2*PI)
      b = width*radius*radius*PI;

   if(max(a, b)!=0.0)
      return max(a, b);
   
   return -1;
}

int main() {
   double width, height;
   
   while(cin >> width >> height) {
      if(width==0.0&&height==0.0)
	 break;

      double minRad = 0;
      double maxRad = width;

      double maxArea = 0;
      double middle = (maxRad + minRad)/2.0;

      while((maxRad-minRad)>0.0001) {
	 if(getVolume(middle, width, height)>maxArea) {
	    minRad = middle;
	    maxArea = getVolume(middle, width, height);
	 } else {
	    maxRad = middle;
	 }

	 middle = (maxRad + minRad)/2.0;
      }

      cout << getVolume(minRad, width, height) << endl;
   }

   return 0;
}
