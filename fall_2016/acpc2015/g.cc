#include <iostream>

using namespace std;

   int nums[2001][2001];

int main() {
   int cases;
   cin>>cases;

   while(cases--) {
      int n;
      cin>>n;
   


   for(int i=0; i<2001; i++)
      for(int j=0; j<2001; j++)
	 nums[i][j]=-1;

   for(int i=0; i<2001; i++) {
      nums[i][0]=0;
      nums[0][i]=0;
   }

   int best=0;
   
   for(int i=1; i<=n; i++) {
      for(int j=1; j<=n; j++) {
	 char ch;
	 cin >> ch;

	 if(ch=='F') {
	    nums[i][j]=0;
	    continue;
	 }

	 int num = min(min(nums[i-1][j-1], nums[i-1][j]), nums[i][j-1])+1;
	 nums[i][j] = num;
	 best = max(best, num);
      	    
	 
      }
   }

   cout << best << endl;

   
   }

   return 0;
}
