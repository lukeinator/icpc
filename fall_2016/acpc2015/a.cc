#include <iostream>
using namespace std;

int main() {

	int cases;
	cin >> cases;

	while(cases--) {
		long long n;

		cin >> n;

		long long i=1;
		long long i2=0;

		long long sum=0;
		long long sum2=0;

		while(sum<n) {
			sum += i*(i+1)/2;
			i++;
		}
		i--;
		
		while(sum2<n) {
			sum2 += i*(i+1)/2;
			i--;
			i2++;
		}

		cout << i2 << endl;

	}

	return 0;
}