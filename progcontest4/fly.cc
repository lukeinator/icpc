#include <iostream>
#include <iomanip>
using namespace std;

int main() {
   int cases;
   cin >> cases;

   for(int i=0; i<cases; i++) {

      int cNum;
      cin >> cNum;
      
   double d, v1, v2, vf, t, df;

   cin >> d >> v1 >> v2 >> vf;

   t = d/(v1+v2);

   df = vf*t;

   cout << cNum << " " << fixed << setprecision(2) << df << endl;

   }
   return 0;
}
