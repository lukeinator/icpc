#include <iostream>
#include <map>
#include <list>
#include <vector>
#include <set>
using namespace std;

int order(map<int, list<int> >& m, int a) {
   if(m[a].size()==0)
      return 1;

   int max=-1;
   int countmax=0;
   
   for(int k : m[a]) {
      int test;
      test = order(m, k);

      if(test>max) {
	 max = test;
	 countmax=0;
      }
      else if(test==max) 
	 countmax++;
      
   }

   if(countmax>=1)
      return max+1;

   else
      return max;
}


int main() {
   int cases;
   cin >> cases;

   for(int a=0; a<cases; a++) {

   map<int, list<int> > data;
   
   set<int> p;
   set<int> c;

   int dNum, m, q;
   cin >> dNum >> m >> q;

   for(int i=0; i<q; i++) {
      int par, chi;

      cin >> par >> chi;

      data[chi].push_back(par);

      p.insert(par);
      c.insert(chi);

   }

   int start;

   for(int j : c) {
      if(p.count(j)==0) {
	 start = j;
      }
   }

   cout << dNum << " " << order(data, start) << endl;
   }
   return 0;
}
