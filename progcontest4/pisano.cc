#include <iostream>
using namespace std;

int main() {
   int cases;
   cin >> cases;
   
   for(int i=0; i<cases; i++) {
      int count = 0;
      int m;
      int dNum;
      cin >> dNum >> m;
      int a=1, b=1;
     
       do {
	  int c;
	  c=(a+b)%m;
	  a=b%m;
	  b=c%m;
	  count++;
       } while(!(a==1&&b==1));
       cout << dNum << " " << count << endl; 
   }


   return 0;
}
