#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <cmath>
#include <iomanip>

using namespace std;

int main() {
	string s;

	int mapNum=1;

	while(getline(cin, s)&&s!="END") {

		double x=0, y=0;

		replace(s.begin(), s.end(), ',', ' ');
		replace(s.begin(), s.end(), '.', ' ');

		

		istringstream iss(s);

		

		int num;

		while(iss>>num) {
			string dir;
			iss>>dir;

			double delta = (sqrt(2.0)*num)/2.0;

			if(dir=="N")
				y+=num;
			else if(dir=="S")
				y-=num;
			else if(dir=="W")
				x-=num;
			else if(dir=="E")
				x+=num;
			else if(dir=="NW") {
				y+=delta;
				x-=delta;
			} else if(dir=="NE") {
				y+=delta;
				x+=delta;
			} else if(dir=="SW") {
				y-=delta;
				x-=delta;
			} else if(dir=="SE") {
				y-=delta;
				x+=delta;
			}


		}


			cout << "Map #" << mapNum++ << endl;
			cout << fixed << setprecision(3) << "The treasure is located at (" << x << "," << y << ")." << endl;
			cout << "The distance to the treasure is " << sqrt(x*x+y*y) << "." << endl;

			cout << endl;

	}


	return 0;
}