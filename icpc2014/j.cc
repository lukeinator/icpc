#include <iostream>
#include <sstream>
#include <cassert>
using namespace std;

bool test(int rule, string test) {


   for(int i=0; i<test.length(); i++) {
      
      bool neg=false;
      if(test[i]=='~') {
	 neg=true;
	 i++;
      }
      i++;

      if(!neg&&(rule&(1<<()))
	 return true;

      if(neg&&!(rule&(1<<(test[i]-'0'-1))))
	 return true;

      i+=3;
   }

   return false;
}

int main() {

    int cases;
   cin>>cases;
   while(cases--) {
      int n, m;
      cin>>n>>m;

      string clauses[105];
      getline(cin, clauses[0]);

      for(int i=0; i<m; i++)
	 getline(cin, clauses[i]);

      bool sat=false;
      for(int i=0; i<(1<<n)&&!sat; i++) {
	 bool inner=true;
	 for(int j=0; j<m; j++) {
	    if(!test(i, clauses[j])) {
	       inner=false;
	       break;
	    }
	 }
	    if(inner)
	       sat=true;
      }
	 if(sat)
	    cout << "satisfiable" << endl;
	 else
	    cout << "unsatisfiable" << endl;
	    }
   return 0;
}
