#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <map>
using namespace std;

int w;
typedef pair<int, int> pii;
map<pii, int> seen;

int calc(int seats, int tickets[55][105][2], int week) {
   if(seen.count(pii(seats, week))!=0)
      return seen[pii(seats, week)];

   if(seats==0)
      return  seen[pii(seats, week)] = 0;

   if(week==w)
      return seen[pii(seats, week)] = 0;

   int best=0;
   int bp=10000;
   for(int i=1; i<=tickets[week][0][0]; i++) {
      int seatsLeft = min(seats, tickets[week][i][1]);

      int buy = tickets[week][i][0]*seatsLeft + calc(seats-seatsLeft, tickets, week+1);

      if(best!=max(best, buy)) {
	 bp=i;
	
      }
	 
      best = max(best, buy);

	 }

   if(week==0)
      cout << best << endl << tickets[0][bp][0] << endl;
   
   return  seen[pii(seats, week)] = best;
}


int main() {
   int n;
   while(cin>>n>>w&&n!=0) {
      seen.clear();
      
   int prices[55][105][2];
   w++;

   for(int i=0; i<w; i++) {
      int p;
      cin>>p;

      prices[i][0][0]=p;
      
      for(int j=1; j<p+1; j++)
	 cin>>prices[i][j][0];

      for(int j=1; j<p+1; j++)
	 cin>>prices[i][j][1];
   }

    calc(n, prices, 0);

   }
   return 0;
}
