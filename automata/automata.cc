#include <iostream>
using namespace std;

int getRule(string s, int pos) {
	int num=0;

	if(s[(pos-1+s.length())%s.length()]=='#')
		num |= 4;

	if(s[pos]=='#')
		num |= 2;

	if(s[(pos+1)%s.length()]=='#')
		num |= 1;

	return num;
}

string nextGen(string s, int rule) {
	string next = s;

	for(int i=0; i<s.size(); i++) {
		if(rule&1<<getRule(s, i))
			next[i] = '#';	
		else
			next[i] = '-';
	}
	return next;
}

int main() {
	int r, n, l;
	string s;

	cin >> r >> n >> l >> s;
	cout << s << endl;

	while(n--) {
		s = nextGen(s, r);
		cout << s << endl;
	}
	return 0;
}