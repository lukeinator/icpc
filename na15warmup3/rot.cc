#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

string key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.";

int main() {
   int shift;

   while(cin>>shift&&shift!=0) {
      string str;
      cin>>str;

      reverse(str.begin(), str.end());
      string rot = str;
      
      for(int i=0; i<str.length(); i++) {
	 rot[i] = key[(key.find(str[i])+shift)%key.length()];
      }

      cout << rot << endl;

     
   }
   return 0;
}
