#include<iostream>
#include<utility>
#include<queue>
#include<algorithm>
#include<cstdlib>
using namespace std;
int d1[4] = {0, 1, 0, -1};
int d2[4] = {1, 0, -1, 0};
int main(){
   int X,Y,T,L,W;
   while(cin >> X >> Y >> T >> L >> W and X != -1){
      queue< pair<int,int> > Q;
      vector<vector<int> > V;
      V.resize(X+1);
      for(int i = 1; i <= X; i++){
         V[i].resize(Y+1);
      }
      for(int i = 1; i <= X; i++){
         for(int j = 1; j <= Y; j++){
            V[i][j] = 0;
         }
      }
      for(int i = 0; i < L; i++){
         int x,y;
         cin >> x >> y;
         Q.push(make_pair(x,y));
         V[x][y] = 1;
      }
      for(int k = 0; k < W; k++){
         int x0,y0,x1,y1;
         cin >> x0 >> y0 >> x1 >> y1;
         if(x0 == x1 or y1 == y0){
            for(int i = min(x0,x1); i <= max(x0,x1); i++){
               for(int j = min(y0,y1); j <= max(y0,y1); j++){
                  V[i][j] = -1;
               }
            }
         }
         else{
            int x=1,y=1;
            if(x0 - x1 > 0)
               x=-1;
            if(y0 - y1 > 0)
               y=-1;
            for(int i = 0; i <= abs(x0-x1); i++){
               V[x0+(i*x)][y0+(i*y)] = -1;
            }
         }
      }
      while(!Q.empty()){
         pair<int,int> P = Q.front();
         Q.pop();
         for(int i = 0; i < 4; i++){
            pair<int,int> N = make_pair(P.first + d1[i], P.second + d2[i]);
            if(N.first > X or N.first < 1 or N.second < 1 or Y < N.second){
               continue;
            }
            if(V[N.first][N.second] != 0)
               continue;
            V[N.first][N.second] = V[P.first][P.second] + 1;
            Q.push(N);
         }
      }
      int count = 0;
      for(int i = 1; i <= X; i++){
         for(int j = 1; j <= Y; j++){
            if(V[i][j] <= T and V[i][j] != 0 and V[i][j] != -1)
               count++;
         }
      }
      cout << count << endl;
   }
   return 0;
}
