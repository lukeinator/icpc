#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

vector<string> getCell(int a, int b) {
   vector<string> cell;

   cell.push_back("+---+");
   cell.push_back("|"+ to_string(((a*b)/10)) + " /|");
   cell.push_back("| / |");
   cell.push_back("|/ "+ to_string(((a*b)%10)) + "|");
   cell.push_back("+---+");
   return cell;
}

vector<string> mergeCell(vector<string> a, vector<string> b) {
   vector<string> merged;

   for(int i=0; i<a.size(); i++) {
      if(a.size()==b.size())
	 merged.push_back(a[i].substr(0, a[i].length()-1)+b[i]);
      else
	 merged.push_back(b[i]);
   }

   return merged;
}

    vector<string>  getGrid(string a, string b) {
    vector< vector<string> > grid;

    for(int j=0; j<b.length(); j++) {
       vector<string> nv;
       grid.push_back(nv);
    }

    for(int j=0; j<b.length(); j++)
       grid[j]=getCell(a[0]-'0', b[j]-'0');
    
  for(int j=0; j<b.length(); j++)
   for(int i=1; i<a.length(); i++) 
      grid[j] = mergeCell(grid[j], getCell(a[i]-'0', b[j]-'0'));
	     
  vector<string> lingrid;
  for(int j=0;j<b.length(); j++) {
     bool first=true;
     for(string s : grid[j]) {
	if(j!=0&&first) {
	   first=false;
	   continue;
	}
	
	lingrid.push_back(s);
     }
  }

  return lingrid;
    }

vector<string> getTop(string a) {
   string top = "+---";
   for(int i=0; i<a.length(); i++)
      top += "----";

   top += "+";

   string next =  "|   ";

   for(int i=0; i<a.length(); i++)
      next += to_string(a[i]-'0') + "   ";
   next += "|";
   vector<string> rtn;

   rtn.push_back(top);
   rtn.push_back(next);

   return rtn;
}

vector<string> getBottom(string a, string b) {
   int anum = stoi(a);
   int bnum = stoi(b);

   string top = "+---";
   for(int i=0; i<a.length(); i++)
      top += "----";

   top += "+";

   int calc = anum*bnum;

   string s = to_string(calc);
   int carry = s.length()-a.length();
   s = s.substr(s.length()-a.length());

   string next = "|";
   for(char ch : s)
      if(carry!=0)
	 next += "/ "+to_string(ch-'0')+" ";
      else
	 next += "  "+to_string(ch-'0')+" ";
   next += "   |";

   vector<string> rtn;
   rtn.push_back(next);
   rtn.push_back(top);

   return rtn;
}

vector<string> getBox(string a, string b) {
    int anum = stoi(a);
   int bnum = stoi(b);
   vector<string> rtn;

   for(string s : getTop(a))
      rtn.push_back(s);

   //for(string s : getGrid(a, b))
   // rtn.push_back("| "+s+" |");

   vector<string> grid = getGrid(a, b);
   int doneS;

   for(int i=0; i<b.length(); i++) {
      grid[2+i*4] = "| " + grid[2+i*4]+ b[i] + "|";
      doneS = grid[2+i*4].length();
   }

   int calc = anum*bnum;

   string s = to_string(calc);

   s = s.substr(0, s.length()-a.length());

   while(s.length()!=b.length())
      s = " " + s;

   int numSpaces=1;

   for(char ch : s)
      if(ch==' ')
	 numSpaces++;
  
   for(int i=0; i<b.length(); i++) {
      grid[3+i*4] = "|" + (s[i]-'0'>=0 ? to_string(s[i]-'0') : " ") + grid[3+i*4] + " |";
      doneS = grid[3+i*4].length();
    }

   for(int i=0; i<grid.size(); i++)
      if(grid[i].length()!=doneS)
	 if(i%4==1&&i!=1&&(--numSpaces)<=0)
	    grid[i] = "|/" + grid[i] + " |";
	 else
	    grid[i] = "| " + grid[i] + " |";

   for(string s : grid)
      rtn.push_back(s);
   
   for(string s : getBottom(a, b))
      rtn.push_back(s);

   return rtn;
      
}


int main() {
   string a, b;
   vector<string> test;
   while(cin>>a>>b&&!(a=="0"&&b=="0")) {
      for(string s : getBox(a, b))
	 cout << s << endl;
   }

   



   return 0;
}
