#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

long long Choose[20][20];

long long numpos(vector<int> B){
   long long ret = 1;
   int n = 0;
   for(int i = 0; i < B.size(); i++){
      n+= B[i];
   }
   for(int i = 0; i < B.size(); i++){
      ret *= Choose[n][B[i]];
      n-=B[i];
   }
   return ret;
}

int main(){
   string s;
   long long K;
   for(int i = 0; i < 20; i++){
      for(int j = 0; j <= i; j++){
         if(i == 0 or i == j)
            Choose[i][j] = 1;
         else
            Choose[i][j] = Choose[i-1][j-1] + Choose[i-1][j];
      }
   }
   while(cin >> s >> K && K){
      int count[30];
      fill(count,count+30,0);
      for(int i = 0; i < s.size(); i++){
         count[s[i]-'A']++;
      }
      vector<int> B;
      string sorted;
      for(int i = 0; i < 30; i++){
         if(count[i] != 0){
            B.push_back(count[i]);
            for(int j = 0; j < count[i]; j++)
               sorted += char(i + 'A');
         }
      }
      string ret;
      unsigned long long sum = 0;
      for(int pos = 0; pos < s.size(); pos++){
         int n = 0;
         for(int i = 0; i < B.size(); i++){
            if(B[i] != 0){
               vector<int> temp = B;
               temp[i]--;
               sum += numpos(temp);
               if(K <= sum){
                  sum -= numpos(temp);
                  ret += sorted[n];
                  sorted.erase(n,1);
                  B = temp;
                  break;
               }
            }
            n+= B[i];
         }
      }
      cout << ret << endl;
   }
   return 0;
}
