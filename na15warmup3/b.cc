#include<iostream>

using namespace std;

int main(){
   int W, H;
   int c = 1;
   while(cin >> W >> H && (W || H)){
      char CH[25][25];
      int x, y;
      for(int i = 0; i < H; i++){
         for(int j = 0; j < W; j++){
            cin >> CH[i][j];
            if(CH[i][j] == '*'){
               x = i;
               y = j;
            }
         }
      }
      int dirlr;
      int dirud;
      if(x == 0){
         dirlr = 0;
         dirud = 1;
      }
      else if(y == 0){
         dirlr = 1;
         dirud = 0;
      }
      else if(x == H-1){
         dirlr = 0;
         dirud = -1;
      }
      else if(y == W-1){
         dirlr = -1;
         dirud = 0;
      }
      while(CH[x][y] != 'x'){
         x += dirud;
         y += dirlr;
         if((CH[x][y] == '/' and dirlr == 1) or
            (CH[x][y] == '\\' and dirlr == -1)){
            dirlr = 0;
            dirud = -1;
         }
         else if((CH[x][y] == '/' and dirud == -1) or
                 (CH[x][y] == '\\' and dirud == 1)){
            dirlr = 1;
            dirud = 0;
         }
         else if((CH[x][y] == '/' and dirud == 1) or
                 (CH[x][y] == '\\' and dirud == -1)){
            dirlr = -1;
            dirud = 0;
         }
         else if((CH[x][y] == '/' and dirlr == -1) or
                 (CH[x][y] == '\\' and dirlr == 1)){
            dirlr = 0;
            dirud = 1;
         }
      }
      CH[x][y] = '&';
      cout << "HOUSE " << c++ << endl;
      for(int i = 0; i < H; i++){
         for(int j = 0; j < W; j++){
            cout << CH[i][j];
         }
         cout << endl;
      }
   }
   return 0;
}
