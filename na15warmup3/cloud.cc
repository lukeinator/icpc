#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <cmath>
using namespace std;

int main() {
   int w, n;
   int cloud = 1;
   while(cin>>w>>n&&n!=0) {
      map<string, int> words;
      int maxp=0;

      for(int i=0; i<n; i++) {
	 string s;
	 int num;
	 cin>>s>>num;

	 if(num>=5)
	    words[s] = num;

	 maxp = max(maxp, num);
      }

      map<string, int> heights;
      map<string, int> widths;

      
      for(auto el : words) {
	 heights[el.first] = 8 + ceil((40.0*(el.second-4.0))/(maxp-4.0));
	 widths[el.first] = ceil((9.0/16.0)*el.first.length()*heights[el.first]);
      }
      // cout << maxp << endl;
      int height=0;
      int rowW=0;
      int rowH=0;
      for(auto el : words) {

	
	 if(rowW+widths[el.first]>w) {
	   
	    height += rowH;
	    // cout << "too big " << height  << endl;
	    rowW = widths[el.first]+10;
	    rowH = heights[el.first];
	 } else {
	    rowH = max(heights[el.first], rowH);
	    rowW += widths[el.first]+10;
	 }

	 // cout << el.first << " " << el.second << " " << heights[el.first] << " " << widths[el.first] << endl;

      }

      height += rowH;
      

      cout << "CLOUD " << cloud++ << ": " << height << endl;


   }
   return 0;
}
