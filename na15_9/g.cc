#include <iostream>
#include <string>
#include <set>
#include <algorithm>
using namespace std;

set<string> dict;

set<string> added;
bool addedStr=false;
int best=100000000;


int calc(string s, string dest, bool switched, string add, int dist) {
   if(s==dest) {
      if(dist<best)
      {
	 added.clear();
	 best=dist;
	 addedStr=switched;
	 added.insert(add);
      }

      if(switched)
	 added.insert(add);
      return dist;
   }

   int b=100000000;

   for(int i=0; i<s.length(); i++) {
      string str=s;

      if(str[i]==dest[i])
	 continue;

      str[i]=dest[i];

      if(dict.count(str)!=0)
	 b = min(b, calc(str, dest, switched, add, dist+1));

      else if(!switched)
	 b = min(b, calc(str, dest, true, str, dist+1));
   }
   return b;
}

int main() {
   int n;
   cin>>n;

   string start, end;

   cin>>start>>end;
   n-=2;

   dict.insert(start);
   dict.insert(end);

   while(n--) {
      string s;
      cin>>s;
      dict.insert(s);
   }

   int dist = calc(start, end, false, "", 0);
   int sdist = calc(start, end, true, "", 0);

   
   if(!addedStr||dist==sdist)
      cout << '0' << endl;
   else
      cout << (*added.begin()) << endl;

   if(dist!=100000000)
   cout << dist << endl;
   else
      cout << "-1" << endl;

   return 0;
}
