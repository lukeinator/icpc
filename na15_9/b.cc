#include <iostream>
using namespace std;

const double EPS = 1e-8;
bool dEqual(double x,double y) { return fabs(x-y) < EPS; }
struct Point {
double x, y;
bool operator==(const Point &p) const { return dEqual(x, p.x) && dEqual(y, p.y); }
bool operator<(const Point &p) const { return y < p.y || (y == p.y && x < p.x); }
};
Point operator-(Point p,Point q){ p.x -= q.x; p.y -= q.y; return p; }
Point operator+(Point p,Point q){ p.x += q.x; p.y += q.y; return p; }
Point operator*(double r,Point p){ p.x *= r; p.y *= r; return p; }
double operator*(Point p,Point q){ return p.x*q.x + p.y*q.y; }
double len(Point p){ return sqrt(p*p); }
double cross(Point p,Point q){ return p.x*q.y - q.x*p.y; }
Point inv(Point p){ Point q = {-p.y,p.x}; return q; }

#define SQR(X) ((X) * (X))
struct Circle{ Point c; double r; };
int intersect_circle_circle(Circle c1,Circle c2,Point& ans1,Point& ans2) {
if(c1.c == c2.c && dEqual(c1.r,c2.r)) return 3;
double d = len(c1.c-c2.c);
if(d > c1.r + c2.r + EPS || d < fabs(c1.r-c2.r) - EPS) return 0;
double a = (SQR(c1.r) - SQR(c2.r) + SQR(d)) / (2*d);
double h = sqrt(abs(SQR(c1.r) - SQR(a)));
Point P = c1.c + a/d*(c2.c-c1.c);
ans1 = P + h/d*inv(c2.c-c1.c); ans2 = P - h/d*inv(c2.c-c1.c);
return dEqual(h,0) ? 1 : 2;
}

int main() {



   return 0;
}
