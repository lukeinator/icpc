#include <iostream>
#include <cmath>
#include <set>
using namespace std;

int main() {
   int num;
   cin>>num;

   int nums[1000005];

   for(int i=0; i<num; i++)
      cin>>nums[i];

   set<int> stacks;

   stacks.insert(nums[0]);

   for(int i=1; i<num; i++) {
      if(nums[i]==1) {
	 stacks.insert(nums[i]);
	 continue;
      }

      if(stacks.count(nums[i]-1)!=0) {
	    stacks.erase(nums[i]-1);
      }

      stacks.insert(nums[i]);
   }

   int count=1;
   int two=1;
   while(two<stacks.size()) {
      count++;
      two*=2;
   }
   
   cout << count-1 << endl;


   return 0;
}
