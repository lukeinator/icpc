#include <iostream>
#include <map>
#include <tuple>
using namespace std;

typedef tuple<int, int, int, int> sTuple;

map<sTuple, long long> seen;

int A[85] = {0};
int n=0;

long long calc(int a, bool b, bool c, int d) {
   if(seen.count(sTuple(a, b, c, d))!=0)
      return seen[sTuple(a, b, c, d)];
   if(a==n)
      return 1;
   
   long long temp=0;
   for(int digit=0; digit<10; digit++) {
      if(b&&digit>A[a]) continue;

      if(digit==d) {
	 temp += calc(a+1, (b&&digit==A[a]), false, digit);

      } else if(digit>d) {
	    temp += calc(a+1, (b&&digit==A[a]), false, digit);
      } 
      // cout << "digit = " << digit << endl;
      // cout << "temp = " << temp << endl;
   }
   return seen[sTuple(a, b, c, d)] = temp;
}

int main() {
   int cases;
   cin>>cases;

   while(cases--) {
   n=0;
   string number;
   cin>>number;

   for(char ch : number)
      A[n++] = ch-'0';

   bool isHill=true;
   for(int i=0; i<n-1; i++) {
      if(A[i+1]<A[i])
	 isHill=false;
   }

   if(!isHill)
      cout << "-1" << endl;
   else
      cout << calc(0, true, false, 0)-1 << endl;
   }
   return 0;
}
