#include <iostream>
#include <map>
#include <tuple>
#include <algorithm>
using namespace std;

typedef tuple<int, int, int> tii;

map<tii, int> seen;

int n;

int calc(int pos, double data[205][2], double w, double c) {
   if(pos==n)
      return 0;

   if(seen.count(tii(pos, w, c))!=0)
      return seen[tii(pos, w, c)];


   if(data[pos][0]<=w||data[pos][1]>=c)
      return seen[tii(pos, w, c)] = calc(pos+1, data, w, c);

   return seen[tii(pos, w, c)] =
      max(1+calc(pos+1, data, data[pos][0], data[pos][1]),calc(pos+1, data, w, c));


}

int main() {
   int cases;
   cin>>cases;

   while(cases--) {
      seen.clear();
      
      cin >> n;
      double data[205][2];

      for(int i=0; i<n; i++)
	 cin>>data[i][0] >> data[i][1];

      cout << calc(0, data, -1.0, 11.0) << endl;

   }
   return 0;
}
