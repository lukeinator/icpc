#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
using namespace std;

int main() {
   int cases;
   cin>>cases;

   string s;
   getline(cin, s);
   while(cases--) {
      getline(cin, s);

      bool found=false;
      for(int i=0; i<10; i++) {
	 string test = s;
	 char inum = '0'+i;

	 if(test.find(inum)!=string::npos)
	    continue;
	 
	 replace(test.begin(), test.end(), '?', inum);

	 istringstream iss(test);
	 istringstream tester(test);

	 if(i==0) {
	    char next;
	    tester>>next;

	    if(next=='-')
	       tester >> next;
	    
	    int atest=0;
	    tester>>atest;
	    //  cout << "next: " << next << " atest: " << atest << endl;
	    if(atest!=0&&next=='0')
	       continue;

	    char op;
	    tester>>op;
	    tester>>next;

	    if(next=='-')
	       tester >> next;
	    
	    int btest=0;
	    tester >> btest;

	    if(btest!=0&&next=='0')
	       continue;

	    char eq;
	    int ctest=0;
	    tester >> eq >> next;
	    if(next=='-')
	       tester >> next;

	    tester >> ctest;

	    if(next=='0'&&ctest!=0)
	       continue;
	 }



	 char op, ch;
	 long long a=0, b=0, c=0;

	 iss>>a >> op >> b >> ch >> c;
	 //cout << a << " " << op << " " << b << " " << c << endl;

	 if(op=='+'&&a+b==c) {
	    found=true;
	    cout << i << endl;
	    break;
	 }

	 if(op=='-'&&a-b==c) {
	    found=true;
	    cout << i << endl;
	    break;
	 }

	 if(op=='*'&&a*b==c) {
	    found=true;
	    cout << i << endl;
	    break;
	 }

      }

      if(!found)
	 cout << "-1" << endl;

   }
   return 0;
}
