#include <iostream>
using namespace std;

int main() {

int s[4] = {1, 2, 3, 4};

int n=4;

for (int i = 0; i < (1 << n); i++) {
	cout << "{ ";
	for (int j = 0; j < n; j++) {
		if((i & (1 << j)) != 0)
			cout << s[j] << ", ";

	}
	cout << "} " << endl;
}

	return 0;
}