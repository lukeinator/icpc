#include <iostream>
#include <string>
#include <map>
#include <algorithm>
using namespace std;

map<string, int> seen;

int calculate(string maze, int x, int y, int moves, int xlen, int ylen) {
	if(seen.count(maze)!=0)
		return seen[maze];

	int pos = xlen*y + x;

//left check
	for(int i=pos; pos>=xlen*(y); i--) {
		if(maze.at(i)=='#')
			break;
		if(maze.at(i)=='>') {
			seen[maze] = 10000;
			return 10000;
		}
	}

//right check 
/*	for(int i=pos; pos<((pos/xlen)+1)*xlen; i++) {
		if(maze.at(i)=='#')
			break;
		if(maze.at(i)=='<') {
			seen[maze] = 10000;
			return 10000;
		}
	}
*/

//up check
	for(int i=pos; pos>=0; i-=xlen) {
		if(i<0)
			break;
		if(maze.at(i)=='#')
			break;
		if(maze.at(i)=='v') {
			seen[maze] = 10000;
			return 10000;
		}
	}

//down check
	for(int i=pos; pos<maze.length(); i+=xlen) {
		if(i>=maze.length())
			break;
		if(maze.at(i)=='#')
			break;
		if(maze.at(i)=='^') {
			seen[maze] = 10000;
			return 10000;
		}
	}

	if(maze[pos]=='G') {
	seen[maze] = moves;
	return moves;
	}

	replace(maze.begin(), maze.end(), '^', '>');
	replace(maze.begin(), maze.end(), '>', 'v');
	replace(maze.begin(), maze.end(), 'v', '<');
	replace(maze.begin(), maze.end(), '<', '^');


	int u, d, l, r;
	//up
	if(pos-xlen>=0&&maze.at(pos-xlen)=='.')
		u = calculate(maze, x, y-1, moves+1, xlen, ylen);

	//down
	if(pos+xlen<maze.size()&&maze.at(pos+xlen)=='.')
		d = calculate(maze, x, y+1, moves+1, xlen, ylen);

	//left
	if(pos%xlen>0&&maze.at(pos-1)=='.')
		l = calculate(maze, x-1, y, moves+1, xlen, ylen);

	//left
	if((pos+1)%xlen!=0&&(pos+1)<maze.length()&&maze.at(pos+1)=='.')
		r = calculate(maze, x+1, y, moves+1, xlen, ylen);

	int sol;
	sol = min(r, min(l, min(u,d)));
	seen[maze] = sol;
	return sol;
}


int main() {

	int cases;
	cin >> cases;

	for(int a=0; a<cases; a++) {

		int xlen, ylen, length;

		cin >> ylen >> xlen;

		length = xlen*ylen;

		string maze;

		for(int i=0; i<length; i++) {
			char ch;
			cin >> ch;
			maze += ch;
		}
		cout << maze;
		int x, y, pos;

		for(int i=0; i< maze.length(); i++)
			if(maze.at(i)=='S')
				pos=i;

		replace(maze.begin(), maze.end(), 'S', '.');

		x = pos%xlen;
		y = pos/xlen;

		cout << "Case #" << a+1 << ": ";

		int b = calculate(maze, x, y, 0, xlen, ylen);

		if(b==10000)
			cout << "impossible" << endl;
		else 
			cout  << b << endl;


	} 



	return 0;
}