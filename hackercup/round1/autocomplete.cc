#include <iostream>
#include <set>
#include <string>

using namespace std;


int main() {

	int cases;
	cin >> cases;

	for(int a=0; a<cases; a++) {

	int b;
	cin >> b;

	set<string> pre;

	int ans=0;

	for(int i=0; i<b; i++) {
		string s;

		cin >> s;

		string t;
		bool found = false;
		for(int j=0; j<s.length(); j++) {
			t+=s[j];
			if(pre.count(t)==0&&found==false) {
				ans+=j+1;
				found=true;
			}
			pre.insert(t);
		}
		if(found==false)
			ans+=s.length();
	}


	cout << "Case #" << a+1 << ": " << ans << endl;

	}


	return 0;
}