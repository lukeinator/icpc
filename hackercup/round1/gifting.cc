#include <iostream>
#include <map>
#include <algorithm>


using namespace std;

int main() {

int cases;

cin >> cases;

for(int a=0; a<cases; a++) {

map<int, int> man1;
map<int, int> man2;

int e;
cin >> e;

int throwaway;

cin >> throwaway;

man1[1] = 0;
man2[1] = 1;

for(int i=2; i<=e; i++) {
int test;
cin >> test;
man1[i] = (man1[test] + 1)%2;
man2[i] = (man2[test] + 1)%2;
}

int min1, min2;

min1 = min2 = 0;

for(int i=1; i<=e; i++) {
	min1+=(man1[i]+1);
}

for(int i=1; i<=e; i++) {
	min2+=(man2[i]+1);
}

int minimum = min(min1, min2);
 
cout << "Case #" << a+1 << ": " << minimum << endl;

}
	return 0;

}