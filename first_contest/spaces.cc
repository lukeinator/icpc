#include <iostream>
#include <vector>
#include <algorithm>


using namespace std;

int main() {
   int max;
   int num;
   vector<int> partition;
   vector<int> answer;
   
   cin >> max >> num;
   partition.push_back(max);
   partition.push_back(0);
   answer.push_back(max);
   
   for(int i=0; i<num; i++) {
      int j;
      cin >> j;
      partition.push_back(j);
   }

 

   sort(partition.begin(), partition.end());

   for(int i=partition.size()-1; i>=0; i--)
      for(int j=i; j>=0; j--)
	 answer.push_back(partition[i]-partition[j]);



    sort(answer.begin(), answer.end());

    auto tans = unique(answer.begin(), answer.end());

    answer.erase(tans, answer.end());

    if(answer[0]==0)
       answer.erase(answer.begin());
    
    for(int i=0; i<answer.size(); i++)
      cout << answer[i] << " ";



    return 0;
}
