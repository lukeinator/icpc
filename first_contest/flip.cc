#include <iostream>
#include <queue>
#include <string>
#include<map>
using namespace std;

bool foundsol(string s) {
   for(int i=0; i<9; i++)
      if(s.at(i)=='*')
	    return false;

   return true;
}

char toggleChar(char c) {
   if(c=='*')
      return '.';

   if(c=='.')
      return '*';
}
int main() {
   int num;
   cin >> num;

   for(int z=0; z<num; z++) {

      string data = "";
   for(int y=0; y<9; y++) {
	 char ch;
	 cin >> ch;
	 data += ch;
	 
      }
   

   if(foundsol(data)==true) {
      cout << '0' << endl;
      continue;
      }

   queue<string> tests;

   tests.push(data);
   
   map<string, int> tested;
   tested[data]=0;

   
   while(!tests.empty()) {
 

      string old = tests.front();
      tests.pop();
     
      if(foundsol(old)==true) {
	 cout << tested[old] << endl;
	 break;
      }
      
      string cases[9];
      for(int i=0; i<9; i++)
	 cases[i] = old;

    
      for(int i=0; i<9; i++) {

	 cases[i].at(i) = toggleChar(cases[i].at(i));
	 
	 if(i>2)
	    cases[i].at(i-3) = toggleChar(cases[i].at(i-3));

	 if(i!=0&&i!=3&&i!=6)
	    cases[i].at(i-1) = toggleChar(cases[i].at(i-1));

	 if(i!=2&&i!=5&&i!=8)
	    cases[i].at(i+1) = toggleChar(cases[i].at(i+1));

	 if(i<6)
	    cases[i].at(i+3) = toggleChar(cases[i].at(i+3));


	 if(tested.count(cases[i])==0) {
	    tested[cases[i]] = tested[old]+1;
	    tests.push(cases[i]);
	 }
      }

    

   }
	    
   }
   return 0;
}
