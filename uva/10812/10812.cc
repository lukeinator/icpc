#include <iostream>
#include <algorithm>
using namespace std;

typedef long long ll;

int main() {
	int cases;
	cin>>cases;

	while(cases--) {
		ll c, d;
		cin >> c >> d;

		ll a = (c+d)/2;
		ll b = c-a;

		if(a<b)
			swap(a, b);

		if(a<0||b<0||(c+d)%2!=0)
			cout << "impossible" << endl;
		else
			cout << a << " " << b << endl;
	}
}