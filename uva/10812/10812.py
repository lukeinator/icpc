
cases = int(input())

while cases:
	cases-=1
	str = input().split()

	c = int(str[0])
	d = int(str[1])
	
	a = int((c+d)/2)
	b = c-a

	if(a<b):
		a, b = b, a

	if(b<0 or a<0 or (c+d)%2 != 0):
		print("impossible")
	else:
		print(a, b)