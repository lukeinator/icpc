import sys

while True:
    nums = sys.stdin.readline()
    if nums == '':
        break

    nums = list(map(int, nums.split()))
    nums.remove(nums[0])

    diffs = []
    for i in range(len(nums)-1):
        diffs.append(abs(nums[i]-nums[i+1]))

    diffs.sort()

    jolly = True

    for i in range(len(nums)-1):
        if(diffs.count(i+1)==0):
            jolly = False

    if(jolly):
        print("Jolly")
    else:
        print("Not jolly")
