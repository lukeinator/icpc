#include <iostream>

using namespace std;

long long iabs(long long l) {
	if(l < 0) l *= -1;
	return l;
}

int main() {
	long long num;

	while(cin >> num) {
		if(num == 0)
			break;

		int best = 1;

		for(long long i=-65536; i <= 65536; i++) {
			if(i == 0 || i == -1 || i == 1)
				continue;

			int count=1;
			long long sum=i;
			
			while(iabs(sum) < iabs(num)) {
				sum *= i;
				count++;
			}

			if(sum == num)
				best = max(best, count);

		}

		cout << best << endl;
	}
}
