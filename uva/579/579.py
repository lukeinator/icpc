import math

hands = input().split(':')
hands = [int(i) for i in hands]

while(hands[0]!=0):
	if(hands[0] == 12):
		hands[0] = 0;


	mangle = (hands[1]/60.0)*360
	hangle = (hands[0]/12.0)*360 + (mangle/360)*(360.0/12)

	if(math.fabs(mangle-hangle) > 180):
		print("{0:.3f}".format(360 - math.fabs(mangle-hangle)))
	else:
		print("{0:.3f}".format(math.fabs(mangle-hangle)))

	hands = input().split(':')
	hands = [int(i) for i in hands]