#include <iostream>
#include <map>
#include <algorithm>
using namespace std;

map<int, int> seen;

int calc(int num) {
	if(seen.count(num) != 0)
		return seen[num];

	if(num == 1)
		return seen[num] = 1;

	else if(num % 2 == 0) return seen[num] = 1+calc(num/2);

	else return seen[num] = 1+calc(3*num+1);
}

int main() {
	int a, b;

	while(cin >> a >> b) {
		int a2=a, b2=b;

		if(b < a)
			swap(a, b);

		int best=0;

		for(int i=a; i <= b; i++)
			best = max(best, calc(i));

		cout << a2 << " " << b2 << " " << best << endl;
	}

	return 0;
}
