#include <iostream>
#include <set>

using namespace std;

int main() {
	string s;
	int roun=1;
	while(getline(cin, s)&&s!="-1") {
		cout << "Round " << roun++ << endl;
		string word, guess;

		getline(cin, word);
		getline(cin, guess);

		set<char> game;
		set<char> guesses;

		for(char ch : word)
			game.insert(ch);

		int bad=0;

		for(char ch : guess) {
			if(game.count(ch)!=0) {
				game.erase(ch);
				guesses.insert(ch);
			}

			else if(guesses.count(ch)==0) {
				bad++;
				guesses.insert(ch);
			}
			

			if(game.size()==0) {
				cout << "You win." << endl;
				break;
			} else if(bad==7) {
				cout << "You lose." << endl;
				break;
			}

		}

		if(game.size()!=0&&bad<7)
			cout << "You chickened out." << endl;


	}

	return 0;
}