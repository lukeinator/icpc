#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

int main() {
	vector<int> nums;
	int num;

	while(cin>>num) {
		nums.push_back(num);
		sort(nums.begin(), nums.end());

		if(nums.size()%2==0)
			cout << ((nums[(nums.size()/2)-1]+nums[(nums.size()/2)])/2) << endl;
		
		else if(nums.size()==1)
			cout << nums[0] << endl;

		else
			cout << nums[(nums.size()/2)] << endl;
	
	}


	return 0;
}