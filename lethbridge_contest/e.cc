#include <iostream>
using namespace std;

int main() {
   int n, w, l, h, a, m;

   while(cin>>n>>w>>l>>h>>a>>m
	 &&!(n==0&&w==0&&l==0
	     &&h==0&&a==0&&m==0)) {

      int total = w*l + 2*w*h + 2*h*l;

      while(m--) {
	 int x, y;
	 cin >> x >> y;

	 total -= x*y;
      }

      total *= n;

      bool d=false;

      if(total%a==0)
	 d=true;
      
      total /= a;

      if(!d)
	 total++;
      
      cout << total << endl;


   }



   return 0;
}
