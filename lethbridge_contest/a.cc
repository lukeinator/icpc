#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <map>
#include <iomanip>
using namespace std;

vector< pair<int, int> > nums;
map< pair<int, int>, int> seen;


int calc(int id, int remW) {
   if(seen.count(make_pair(id, remW))!=0)
      return seen[make_pair(id, remW)];
   
   if(remW==0)
      return seen[make_pair(id, remW)] = 0;

   if(id==nums.size())
      return seen[make_pair(id, remW)] = 0;

   if(nums[id].second>remW)
      return seen[make_pair(id, remW)] = calc(id+1, remW);


   int Max =  max(calc(id+1, remW),
		  nums[id].first + calc(id+1, remW-nums[id].second));

   Max = max(Max, nums[id].first + calc(id, remW-nums[id].second));

   return seen[make_pair(id, remW)] = Max;
}

int readNum() {
   char b,c,d;
   cin >> b >> c >> c >> d;
    int m = b-'0';
      m*=10;
      m+=c-'0';
      m*=10;
      m+=d-'0';
      return m;
}

int main() {
   int n, m;
  

   while(cin>>n&&n!=0) {
      nums.clear();
      seen.clear();
      
      m = readNum();

      while(n--) {
	 int a, b;
	 cin >> a;

	 b = readNum();
	 nums.push_back(make_pair(a, b));

      }

      cout << fixed << setprecision(2) << showpoint << calc(0, m) << endl;

      

   }
   return 0;
}
