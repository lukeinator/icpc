#include <iostream>
using namespace std;

int main() {
	while(1) {
	double h, u, d, f;

	cin >> h >> u >> d >> f;

	double temp = u;
	temp *= (1-(f*0.01));
	f = u - temp;

	if(h==0)
		break;
	int day=1;
	double pos = 0;

	while(1) {
		pos += u;
		if(pos>h) {
			cout << "success on day " << day << endl;
		break;
		}

		pos-=d;

		if(pos<0) {
			cout << "failure on day " << day << endl;
		break;
		}

		u -= f;

		if(u<0)
			u=0;
		day++;
	}

}
return 0;
}