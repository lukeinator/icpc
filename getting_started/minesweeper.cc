#include <iostream>
using namespace std;

int main() {
int field=1,r, c;

while(cin>>r>>c) {
if(r==0&&c==0)
	break;
if(field>1)
	cout << endl;

int game[110][110];

for(int i=0; i<110; i++) 
	for(int j=0; j<110; j++) 
		game[i][j]=0;

for(int i=1; i<=r; i++) {
	for(int j=1; j<=c; j++) {
		char ch;
		cin >> ch;
		if(ch=='*')
			game[i][j]=-1000;
	}
}

int row[8] = {0, 1, 0, -1, 1, -1, 1, -1};
int col[8] = {1, 0, -1, 0, 1, -1, -1, 1};

for(int i=1; i<=r; i++) {
	for(int j=1; j<=c; j++) { 
		for(int k=0; k<8; k++) {
			if(game[i][j]<0)
				game[i+row[k]][j+col[k]]++;
			}
		}
	}

cout << "Field #" << field << ":"<< endl;
for(int i=1; i<=r; i++) {
	for(int j=1; j<=c; j++) {
	if(game[i][j]<0)
		cout << '*';
	else 
		cout << game[i][j];
	}
	if(i<r)
	cout << endl;
}
cout << endl;
field++;
}
	return 0;
}