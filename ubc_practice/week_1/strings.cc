#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool works(string test) {
   bool works=true;

      for(int i=0; i<test.length(); i++) {
	 if(i==0)
	    continue;

	 if(test[i]==test[i-1]&&test[i]!='?')
	    works=false;
      }

      return works;
}

unsigned long long calc(string s) {
   size_t found = s.find("?");

   if(found==string::npos)
      return 1;


 
 unsigned long long sum=0;
   

      s[found]='v';
      if(works(s))
      sum+= 6*calc(s);


  
      s[found]='c';
      if(works(s))
      sum+= 20*calc(s);
    

   return sum;
  
}

int main() {
   string vowels = "euioay";
   string consos = "qwrtpsdfghjklzxcvbnm";
   
   int cases;
   cin>>cases;
   int str=1;
   while(cases--) {
      string s;
      cin >> s;
      string test;
      
      for(char ch : s) {
	 if(ch == '?')
	    test += "?";

	 else if(vowels.find(ch)!=string::npos)
	    test += "v";

	 else
	    test += "c";
      }
      
      cout << "String #" << str++ << ": ";
      
      
   
      
      if(works(test))
	 cout << calc(test) << endl << endl;
      else
	 cout << "0" << endl << endl;


   }


   return 0;
}
