#include <iostream>
using namespace std;

int main() {
	int cases;
	cin>>cases;
	int grid=1;
	while(cases--) {
		int d, n;
		cin >> d >> n;
		cout << "Grid #" << grid++ << ": ";
		if(d>(n*n))
			cout << "impossible" << endl << endl;
		else
			cout << (2*d*n - 2*d) << endl << endl;

	}



	return 0;
}