#include <iostream>
using namespace std;

int main() {
   int cases;
   cin>>cases;

   while(cases--) {
      int d, l, s;
      cin >> d >> l >> s;

      int cups[1005] = {0};
      int prices[1005][2];

      int minL, minS;
      int sugar=0;
      
      for(int i=0; i<d; i++) {
	 cin >> cups[i] >> prices[i][0] >> prices[i][1];

	 if(i>0) {
	    if(prices[i][0]>prices[i-1][0]) 
	       prices[i][0]=prices[i-1][0];
	 

	    if(prices[i][1]>prices[i-1][1])
	       prices[i][1]=prices[i-1][1];
	 }
      }
      int sum=0;
      for(int i=0; i<d; i++) {
	 sum+=prices[i][0]*l*cups[i];

	 while(sugar<s*cups[i]) {
	    sum+=prices[i][1];
	    sugar+=5*16;
	 }
	 sugar -= s*cups[i];
	 
      }
      cout << sum << endl;

   }

   return 0;
}
