#include <iostream>
using namespace std;

int main() {
   int count;
   cin>>count;

   int practice=1;
   while(count--) {
      int s, b, m;
      cin >> s >> b >> m;

      cout << "Practice #" << practice++ << ": " << s << " " << b << endl;

      for(int i=0; i<m; i++) {
	 int g;
	 cin >> g;
	 cout << g << " ";

	 while(b<=g)
	    b*=2;

	 b-=g;
	 cout << b << endl;

      }

      cout << endl;
   }

   return 0;
}
