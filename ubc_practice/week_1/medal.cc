#include <iostream>
using namespace std;

int main() {
   int count;
   cin >> count;

   while(count--) {
      int usa[3] = {0};
      int rus[3] = {0};

      cin >> usa[0] >> usa[1] >> usa[2];
      cin >> rus[0] >> rus[1] >> rus[2];

      int usum=0;
      int rsum=0;
      for(int i=0; i<3; i++) {
	 usum += usa[i];
	 rsum += rus[i];
      }

      bool col = false;
      bool cou = false;

      if(usum > rsum)
	 cou=true;

      for(int i=0; i<3; i++) {
	 if(usa[i]>rus[i])
	    col=true;
	 if(usa[i]<rus[i])
	    break;
      }

      cout << usa[0] << " " << usa[1] << " " << usa[2] << " "
	   << rus[0] << " "  << rus[1] << " "  << rus[2] << endl;

      
      if(col&&cou)
	 cout << "both" << endl << endl;

      else if(col)
	 cout << "color" << endl << endl;
      else if(cou)
	 cout << "count" << endl << endl;
      else
	 cout << "none" << endl << endl;
      
   }
   return 0;
}
