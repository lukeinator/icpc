#include <iostream>
#include <vector>
#include <queue>
#include <set>
using namespace std;

struct disk {
	
	disk(int n) {
		this->n = n;
		
		for(int i=0; i<n; i++) {
			vector<int> temp;
			board.push_back(temp);
		}

		for(int i=0; i<n; i++)
			for(int j=0; j<n; j++)
				board[i].push_back(0);
		
		/*for(int i=0; i<n; i++)
			for(int j=0; j<n; j++)
				board[i][j]=0;*/

	}

	int moves;
	
	vector< vector<int> > board;

	int n;
};




int main() {
	int cases;
	cin >> cases;

	while(cases--) {
		queue<disk> q;

		int d, n;
		cin >> d >> n;

		disk start = disk(n);

		start.moves = 0;
		start.board[0][0] = 1;
		start.board[n-1][n-1] = d+1;

		q.push(start);

		bool found=false;
		set< vector< vector<int> > > seen;

		while(!q.empty()) {
			disk parent = q.front();
			q.pop();

			if(parent.board[n-1][n-1]==1) {
				found=true;
				cout << parent.moves << endl;
				break;
			}

			if(seen.count(parent.board)!=0) {
			//	cout << "seen" << endl;
				continue;
			}

			cout << parent.moves << endl;
			for(int i=0; i<n; i++) {
				for(int j=0; j<n; j++)
					cout << parent.board[i][j] << " ";
				cout << endl;
			}
			cout << endl;

			seen.insert(parent.board);
			
			for(int i=0; i<n; i++) {
				for(int j=0; j<n; j++) {
					if(i==0&&j==0)
						continue;

					if(i==n-2&&j==n-1)
						continue;

					if(i==n-1&&j==n-2)
						continue;

					if(j<n-1&&parent.board[i][j+1]==0) {
						disk next = disk(parent);
						next.moves++;
						next.board[i][j+1]=next.board[i][j];
						next.board[i][j]=0;
						q.push(next);
					}

					if(i<n-1&&parent.board[i+1][j]==0) {
						disk next = disk(parent);
						next.moves++;
						next.board[i+1][j]=next.board[i][j];
						next.board[i][j]=0;
						q.push(next);
					}
				}
			}

			if(parent.board[0][0]<=d) {
				if(parent.board[0][1]==0) {
					disk next = disk(parent);
					next.moves++;
					next.board[0][1]=next.board[0][0];
					next.board[0][0]++;
					q.push(next);
				}

				if(parent.board[1][0]==0) {
					disk next = disk(parent);
					next.moves++;
					next.board[1][0]=next.board[0][0];
					next.board[0][0]++;
					q.push(next);
				}
			}


			if(parent.board[n-1][n-2]==parent.board[n-1][n-1]-1) {
				disk next = disk(parent);
				next.moves++;
				next.board[n-1][n-2]=0;
				next.board[n-1][n-1]--;
				q.push(next);
			}

			if(parent.board[n-2][n-1]==parent.board[n-1][n-1]-1) {
				disk next = disk(parent);
				next.moves++;
				next.board[n-2][n-1]=0;
				next.board[n-1][n-1]--;
				q.push(next);
			}

		}

		if(!found)
			cout << "impossible" << endl;

	}

	return 0;
}