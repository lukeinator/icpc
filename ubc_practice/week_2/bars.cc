#include <iostream>
#include <algorithm>

using namespace std;

int main() {
   int cases;
   cin >> cases;

   while(cases--) {
      int bars[100005];

      int b;
      cin>>b;

      for(int i=0; i<b; i++)
	 cin >> bars[i];

      sort(bars, bars+b);

      int bar=bars[0];
      int count = 0;
      
      for(int i=0; i<b; i++) {
	 if(bars[i]>bar) {
	    cout << bars[i] - bar << " " << count << endl;
	    //   count = 0;
	    bar = bars[i];
	 }

	 count++;
      }

      cout << endl;
   }

   return 0;
}
