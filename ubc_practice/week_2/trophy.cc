#include <iostream>
#include <algorithm>
#include <map>
#include <utility>

using namespace std;

map< pair<int, int>, int> seen;

struct trophy{
	int h, w;
};

trophy t[450];
int n;

bool operator<(const trophy& a, const trophy& b) {
	return a.h < b.h;
}

int calc(int a, int b) {
	if(a==n-1||b==n-1)
		return 0;

	if(seen.count(make_pair(a, b))!=0)
		return seen[make_pair(a, b)];

	int next = max(a, b) + 1;

	return seen[make_pair(a, b)] = min(calc(next, b)+abs(t[a].w-t[next].w), 
				calc(a, next)+abs(t[b].w-t[next].w));



}

int main() {

	while(cin>>n) {
		
		seen.clear();

		for(int i=0; i<n; i++)
			cin >> t[i].h >> t[i].w;
		
		reverse(t, t+n);

		cout << calc(0, 0) << endl;

	}
	return 0;
}