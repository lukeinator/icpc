#include <iostream>
#include <iomanip>
using namespace std;

double calc(int i, int j, double p) {
   if(i==4&&j<3)
      return 1.0;

   if(i<3&&j==4)
      return 0.0;

   if(i==3&&j==3)
      return p*p/(1-2*p*(1-p));

   return p*calc(i+1, j, p) + (1-p)*calc(i, j+1, p);

}

int main() {
   int cases;
   cin>>cases;

   while(cases--) {
      double p;

      cin >> p;

      cout << fixed << setprecision(5) << calc(0, 0, p) << endl;


   }
   return 0;
}
