#include <iostream>
#include <utility>
#include <iomanip>
using namespace std;

double win, lose, start;

double calc(double ratio, int depth, int wins, int prev) {
   if(depth-wins==4)
      return 0;

   if(wins==4)
      return ratio;
   double temp;
   if(prev==0) 
     temp=start;   

   if(prev==-1) 
    temp=lose;
   
   if(prev==1) 
      temp=win;
   
   
   double w = calc(ratio*temp, depth+1, wins+1, 1);
   double l = calc(ratio*(1-temp), depth+1, wins, -1);

   return w+l;
}

int main() {
   string s;
   int cases;
   cin>>cases;

   while(cases--) {
      getline(cin, s);
      getline(cin, s);

      int s, w, l;
      cin >> s >> w >> l;

      start = 20.0/(s+20.0);

      win = (20.0+w)/(s+20.0+w);

      lose = (20.0-l)/(s+20.0-l);

      cout << fixed << setprecision(5) << calc(1, 0, 0, 0) << endl;

   }
   return 0;
}
