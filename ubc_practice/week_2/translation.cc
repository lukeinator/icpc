#include <iostream>
#include <string>
#include <set>
using namespace std;

set<string> rep(string str, string a, string b) {
	set<string> ans;
	set<int> seen;

		string test = str;
	
		int pos = str.find(a);

		string empty;

		for(char ch : b)
			empty = empty+'9';

		while(pos!=string::npos&&!seen.count(pos)) {
	    string save = str;

		save.replace(save.begin()+pos, save.begin()+pos+a.length(), b);
		ans.insert(save);
		seen.insert(pos);

		test.replace(test.begin()+pos, test.begin()+pos+a.length(), empty);

		pos = test.find(a);
		}

	return ans;
}

string r1(string s) {
	if(s.at(s.length()-1)=='e'&&s.length()>=2) {
		s.at(s.length()-1) = s.at(s.length()-2);
		s.at(s.length()-2) = 'e';
	}

	return s;
}

string r1Rev(string s) {
	if(s.at(s.length()-2)=='e'&&s.length()>=2) {
		s.at(s.length()-2) = s.at(s.length()-1);
		s.at(s.length()-1) = 'e';
	}

	return s;
}

set<string> r2(string s) {
	return rep(s, "rge", "che");
}

set<string> r2Rev(string s) {	
	return rep(s,"che", "rge");
}

set<string> r3(string s) {
	return rep(s, "re", "rec");
}

set<string> r3Rev(string s) {
	return rep(s, "rec", "re");
}

set<string> r4(string s) {
	return rep(s, "ce", "ec");
}

set<string> r4Rev(string s) {
	return rep(s, "ec", "ce");
}


using namespace std;

int main() {
	int cases=1;
	string s;
	bool start=false;
	while(cin>>s) {
		set<string> a;
		set<string> b;

		a.insert(s);
		
		if(start)
			cout << endl;
		start=true;

		int aSize=0;
		while(1) {
			aSize = a.size();
			for(string str : a) {
				set<string> testStr;


				testStr.insert(r1(str));
				for(string t : testStr)
					if(t!=str||(t.length()>=2&&t[t.length()-1]=='e'&&t[t.length()-2]=='e'))
						b.insert(t);


				testStr = r2(str);
				for(string t : testStr)
					if(t!=str)
						b.insert(t);


				testStr = r3(str);
				for(string t : testStr)
					if(t!=str)
						b.insert(t);


				testStr = r4(str);
				for(string t : testStr)
					if(t!=str)
						b.insert(t);

			}

			for(string str : b) {
				set<string> testStr;

				testStr.insert(r1Rev(str));
				for(string t : testStr)
				if(t.length()>=3&&t.length()<=12&&(t!=str||(t.length()>=2&&t[t.length()-1]=='e'&&t[t.length()-2]=='e'))) {
					a.insert(t);

				}
				
				testStr = r2Rev(str);
				for(string t : testStr)
				if(t.length()>=3&&t.length()<=12&&t!=str) {
					a.insert(t);

				}
				
				testStr = r3Rev(str);
				for(string t : testStr)
				if(t.length()>=3&&t.length()<=12&&t!=str) {
					a.insert(t);

				}
				
				testStr = r4Rev(str);
				for(string t : testStr)
				if(t.length()>=3&&t.length()<=12&&t!=str) {
					a.insert(t);

				}
			}

			if(a.size()==aSize)
				break;
		}

		cout << "Case " << cases++ << ":" << endl;
		for(string str : a)
			cout << str <<endl;


	}

	return 0;
}