#include <iostream>
#include <string>
#include <algorithm>
#include <set>
using namespace std;

set<string> rep(string str, string a, string b) {
	set<string> ans;
	set<int> seen;

		string test = str;
	
		int pos = str.find(a);

		string empty;

		for(char ch : b)
			empty = empty+'9';

		while(pos!=string::npos&&!seen.count(pos)) {
	    string save = str;

		save.replace(save.begin()+pos, save.begin()+pos+a.length(), b);
		ans.insert(save);
		seen.insert(pos);

		test.replace(test.begin()+pos, test.begin()+pos+a.length(), empty);

		pos = test.find(a);
		}
		
	return ans;
}

set<string> r3(string s) {
	return rep(s, "re", "rec");
}



int main() {
	string s;
	while(cin>>s) {
		for(string str : r3(s))
			cout << str << endl;

	}



	return 0;
}