#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
using namespace std;

int main() {

   int n;

   while(cin>>n&&n!=0) {
      vector<int> dryer;

      for(int i=0; i<n; i++) {
	 int num;
	 cin>>num;
      
	 dryer.push_back(num);
      }
	 sort(dryer.begin(), dryer.end());
	 reverse(dryer.begin(), dryer.end());

	 int time=30;
	 int offset=0;
	 for(int i=0; i<dryer.size(); i++) {
	    if(i==dryer.size()-1||offset>0) {
	       time += dryer[i];
	       continue;
	    }

	    if(dryer[i]<30) {
	       time +=30;
	       if(offset>0)
		  offset-= 30 - dryer[i];
	    }
	    else {
	       time += dryer[i];
	       offset += dryer[i]-30;
	    }
	 }

	 
	 string minutes = (time%60)<10 ? '0'+to_string(time%60) : to_string(time%60);

	 cout << time/60 << ":" << minutes << endl; 

      }




   

   return 0;
}
