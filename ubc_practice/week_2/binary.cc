#include <iostream>
#include <string>
using namespace std;

string stripOs(string s) {
	while(s.length()!=0 && s.at(s.length()-1)=='0')
		s = s.substr(0, s.length()-1);	
	return s;
}

string multiply(string s) {
	
	int carry=0;

	for(int i=s.length()-1; i>=0; i--) {
		int c = (s.at(i)-'0')*2+carry;
		carry = c/10;
		s.at(i) = '0'+c%10;
	}

	return s;
}

bool calc(string s) {


	if(s.find('.')!=string::npos)
		s = s.substr(s.find('.')+1, s.length());

	if(s.length()==0) return true;

	s = stripOs(s);

	if(s.at(s.length()-1)!='5')
			return false;

        return calc(stripOs(multiply(s)));
		
}

int main() {
	string s;

	while(cin>>s) {
	   s = stripOs(s);
	   //  cout << s << endl;
		if(calc(s))
			cout << "Exact." << endl;
		else
			cout << "Some precision lost." << endl;
		
	}

	return 0;
}
