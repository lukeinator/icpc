#include <iostream>
using namespace std;
typedef unsigned long long ull;
int main() {
	ull n;
	while(cin>>n) {
		ull k=1;
		bool seen[10] = {false};

		while(true) {
		ull test = k*n;
		
		while(test!=0) {
			seen[test%10]=true;
			test /= 10;
		}

		bool found=true;

		for(int i=0; i<10; i++)
			if(!seen[i])
				found=false;

		if(found)
			break;

		k++;
		}

		cout << k << endl;

	}

	return 0;
}