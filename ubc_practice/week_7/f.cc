#include <iostream>
#include <string>
#include <sstream>
using namespace std;

double calc(double nums[10], int size) {
	bool end=true;
	
	for(int i=0; i<size; i++) {
		cout << nums[i] << " ";
		if(nums[i]!=0) 
			end=false;
	}
	cout << endl;
		
	if(end)
		return 1.0;

	double p;

	if(nums[0]!=0&&nums[0]!=1)
		p = (nums[0]-1)/nums[0];
	else
		p=1;
	
	
	for(int i=1; i<size; i++) {	
		if(nums[i]!=0&&nums[0]!=1)	
		p *= ((nums[i]-1)/nums[i]);
	}
	

	for(int i=0; i<size; i++)
		if(nums[i]>0)
			nums[i]--;

	double prob = p+calc(nums, size);

	return prob;
}

int main() {

	int cases;
	cin>>cases;

	while(cases--) {
		int n;
		cin>>n;

		double nums[10] = {0};

		string s;
		getline(cin, s);

		for(int i=0; i<n; i++) {
			getline(cin, s);
			istringstream iss(s);
			char t;
			while(iss>>t)
				nums[i]++;
		}

		cout << calc(nums, n) << endl;
	}



	return 0;
}