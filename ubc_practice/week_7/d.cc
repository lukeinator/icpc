#include <iostream>
#include <vector>
using namespace std;

vector< vector< int > > nums;

int main() {
	for(int i=0; i<100005; i++) {	
		vector<int>	temp;
		nums.push_back(temp);
	}

	for(int i=1; i<=100000; i++) {
		int j=1;

		while(i*j<=100000) {
			nums[i*j].push_back(i);
			j++;
		}

	}

	bool perfect[100005] = {false};

	for(int i=0; i<=100005; i++) {
		int sum=0;

		for(int n : nums[i])
			if(n!=i)
				sum+=n;

		if(sum==i)
			perfect[i] = true;	

	}

	int num;
	while(cin>>num&&num!=-1) {
		if(perfect[num]==true) {
			cout << num << " = ";
			for(int i=0; i<nums[num].size()-1; i++) {
				cout << nums[num][i];

				if(i<nums[num].size()-2)
					cout << " + ";
			}
			cout << endl;
		} else {
			cout << num << " is NOT perfect." << endl;
		}

	}


	return 0;
}