#include <iostream>
#include <set>
#include <algorithm>
#include <vector>
using namespace std;

int main() {
	int n;

	while(cin>>n&&n!=0) {
		int p;
		cin>>p;

		bool odd=false;
		if(p%2==1) {
			p++;
			odd=true;
		}
		p/=2;
		n/=2;
		n++;

		set<int> nums;
		int same = p*2;
		if(!odd)
			same--;
		nums.insert(same);
		nums.insert(abs(p-n)*2);
		nums.insert(abs(p-n)*2-1);
		
		bool start=false;

		for(int n : nums) {
			if(start)
				cout << " ";
			start=true;
			
			cout << n;
		}

		cout << endl;
	}

	return 0;
}