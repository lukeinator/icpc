#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <cctype>
using namespace std;

string alpha;

bool comp(string a, string b) {
	int i=0;

	while(i<a.length()&&i<b.length()&&a[i]==b[i])
		i++;

	if(i==a.length())
		return true;

	if(i==b.length())
		return false;

	return(alpha.find(toupper(a[i]))<alpha.find(toupper(b[i])));

}

int main() {
	int count;
	int year=1;

	while(cin>>count&&count!=0) {
		cin >>alpha;

		vector<string> words;

		while(count--) {
			string s;
			cin>>s;
			words.push_back(s);
		}

		sort(words.begin(), words.end(), comp);

		cout << "year " << year++ <<endl;

		for(string s : words)
			cout << s << endl;

	}


	return 0;
}