#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

string numerals;

string fix0(string s) {
	int pos=0;

	while(pos<s.length()&&s[pos]=='0')
		pos++;

	if(pos!=0&&pos<s.length())
		swap(s[0], s[pos]);

	return s;
}

long long calc(string a, string b, int pos) {
	if(pos==numerals.length()) {

			sort(a.begin(), a.end());
			sort(b.begin(), b.end());

			a = fix0(a);
			b = fix0(b);

			if(atoll(a.c_str())==0||atoll(b.c_str())==0)
				return 1000000000000;

			return atoll(a.c_str())+atoll(b.c_str());
	}

	return min(calc(a+numerals[pos], b, pos+1), calc(a, b+numerals[pos], pos+1));

}

int main() {

	int num;
	while(cin>>num&&num!=0) {
		numerals="";
		int zeros = 0;
		while(num--) {
			char ch;
			cin >> ch;

			numerals += ch;

		}

		cout << calc("", "", 0) << endl;
	}




	return 0;
}