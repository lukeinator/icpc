#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void print(int n) {
    if(n<10)
        cout << '0';
    cout << n;
}

int main(){
    int h, m, s;
    char ch;
    string pm;
    
    cin >> h >> ch >> m >> ch >> s >> pm;
    
    if(pm=="PM")
        h+=12;
    
    if(h==24&&pm=="PM")
        h=12;
    
    if(h==12&&pm=="AM")
        h=0;
    
    
    
    print(h);
    cout << ":";
    print(m);
    cout << ":";
    print(s);
   
    
    return 0;
}
