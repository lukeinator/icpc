#include <iostream>

using namespace std;

int main() {
   int a, b;
   cin >> a >> b;

   int diff = b - a;

   if(diff < 0)
      diff *= -1;

   //cout << "diff: " << diff << endl;

   if(diff < 180) {
      if(a <= b)
	 cout << diff << endl;
      else
	 cout << diff*-1 << endl;

   } else if(diff > 180) {
      if(a>=b)
	 cout << 360-diff << endl;
      else
	 cout << -1*(360-diff) << endl;
   } else {
      cout << "180" << endl;
   }




   return 0;
}
