#include <iostream>
#include <set>
#include <vector>
using namespace std;


class UnionFind
{
      struct UF { int p; int rank; };

   public:
      UnionFind(int n) {          // constructor
	 howMany = n;
	 uf = new UF[howMany];
	 for (int i = 0; i < howMany; i++) {
	    uf[i].p = i;
	    uf[i].rank = 0;
	 }
      }

      ~UnionFind() {
         delete[] uf;
      }

      int find(int x) { return find(uf,x); }        // for client use
      
      bool merge(int x, int y) {
	 int res1, res2;
	 res1 = find(uf, x);
	 res2 = find(uf, y);
	 if (res1 != res2) {
	    if (uf[res1].rank > uf[res2].rank) {
	       uf[res2].p = res1;
	    }
	    else {
	       uf[res1].p = res2;
	       if (uf[res1].rank == uf[res2].rank) {
		  uf[res2].rank++;
	       }
	    }
	    return true;
	 }
	 return false;
      }
      
   private:
      int howMany;
      UF* uf;

      int find(UF uf[], int x) {     // recursive funcion for internal use
	 if (uf[x].p != x) {
	    uf[x].p = find(uf, uf[x].p);
	 }
	 return uf[x].p;
      }
};


int main() {
   int n, m, q;

   cin >> n >> m >> q;

   UnionFind uf(1000000);

   set<int> s;
   vector<int> ans;

   int board[1000][1000];

   for(int i=0; i<1000; i++)
      for(int j=0; j<1000; j++)
	 board[i][j] = 1;

   int moves[10000][4];

   for(int i=0; i<q; i++) {
      for(int j=0; j<4; j++) {
	 cin >> moves[i][j];
	 moves[i][j]--;
      }
   }
      
   for(int i=0; i<q; i++) {
      if(moves[i][0] == moves[i][2]) {
	 for(int j=moves[i][1]; j<=moves[i][3]; j++)
	    board[j][moves[i][0]] = 0;
      } else {
	 for(int j=moves[i][0]; j<=moves[i][2]; j++)
	    board[moves[i][1]][j] = 0;
      }
   }
   
   for(int i=0; i<m; i++) {
      for(int j=0; j<n; j++)
	 cout << board[i][j];
      cout << endl;
   }

   for(int i=0; i<m; i++) {
      for(int j=0; j<n; j++) {
	 if(board[i][j]==1 && board[i+1][j]==1 && i<m)
	    uf.merge(i*1000+j, (i+1)*1000+j);
	 if(board[i][j]==1 && board[i][j+1]==1 && j<n)
	    uf.merge(i*1000+j, i*1000+j+1);
      }
   }

   for(int i=0; i<m; i++) 
      for(int j=0; j<n; j++) 
	 if(i*1000+j == uf.find(i*1000+j) && board[i][j] == 1)
	       s.insert(i*1000+j);
	    

   cout << s.size() << endl;

   for(int i=q-1; i>=0; i--) {
      

   }
      
   

	    


   return 0;
}
