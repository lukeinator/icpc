#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include <cstdlib>
using namespace std;
const double PI = acos(-1.0);

int main() {


   string typ;
   double secwid;
   string slash;
   double ratio;
   char speed;
   string typ2;
   bool boolspeed;

   cin >> typ >> secwid >> slash >> ratio >> speed >> typ2;
  
   while(cin) {
      double diam=0;

      if(typ2.at(0)>='A'&&typ2.at(0)<='Z') {
	 cin >> diam;
	 boolspeed=true;
      } else {
	 diam = atof(typ2.c_str());
	 typ2 = speed;
      }
    
      
      secwid = secwid*.1;
      ratio = ratio *.01;
      double diam2;

      diam2 = diam*2.54;

      diam2 += ratio*secwid*2;

      double circum;

      circum = diam2*PI;

      cout << typ << " " << secwid*10 << " / " << ratio*100 << " ";

      if(boolspeed==true)
	 cout << speed << " ";


      
      cout << fixed << setprecision(0) << typ2 << " " << diam << ": " << round(circum) << endl;
      

      cin >> typ >> secwid >> slash >> ratio >> speed >> typ2;
 boolspeed=false;
   }
   

   return 0;
}
