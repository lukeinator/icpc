#include <iostream>
#include <cmath>
using namespace std;

int main() {

  

   while(cin) {
       int odo;
       cin >> odo;
       if(odo==0)
	  break;
       
      int oldodo = odo;
      int real=0;
      int exp=0;
      while(odo>0) {
	
	 int digit;
	 int base=1;

	 for(int i=0; i<exp; i++)
	    base=base*9;
	 digit = odo%10;
	 if(digit>4)
	    digit--;

	 real += digit*base;

	 odo = odo/10;
	 exp++;
      }

      cout << oldodo << ": " << real << endl;

  
   }
   

   return 0;
}
