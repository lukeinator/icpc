//Lukas Grasse - Feb 2015
//Problem: Unidirectional TSP
//
//Approach: Sum up all paths going right to left, and then trace
//the best one backwards from left to right.
//
//Resources used: extra input/outout test cases from 
//http://www.algorithmist.com/index.php/UVa_116
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

//sums up paths from right to left
void fillsum(int row, int col, int board[11][101], long long sums[11][101]) {
	
   //loop through cols backwards
   for(int i=col-2; i>=0; i--) {
      for(int j=0; j<row; j++) {
			
	 //find best
	 long long num;
	 num=sums[(j-1+row)%row][i+1];
	 num=min(num, sums[j][i+1]);
	 num=min(num, sums[(j+1)%row][i+1]);
			
	 //add itself to best adjacent option
	 sums[j][i] = board[j][i] + num;

      }
   }

}

//trace lexographically best solution
long long traceSol(int row, int col, long long sums[11][101], 
		   int board[11][101], vector<int>& sols) {
	
   //find best starting pos
   int start = 0;
	
   for(int i=0; i<row; i++) {
      if(sums[i][0]<sums[start][0])
	 start=i;
   }

   //best starting value to return (sum)
   long long sum = sums[start][0];

   //add start as 1 index based to solution vector
   sols.push_back(start+1);

   for(int i=1; i<col; i++) {

      //find best adjacent value
      long long best = sums[start][i];
      best = min(best, sums[(start+row-1)%row][i]);
      best = min(best, sums[(start+1)%row][i]);

      //add all indexes that are best to test vector
      vector<int> test;
      //start+1
      if(sums[(start+1)%row][i]==best)
	 test.push_back((start+1)%row);

      //start
      if(sums[start][i]==best)
	 test.push_back(start);

      //start-1
      if(sums[(start+row-1)%row][i]==best)
	 test.push_back((start+row-1)%row);

      //start = first
      start = test[0];

      //find min index
      for(int i=1; i<test.size(); i++)
	 start=min(start, test[i]);

      //add min index as 1 based to solution vector
      sols.push_back(start+1);
   }

   return sum;

	
}

int main() {

   int row, col;
//cases
   while(cin>>row>>col) {

      int board[11][101];
      long long sums[11][101];

      //read in board
      for(int i=0; i<row; i++)
	 for(int j=0; j<col; j++)
	    cin >> board[i][j];

      //initialize last column of sum
      for(int i=0; i<row; i++)
	 sums[i][col-1] = board[i][col-1];

      //calculate sums
      fillsum(row, col, board, sums);
	
      //vector for solution path
      vector<int> path;

      //find sum and solution
      long long sum = traceSol(row, col, sums, board, path);

      //output sol
      for(int i=0; i<path.size(); i++) {
	 cout << path[i];
	 if(i<path.size()-1)
	    cout << " ";
	
      }
      //output answer
      cout << endl << sum << endl;
	
   }

   return 0;
}
