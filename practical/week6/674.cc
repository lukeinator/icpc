//Lukas Grasse - Feb 2015
//Problem: Coin Change
//
//Approach: Recursively subtract each coin
//from total where coin is less than max
#include <iostream>
#include <map>
#include <utility>
using namespace std;

//seen ones (can be the same for multiple test cases)
map< pair<int, int> , long long> seen;

//calc
long long calc(int change, int max) {
   //works
   if(change==0) 
      return 1;

   //doesnt work
   if(change<0)
      return 0;

   //already seen
   if(seen.count(make_pair(change, max))!=0)
      return seen[make_pair(change, max)];

   int sum = 0;

   //types of coins
   int vals[5] = {50, 25, 10, 5, 1};

   //try each coin less than max
   for(int i=0; i<5; i++) {
      if(max>=vals[i])
	 sum += calc(change-vals[i], vals[i]); 
   }
   //store seen val
   seen[make_pair(change, max)] = sum;
	
   return sum;
	
}

int main() {

   int change;
   //cases
   while(cin>>change) 
      cout << calc(change, 50) << endl;

   return 0;
}
