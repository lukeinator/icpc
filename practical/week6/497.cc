//Lukas Grasse - Feb 2015
//Problem: Strategic Defense Initiative
//
//Approach: Use longest strictly ascending subsequence
//to find best amount of missiles to attack
//
//Resources used: longest strictly ascending subsequence
//from code library 
#include <iostream>
#include <vector>
#include <string>
using namespace std;

//strictly ascending subsequence from code library
int sasc_seq(vector<int> A, int n, int S[])
{
   vector<int> last(n+1), pos(n+1), pred(n);
   if (n == 0) {
      return 0;
   }

   int len = 1;
   last[1] = A[pos[1] = 0];

   for (int i = 1; i < n; i++) {
      int j = lower_bound(last.begin()+1, last.begin()+len+1, A[i]) -
	 last.begin();
      pred[i] = (j-1 > 0) ? pos[j-1] : -1;
      last[j] = A[pos[j] = i];
      len = max(len, j);
   }

   int start = pos[len];
   for (int i = len-1; i >= 0; i--) {
      S[i] = A[start];
      start = pred[start];
   }

   return len;
}


int main() {

//get cases
   int cases;
   cin >> cases;
	
   //string for input
   string s;
   //get rid of extra spaces
   getline(cin, s);
   getline(cin, s);
   //for each case
   for(int a=0; a<cases; a++) {

      //missiles
      vector<int> missiles;
      //solution
      int sol[100000];
		
      //read in number until space
      while(getline(cin, s)&&atoi(s.c_str())) {
	 missiles.push_back(atoi(s.c_str()));
      }

      //find best path
      int len = sasc_seq(missiles, missiles.size(), sol);

      //output length and solution
      cout << "Max hits: " << len << endl;
      for(int i=0; i<len; i++)
	 cout << sol[i] << endl;

      if(a<cases-1)
	 cout << endl;

   }

   return 0;
}
