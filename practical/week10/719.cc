//Lukas Grasse - April 2015
//Problem: Glass Beads
//
//Approach: Use least rotation function from code library to
//find least rotation position and output
//
//Resources used: Suffix array and LCP array from code library


#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

// pos = position of the start of the lexicographically least rotation
// period = the period
void compute(string s, int &pos, int &period)
{
   s += s;
   int len = s.length();
   int i = 0, j = 1;
   for (int k = 0; i+k < len && j+k < len; k++) {
      if (s[i+k] > s[j+k]) {
	 i = max(i+k+1, j+1);
	 k = -1;
      } else if (s[i+k] < s[j+k]) {
	 j = max(j+k+1, i+1);
	 k = -1;
      }
   }

   pos = min(i, j);
   period = (i > j) ? i - j : j - i;
}

int main(void)
{
   int cases;
   cin >> cases;
   while (cases--) {
      string s;
      cin >> s;
    
      //position of least rotation
      int pos, period;
      compute(s, pos, period);
    
      //output indexed by 1 not 0
      cout << pos+1 << endl;

   }
   return 0;
}

