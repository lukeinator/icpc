#!/bin/bash

## This script will collect everything, in the directory specified as the
## argument, into a single file that can be submitted using Moodle
##
## Written by: Rex Forsyth
## Jan 17, 2011

## Modify the FILELIST to include the names of all files to be submitted
## Filenames should be separated by spaces
FILELIST=""

TMPDIR=${USER}

if [ -e "$TMPDIR" ] ; then
   echo "The working directory $TMPDIR already exists.  Exiting."
   exit 1
fi

umask 077
trap "rm -rf $TMPDIR 1>&- 2>&-" 0 1 2 3 15

if [ $# -eq 0 ] ; then 
   echo "You must provide a directory name." 
   exit 1
fi

if [ ! -d "$1" ] ; then
   echo "The argument must be a valid directory name." 
   exit 1
fi
   
cp -rf "$1" "$TMPDIR"
cd "$TMPDIR"
rm -f *.o 2> /dev/null                     ## remove any .o files
rm -f  *% *~  *\# .*\# 2> /dev/null        ## remove %,~ and # files
DIRS=
SENT=
for fname in *; do                         ## for each file in the directory
 if file "$fname" | grep 'ELF' > /dev/null ## if it is a binary executable
 then                          
    rm -f  "$fname" 2> /dev/null           ##      remove it
 elif [ -f $fname ] ; then                 ## if it is a file, 
    SENT="$SENT $fname"                    ##    add it to the SENT list
 elif [ -d $fname ] ; then                 ## if it is a file, 
    DIRS="$DIRS $fname"                    ##    add it to the DIRS list
 fi
done

## this section checks to see if all the desired files are present and
## if not, it issues a warning message.
for file in  $FILELIST; do
  if [ ! -f $file ] ; then 
    echo "$file       <- I can not find this file in the $1 directory."
    echo "             It will be missing from your submission."
    touch $file.missing                    ## place dummy in directory
  fi
done 
cd -  > /dev/null                         ## change back to original directory
FILENAME=$(basename "$1").submit
rm -rf "$FILENAME"                        ## delete old version if it exists
zip -r "$FILENAME" "$TMPDIR" > /dev/null  ## zip the directory
rm -rf "$TMPDIR"                          ## delete the temporary directory

if [ -z "$SENT" ] && [ -z "$DIRS" ] ; then
  echo "NO FILES found to bundle!!! "
  rm -rf "$FILENAME"
else
  if [ ! -z "$DIRS" ] ; then
     echo "The following directories and contents were bundled: "
     echo "   $DIRS"
  fi
  echo "The following files were bundled: "
  echo "    $SENT"
  echo "$FILENAME created.  Submit it using moodle."
fi
