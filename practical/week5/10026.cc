//Lukas Grasse - Feb 2015
//Problem: Shoemaker's Problem
//
//Approach: Sort according to comparison:
//			secondPenalty*firstTime < firstPenalty*secondTime
//repurposed algorithm's sort.
#include <iostream>
#include <algorithm>
#include <map>
#include <vector>

using namespace std;

//comparison function for algorithm sort
bool compare(pair<int, int> i, pair<int, int> j) {

return (j.second*i.first<i.second*j.first);
}

int main() {

//read in cases
int cases;
cin >> cases;

//for cases
for(int a=0; a<cases; a++) {

	//read in jobnum
	int job;
	cin >> job;

	//location of pairs in list
	map< pair<int, int>, int > locs;

	//pairs for sorting
	vector< pair<int, int> > jobs;

	//read in each job
	for(int i=1; i<=job; i++) {
		int t, s;

		cin >> t >> s;

		locs[make_pair(t, s)] = i;
		jobs.push_back(make_pair(t, s));
	}
	//sort jobs using custom function
	sort(jobs.begin(), jobs.end(), compare);

	//output answers
	for(int i=0; i<jobs.size(); i++) {
		cout << locs[jobs[i]];

		if(i<jobs.size()-1)
			cout << " ";
	}

	cout << endl;
	if(a<cases-1)
		cout << endl;

}
	return 0;
}