//Lukas Grasse - Feb 2015
//Problem: Maximum Sum 2
//
//Approach: Add up all numbers greater than zero.
//output sum or 0 if entire list is 0.
#include <iostream>
#include <vector>
using namespace std;

int main() {

	int cases;
	//loop through cases
	while(cin>>cases&&cases!=0) {
		//list of numbers
		vector<int> nums;

		//read in and sum numbers greater than 0
		for(int i=0; i<cases; i++) {
			int num;
			cin >> num;

			if(num>0)
				nums.push_back(num);
		}
		//if empty output 0, else output sum
		if(nums.size()==0) {
			cout << "0" << endl;
		} else {

			//output answer
		for(int i=0; i<nums.size(); i++) {
			cout << nums[i];
			if(i<nums.size()-1)
				cout << " ";
		}

		cout << endl;
	}

	}

	return 0;
}