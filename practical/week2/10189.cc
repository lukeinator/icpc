//Lukas Grasse - Jan 2014
//Minesweeper problem
//
//Approach: for each game an integer array is set to 0
//and then the location of every mine is set to -1000.
//using a table of locations for up, down, left, right, 
//and diagonals, 1 is added to every location surrounding
//a mine. On output, the negative numbers are switched back
//to mine symbols

#include <iostream>
using namespace std;

int main() {
   int field=1,r, c;

   while(cin>>r>>c) {

//end case
      if(r==0&&c==0)
	 break;

//keeps count of field number to print spaces
//correctly
      if(field>1)
	 cout << endl;

      int game[110][110];

//initialize
      for(int i=0; i<110; i++) 
	 for(int j=0; j<110; j++) 
	    game[i][j]=0;

//read in board
      for(int i=1; i<=r; i++) {
	 for(int j=1; j<=c; j++) {
	    char ch;
	    cin >> ch;
	    if(ch=='*')
	       game[i][j]=-1000;
	 }
      }

//arrays representing x and y offsets
      int row[8] = {0, 1, 0, -1, 1, -1, 1, -1};
      int col[8] = {1, 0, -1, 0, 1, -1, -1, 1};

//increment squares surrounding each mine
      for(int i=1; i<=r; i++) {
	 for(int j=1; j<=c; j++) { 
	    for(int k=0; k<8; k++) {
	       if(game[i][j]<0)
		  game[i+row[k]][j+col[k]]++;
	    }
	 }
      }

//output numbered mine field
      cout << "Field #" << field << ":"<< endl;
      for(int i=1; i<=r; i++) {
	 for(int j=1; j<=c; j++) {
	    if(game[i][j]<0)
	       cout << '*';
	    else 
	       cout << game[i][j];
	 }
	 if(i<r)
	    cout << endl;
      }
      cout << endl;
      field++;
   }
   return 0;
}
