
import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

//Lukas Grasse 18 January, 2015
//Cancer or Scorpio problem

//Resources Used:

//general java info
//http://pages.cs.wisc.edu/~cs368-2/JavaTutorial/

//Gregorian Calendar docs
//http://docs.oracle.com/javase/7/docs/api/java/util/GregorianCalendar.html

//Integer docs
//http://docs.oracle.com/javase/7/docs/api/java/lang/Integer.html

//Calendar docs
//http://docs.oracle.com/javase/7/docs/api/java/util/Calendar.html#after(java.lang.Object)

//SimpleDateFormat docs
//http://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html

//Approach: Input is parsed into a gregorian calendar. The 40 weeks are added
//and the horoscope is calculated in getHoroscope function

class Main {

   public static void main(String[] args) {

      //read input
      Scanner scanner = new Scanner( System.in );
      String input = scanner.nextLine();
      input = input.trim();

      //read casenum
      int cases = Integer.parseInt(input);

      //loop through cases
      for(int i=0; i<cases; i++) {

	 //read case
	 input = scanner.nextLine();

	 //set up calendar with parsed input. Months are -1 since they go from 0-11
	 GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(input.substring(4, 8)), 
							    Integer.parseInt(input.substring(0, 2))-1, 
							    Integer.parseInt(input.substring(2, 4)));

	 //add the 40 weeks
	 calendar.add(Calendar.WEEK_OF_MONTH, 40);

	 //format for output
	 SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	 dateFormat.setCalendar(calendar); 

	 //output answer
	 System.out.println(i+1 + " " + dateFormat.format(calendar.getTime()) + " " + getHoroscope(calendar));

      }
   }

   //takes in calendar as parameter and returns horoscope string
   //uses lookup array for comparison of days
   public static String getHoroscope(GregorianCalendar calendar) {

      //get year in question
      int year = calendar.get(Calendar.YEAR);

      //day of month that ends that months horoscope
      int[] days = {21, 19, 20, 20, 21, 21, 22, 21, 23, 23, 22, 22, 20};

      //capricorn appears at beginning for first part of january, and end for end of year
      String[] horoscopes = {"capricorn", "aquarius", "pisces", "aries", "taurus", "gemini", 
			     "cancer", "leo", "virgo", "libra", "scorpio", "sagittarius", "capricorn"};

      //compare each months date until we find which one input calendar is less than
      for(int i=0; i<13; i++) {
	 if(calendar.before(new GregorianCalendar(year, i, days[i])))
	    return horoscopes[i];
      }

      return horoscopes[13];

   }
}