//Lukas Grasse - Jan 2014
//Saxaphone problem
//
//Approach: A vector of vectors representing
//each note is created. A string converts the 
//character to the index number. Calculate updates
//an array representing the number of finger presses
//the note 'H' is a dummy note prepended to the 
//beginning of the song to allow the first comparison to
//work
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

vector< vector<int> > keys;
string notes;

//updates finger array with the numbers from finger b
//less than the notes from finger a
void calculate(char a, char b, int fingers[11]) {

   vector<int> test;
//loops through previous notes fingers and if current finger
//isnt found, add it to the test vector
   for(unsigned int i=0; i<keys[notes.find(b)].size(); i++) {
	
      if(count(keys[notes.find(a)].begin(), 
	       keys[notes.find(a)].end(), 
	       keys[notes.find(b)][i]) == 0)
	 test.push_back(keys[notes.find(b)][i]);
   }

//increment all remaining fingers
   for(auto i : test)
      fingers[i]++;

}

int main() {
	
   //used to convert chars to int index
   notes = "cdefgabCDEFGABH";

   //first index of keys is note, second is
   //finger number
   keys.push_back({2, 3, 4, 7, 8, 9, 10});
   keys.push_back({2, 3, 4, 7, 8, 9});
   keys.push_back({2, 3, 4, 7, 8});
   keys.push_back({2, 3, 4, 7});
   keys.push_back({2, 3, 4});
   keys.push_back({2, 3});
   keys.push_back({2});
	
   keys.push_back({3});
   keys.push_back({1, 2, 3, 4, 7, 8, 9});
   keys.push_back({1, 2, 3, 4, 7, 8});
   keys.push_back({1, 2, 3, 4, 7});
   keys.push_back({1, 2, 3, 4});
   keys.push_back({1, 2, 3});
   keys.push_back({1, 2});
   keys.push_back({});

   int cases;
   cin >> cases;

   //get rid of newline char
   string discard;
   getline(cin, discard);

   for(int a=0; a<cases; a++) {

      int fingers[11] = {0};


      //read in song
      string song;
      getline(cin, song);

      //prepend H to song for first comparison
      song = "H" + song;

      //calculate fingers
      for(unsigned int i=0; i< song.length()-1; i++) {
	 calculate(song.at(i), song.at(i+1), fingers);
      }
      //output answer
      for(int i=1; i<11; i++) {
	 cout << fingers[i];
	 if(i<10)
	    cout << ' ';

      }

      cout << endl;

   }

   return 0;
}
