//Lukas Grasse - March 2015
//Problem: 
//
//Approach: Use recursion to find length of longest common substring
//output the number

#include <iostream>
#include <string>
using namespace std;

//test slump
bool slump(string s) {
   int pos=0;

   //prop 1
   if(!(s[pos]=='D'||s[pos]=='E'))
      return false;

   pos++;

   //prop 2
   if(s[pos]!='F')
      return false;

   while(s[pos]=='F')
      pos++;

   //prop 3
   if(pos==s.length())
      return false;

   if(pos==s.length()-1&&s[pos]=='G')
      return true;

   return slump(s.substr(pos));
}

//test slimp
bool slimp(string s) {
   int pos=0;


   //prop 1
   if(s[pos++]!='A') 
      return false;
	
   //prop 2
   if(s[pos]=='H'&&pos==s.length()-1)
      return true;

   //prop 3
   if(s[s.length()-1]!='C') 
      return false;

   //prop 3b
   if(slump(s.substr(pos, s.length()-2)))
      return true;

   //prop 3a
   if(s[pos]=='B'&&slimp(s.substr(pos+1, s.length()-3))) 
      return true;	
	

   return false;



}

//test slurpy
bool slurpy(string s) {
//for each division test slimp and slump
   for(int i=0; i<s.length(); i++)
      if(slimp(s.substr(0, i))&&slump(s.substr(i)))
	 return true;

   return false;
}

int main() {
   int n;
   cin >> n;
	
   string s;
   getline(cin, s);

   //output
   cout << "SLURPYS OUTPUT" << endl;
	
   //test each case
   for(int i=0; i<n; i++) {
      getline(cin, s);
      cout << (slurpy(s) ? "YES" : "NO") << endl; 
   }

   //end output
   cout << "END OF OUTPUT" << endl;

   return 0;
}
