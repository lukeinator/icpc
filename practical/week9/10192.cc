
//Lukas Grasse - March 2015
//Problem: Vacation
//
//Approach: Use recursion to find length of longest common substring
//output the number

#include <iostream>
#include <string>
#include <algorithm>
#include <map>
using namespace std;

//substrings already visited
map<pair<int, int>, int> seen;

//recursive function
int calc(string& s1, string& s2, int i, int j) {

//if already seen
   if(seen.count(make_pair(i, j))!=0)
      return seen[make_pair(i, j)];

//if string is empty
   if(i<0||j<0)
      return 0;

//if chars are same, try i-1, j-1
   if(s1[i]==s2[j])
      return seen[make_pair(i, j)] = 1 + calc(s1, s2, i-1, j-1);

//if different find best between left or right reduce in size
   return seen[make_pair(i, j)] = max(calc(s1, s2, i, j-1), calc(s1, s2, i-1, j));

}

int main() {
   string s1, s2;
   int cases=1;
	
   //get strings
   while(getline(cin, s1)&&s1[0]!='#') {
      getline(cin, s2);	

      //output answer
      cout << "Case #" << cases++ << ": you can visit at most " 
	   << calc(s1, s2, s1.size()-1, s2.size()-1) << " cities." << endl;

      //empty seen array
      seen.clear();
   }

   return 0;
}
