//Lukas Grasse - March 2015
//Problem: Full Tank?
//
//Approach: Use dijkstra's algorithm. Each node stores location and fuel
//amount. It costs nothing to go from city to city, but fuel amount is reduced.
//edges from the same city to the city with a higher fuel amount cost the
//price of fuel 
//
//Resources used: dijkstra's sparse algorithm from code library.
//http://acm.uva.es/board/viewtopic.php?f=41&t=42186&p=95640&hilit=11367&sid=34cb821dc9057c970d42d684df862545#p95640
#include <iostream>
#include <algorithm>
#include <vector>
#include <cassert>
#include <queue>
#include <map>
#include <set>

using namespace std;


struct Edge {
   int to;
   int weight;       // can be double or other numeric type
   Edge(int t, int w)
      : to(t), weight(w) { }
};
  
typedef vector<Edge>::iterator EdgeIter;

struct Graph {
   vector<Edge> *nbr;
   int num_nodes;
   Graph(int n)
      : num_nodes(n)
   {
      nbr = new vector<Edge>[num_nodes];
   }

   ~Graph()
   {
      delete[] nbr;
   }

   // note: There is no check on duplicate edge, so it is possible to
   // add multiple edges between two vertices
   //
   // If this is an undirected graph, be sure to add an edge both
   // ways
   void add_edge(int u, int v, int weight)
   {
      nbr[u].push_back(Edge(v, weight));
   }
};

/* assume that D and P have been allocated */
void dijkstra(const Graph &G, int src, int end, vector<int> &D, int cap)
{
   typedef pair<int,int> pii;

   int n = G.num_nodes;
   vector<bool> used(n, false);
   priority_queue<pii, vector<pii>,  greater<pii> > fringe;

   D.resize(n);
 
   fill(D.begin(), D.end(), -1);


   D[src] = 0;
   fringe.push(make_pair(D[src], src));

   while (!fringe.empty()) {
      pii next = fringe.top();
      fringe.pop();
      int u = next.second;
      if (used[u]) continue;
      used[u] = true;

      for (EdgeIter it = G.nbr[u].begin(); it != G.nbr[u].end(); ++it) {
	 int v = it->to;
	 int weight = it->weight + next.first;

    if (used[v]) continue;
	 if (D[v] == -1 || weight < D[v]) {
	    D[v] = weight;

	    if(v%101<=cap)
	       fringe.push(make_pair(D[v], v));

      if(v==end)
      return;
	 }
      }
   }
}


int main(void)
{
 

   Graph G(101000);

   //read in n and m
   int n, m;
   cin >> n >> m;

   //prices at each city
   vector<int> prices(1005);

   //read in prices
   for(int i=0; i<n; i++) {
      int p;
      cin >> p;
      prices[i] = p;
   }    
                                                                       
   //fuel price at each city
   set<int> fuel;


   //for each road
   for(int i=0; i<m; i++) {
      int u, v, d;

      //read in u v and distance
      cin >> u >> v >> d;
    
      //add city for fuel up edges
      fuel.insert(u);
      fuel.insert(v);

      for(int j=0; j<=100; j++) {
	 //if we can make it without running out of fuel
	 if(j-d>=0) {
	    G.add_edge(u*101 + j, v*101 + (j-d), 0);    
	    G.add_edge(v*101 + j, u*101 + (j-d), 0);
	 }
    
      }

   }

   //add fuel up edges
   for(auto u : fuel)
      for(int i=0; i<100; i++) {
	 G.add_edge(u*101 + i, u*101 + i + 1, prices[u]); 
      }

   //queries
   int q;
   cin >> q;


  
   //for each query
   for(int i=0; i<q; i++) {
      bool exists=false;
      int cap, u, v;
      cin >> cap >> u >> v;
   
      //djikstras
      vector<int> D;

      dijkstra(G, u*101, v*101, D, cap);


   //found
	 if(D[v*101]>-1) {
	  cout << D[v*101] << endl;
	    exists=true;

	 }
   
   //not found
   if(!exists)
	 cout << "impossible" << endl;

   }




   return 0;
}
