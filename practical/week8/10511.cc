//Lukas Grasse - March 2015
//Problem: Councilling
//
//Approach: set up graph with source to each party with max flow
//of 1 less than half of the count of the clubs. set up edges with flow
//of 1 from party to people and people to clubs and clubs to sink. Run
//max flow and find one person per club where the club is contributing to
//the sink.

//Resources used:
//http://acm.uva.es/board/search.php?keywords=10511
//Max Flow from code library

#include <iostream>
#include <algorithm>
#include <vector>
#include <list>
#include <cassert>
#include <map>
#include <string>
#include <sstream>
#include <set>

using namespace std;

struct Edge;
typedef list<Edge>::iterator EdgeIter;

struct Edge {
   int to;
   int cap;
   int flow;
   bool is_real;
   EdgeIter partner;
  
   Edge(int t, int c, bool real = true)
      : to(t), cap(c), flow(0), is_real(real)
   {};

   int residual() const
   {
      return cap - flow;
   }
};

struct Graph {
   list<Edge> *nbr;
   int num_nodes;
   Graph(int n)
      : num_nodes(n)
   {
      nbr = new list<Edge>[num_nodes];
   }

   ~Graph()
   {
      delete[] nbr;
   }

   // note: this routine adds an edge to the graph with the specified capacity,
   // as well as a residual edge.  There is no check on duplicate edge, so it
   // is possible to add multiple edges (and residual edges) between two
   // vertices
   void add_edge(int u, int v, int cap)
   {
      nbr[u].push_front(Edge(v, cap));
      nbr[v].push_front(Edge(u, 0, false));
      nbr[v].begin()->partner = nbr[u].begin();
      nbr[u].begin()->partner = nbr[v].begin();
   }
};

void push_path(Graph &G, int s, int t, const vector<EdgeIter> &path, int flow)
{
   for (int i = 0; s != t; i++) {
      if (path[i]->is_real) {
	 path[i]->flow += flow;
	 path[i]->partner->cap += flow;
      } else {
	 path[i]->cap -= flow;
	 path[i]->partner->flow -= flow;
      }
      s = path[i]->to;
   }
}

// the path is stored in a peculiar way for efficiency: path[i] is the
// i-th edge taken in the path.
int augmenting_path(const Graph &G, int s, int t, vector<EdgeIter> &path,
		    vector<bool> &visited, int step = 0)
{
   if (s == t) {
      return -1;
   }
   for (EdgeIter it = G.nbr[s].begin(); it != G.nbr[s].end(); ++it) {
      int v = it->to;
      if (it->residual() > 0 && !visited[v]) {
	 path[step] = it;
	 visited[v] = true;
	 int flow = augmenting_path(G, v, t, path, visited, step+1);
	 if (flow == -1) {
	    return it->residual();
	 } else if (flow > 0) {
	    return min(flow, it->residual());
	 }
      }
   }
   return 0;
}

// note that the graph is modified
int network_flow(Graph &G, int s, int t)
{
   vector<bool> visited(G.num_nodes);
   vector<EdgeIter> path(G.num_nodes);
   int flow = 0, f;

   do {
      fill(visited.begin(), visited.end(), false);
      if ((f = augmenting_path(G, s, t, path, visited)) > 0) {
	 push_path(G, s, t, path, f);
	 flow += f;
      }
   } while (f > 0);
  
   return flow;
}

int main(void)
{
   string s;
  
   //read in cases
   int cases;
   cin >> cases;

   cin.ignore();
   cin.ignore();


   while(cases--) {
 
      //graph
      Graph G(2000000);

      //start at 1, since 0 is source
      int available=1;
  
      //maps to keep track of locations in net array
      map<string, int> party;
      map<string, int> clubs;
      map<string, int> person;

      //net array
      string net[800000];

      //while line isnt empty
      while(getline(cin, s)&&s.length()!=0) {

	 //istringstream
	 istringstream iss(s);

	 //read in person
	 string pe;
	 iss >> pe;
    
	 //set person to next spot in array
	 person[pe] = available;
	 available++;
	 net[person[pe]] = pe;

	 //read in party
	 string pa;
	 iss >> pa;

	 //if party hasn't been seen, insert it
	 if(party.count(pa)==0) {
	    party[pa] = available;
	    available++;
	 }

	 //set party in array
	 net[party[pa]] = pa;

	 //add edge to graph
	 G.add_edge(party[pa], person[pe], 1);

	 //string for club, and set to remove duplicate clubs
	 string cl;
	 set<string> seenclub;

	 //read in clubs
	 while(iss>>cl) {

	    //if new club, insert into net
	    if(clubs.count(cl)==0) {
	       clubs[cl] = available;
	       available++;
	    } 

	    //add edge if we havent already
	    if(seenclub.count(cl)==0)
	       G.add_edge(person[pe], clubs[cl], 1);
    
	    //add to net
	    net[clubs[cl]] = cl;
	    seenclub.insert(cl);
	 }

      }

      //find max edge size from source to party
      int maxS = ((clubs.size()+1)/2)-1;

 

      //add edges from source to party
      for(auto par : party) 
	 G.add_edge(0, par.second, maxS);

      //add edge from clubs to sink
      for(auto clu : clubs) 
	 G.add_edge(clu.second, available, 1);

      //run network flow
      int flow = network_flow(G, 0, available);

      //if not enough flow, output impossible
      if(flow<clubs.size())
	 cout << "Impossible." << endl;

      //output answer
      else  {
	 //find clubs that are sending to sink
	 set<int> clu;

	 //for each node
	 for(int i=0; i<G.num_nodes; i++) {
    
	    //for each edge in node
	    for(auto e : G.nbr[i]) {
	       //if is a node from a club and has flow and is real
	       if(clubs.count(net[i])!=0&&e.flow>0&&e.is_real==true&&e.to==available)
		  clu.insert(i);
	    }

	 }

	 //map for output
	 map<string, string> output;

	 //for each node
	 for(int i=0; i<G.num_nodes; i++) {

	    //for each edge
	    for(auto e : G.nbr[i]) {
        
	       //if edge goes to club and is real and has flow
	       //set output for person to the club. map ensures
	       //person is unique
	       if(clu.count(e.to)!=0&&e.flow>0&&e.is_real==true)
		  output[net[i]] = net[e.to];
	    }

	 }

	 //output answer
	 for(auto o : output)
	    cout << o.first << " " << o.second << endl;
      }

      if(cases!=0)
	 cout << endl;

   }

   return 0;
}


