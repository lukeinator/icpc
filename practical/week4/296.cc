//Lukas Grasse - Feb 2015
//Problem: Safebreaker
//
//Approach: Try all combinations. Count number of digits
//completely correct and number of total correct digits 
//minus the completely correct ones. If these numbers match
//for every guess, a solution is found. if one solution is found
//output it, otherwise output none or too many.

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

//function that counts how many digits are completely correct
int cPerm(int a[4], int b[4]) {
   int c=0;

   for(int i=0; i<4; i++) {
      if(a[i]==b[i])
	 c++;
   }

   return c;
}

//count number of correct digits in wrong place
int cComb(int a[4], int b[4]) {
   int c[4];
   int d[4];

   int comb=0;

//copy to test arrays
   for(int i=0; i<4; i++) {
      c[i] = a[i];
      d[i] = b[i];
   }

//count all correct digits, even in wrong place
   for(int i=0; i<4; i++) {
      for(int j=0; j<4; j++) {
	 if(c[i]==d[j]) {
	    comb++;
	    d[j]=-1;
	    break;
	 }
      }

		
   }

//return total correct digits minus completely correct
   return comb-cPerm(a, b);
}

//convert integer to array of digits
void getArr(int a, int b[4]) {
   int dig=1000;

   for(int i=0; i<4; i++) {
      b[i] = a/dig;
      a%=dig;
      dig/=10;
   }
}

//calculate whether or not a test array of digits passes all guesses
//passed to function
bool compute(int g, int test[4], int guess[11][4], int hints[11][2]) {
	
   for(int i=0; i<g; i++) {
      if(cPerm(test, guess[i])!=hints[i][0]
	 ||cComb(test, guess[i])!=hints[i][1])
	 return false;

   }

   return true;
}

int main() {
//cases
   int cases;
   cin >> cases;
//cases loop
   for(int a=0; a<cases; a++) {
      int g;
      cin >> g;

      int guess[11][4];

      int hints[11][2];

      //read in guesses and hints
      for(int i=0; i<g; i++) {
	 int num;
	 cin >> num;
	 getArr(num, guess[i]);

	 char ch;

	 cin >> hints[i][0] >> ch >> hints[i][1];

      }

      //ans to problem
      int ans[4];
      //number of correct solutions
      int numAns=0;

      //try all numbers from 0 to 10000
      for(int i=0; i<10000; i++) {
	 int test[4];
	 getArr(i, test);

	 if(compute(g, test, guess, hints)) {
	    numAns++;
	    for(int j=0; j<4; j++)
	       ans[j]=test[j];
	 }
      }
      //output answer
      if(numAns==0) {
		
	 cout << "impossible" << endl;
		
      } else if(numAns==1) {

	 for(auto a : ans)
	    cout << a;
	 cout << endl;

      } else {
	 cout << "indeterminate" << endl;
      }
   }


   return 0;
}
