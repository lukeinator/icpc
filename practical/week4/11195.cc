//Lukas Grasse - Feb 2015
//Problem: n queens
//
//Approach: Fix rows and cols and try every permutation of calls.
//stop trying children permutations if a digit is wrong

//resources used
//competitive programming 3
//http://stackoverflow.com/questions/7537791/understanding-recursion-to-generate-permutations
#include <iostream>
#include <algorithm>
using namespace std;


int board[16];
char bad[16][16];
bool ldiag[50];
bool rdiag[50];

//check diagonals and bad squares
bool check(int i) {
   //diagonals
   if(rdiag[16+i-board[i]])
      return false;
		

   if(ldiag[i+board[i]])
      return false;
		
   //check bad
   if(bad[board[i]][i]=='*')
      return false;

   return true;
}

//recursively try permutations, skipping if check fails
int compute(int index, int sz) {
   int sols=0;
   //stopping case
   if(index==sz)
      return sols+1;

   //try swapping each digit to current index
   for(int i=index; i<sz; i++) {
      //initial swap
      swap(board[index], board[i]);

      //if failing swap, skip subpermutations
      if(!check(index)) {
	 swap(board[index], board[i]);
	 continue;
      }

      //set diagonal bools to true
      rdiag[16+index-board[index]]=true;
      ldiag[index+board[index]]=true;

      //recurse
      sols += compute(index+1, sz);

      //reset bools and swap back
      rdiag[16+index-board[index]]=false;
      ldiag[index+board[index]]=false;
      swap(board[index], board[i]);

   }

   return sols;
}


int main() {
   int count=1;

   //while cases arent 0
   int sz;
   while(cin >> sz) {
      if(sz==0)
	 break;

      //read in bad array
      for(int i=0; i<sz; i++) 
	 for(int j=0; j<sz; j++)
	    cin >> bad[i][j];
		
	
      //initialize board
      for(int i=0; i<sz; i++) {
	 board[i]=i;
      }


      //output answer
      cout << "Case " << count << ": " << compute(0, sz) << endl;
      //increase casenum
      count++;
	

	
   }
   return 0;
}
