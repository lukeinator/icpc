//Lukas Grasse - Feb 2015
//Problem: Wifi
//
//Approach: Binary search whether or not all the houses can be covered
//given a number of routers and the house distances. find least amount 
//of distance required.

//resources used:
//Howard's code from lab
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
using namespace std;

//returns whether or not n routers with a dist range can cover
//all the houses
bool calc(double dist, vector<int> houses, int n) {
   int loc=0;
   double range;
   //place routers
   while(n>0) {
      range = houses[loc] + 2*dist;

      while(houses[loc]<range&&range<houses[houses.size()-1]) {
	 loc++;

      }

      n--;
   }
   //if we reached end then it worked
   if(range>=houses[houses.size()-1]) {
      return true;
   }
   //didnt work
   return false;
}

int main() {
   int cases;
   cin >> cases;

   //cases
   for(int a=1; a<=cases; a++) {
      vector<int> houses;
      int m, n;
      cin >> n >> m;

      //read in houses
      for(int i=0; i<m; i++) {
	 int h;
	 cin >> h;
	 houses.push_back(h);
      }
      //sort houses initially
      sort(houses.begin(), houses.end());

      //try largest possible as largest
      double lo=0, hi = houses[houses.size()-1]-houses[0];

      //binary search
      while(hi-lo>0.01) {
	 double mid = (hi+lo)/2;

	 if(!calc(mid, houses, n)) {
	    lo = mid;
	 } else {
	    hi = mid;
	 }
      }
      //output answer
      cout << fixed << setprecision(1) << hi << endl;
   }
   return 0;
}
