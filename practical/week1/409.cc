#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cctype>
#include <algorithm>
using namespace std;

int main() {
	
	int defNum, exNum;

	int cases=1;


	while(cin >> defNum >> exNum) {

	vector<string> keywords;
	vector<string> sentences;
	vector<string> formatted;


	string discard;
	getline(cin, discard);


	//read in keywords
	for(int i=0; i<defNum; i++) {
		string s;
		getline(cin, s);
		keywords.push_back(s);
	}

	//read in sentences
	for(int i=0; i<exNum; i++) {
		string s;
		getline(cin, s);
		sentences.push_back(s);
	}


	//format sentences
	for(int i=0; i<exNum; i++) {
		string s = sentences[i];


		for(int a=0; a<s.length(); a++) {
			if(!isalpha(s[a]))
				s[a] = ' ';
		}

		

		for(int k=0; k<s.length(); k++)
			s.at(k) = tolower(s.at(k));

		formatted.push_back(s);
	}

	
	//calculate worst excuses
	int worst=0;
	vector<int> locs;

	for(int i=0; i<exNum; i++) {
		int counter=0;
		istringstream iss(formatted[i]);

		string s;
		while(iss>>s) {
			if(count(keywords.begin(), keywords.end(), s)>0)
				counter++;

		}
		if(counter>worst) {
			worst=counter;
			locs.clear();
			locs.push_back(i);
		} else if(counter==worst) {
			locs.push_back(i);
		}
	}

	//output all worst excuses
	cout << "Excuse Set #" << cases << endl;
	for(auto index : locs) {
	 cout << sentences[index] << endl;
	}
	cout << endl;
	cases++;
	}
	return 0;
}