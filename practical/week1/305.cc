#include <iostream>
#include <map>
using namespace std;


//based on Competitive Programming 3, pg 356, general case section
//using formula F(n, k) = (F(n-1, k)+k)%n with base case of
//number of good guys instead of single survivor
bool solve(int k, int kill, int m, int n) {

	if(n<=k)
		return true;

	if((kill+m-1)%n<k) 
		return false;

	kill=(kill+m-1)%n;

	n--;
	
	return(solve(k, kill, m, n));
}


int main() {

	int nums[15];

//calculate all values to 14 to table
	for(int k=0; k<15; k++) {

	int m = k+1;
	int n = 2*k; 

	while(!solve(k, 0, m, n)) {
		m++;
	}

	nums[k] = m;
}

//output values from table
	int sol;
	
	while(cin >> sol) {
	
	if(sol==0) 
		break;

	cout << nums[sol] << endl;
	}
	
	return 0;
}