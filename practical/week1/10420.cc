//Lukas Grasse - Sept-Jan 2014
//conquest 
#include <iostream>
#include <map>
#include <string>
using namespace std;

int main() {

int count;
cin >> count;

map<string, int> country;

for(int i=0; i<count; i++) {

string c1, junk;

cin >> c1;

//get rid of extra names
getline(cin, junk);

//start at 1, using if else
if(country.count(c1)==1)
	country[c1]++;

else
	country[c1]=1;


}

//c++11 print loop
for(auto& i : country)
	cout << i.first << " " << i.second << endl;

	
	return 0;
}