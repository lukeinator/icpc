//Lukas Grasse - Jan 2014
//Hashmat Problem

#include <iostream>
#include <algorithm>
using namespace std;

int main() {

long long them, us;

while(cin >> them >> us) {

//using swap, works with unsigned numbers as well
	if(them<us)
		swap(them, us);

	long long diff = them - us;

	cout << diff << endl;
}
	
	return 0;
}