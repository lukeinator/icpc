//Lukas Grasse - March 2015
//Problem: Bicoloring
//
//Approach: use bfs to see if graph is bipartite

#include <iostream>
#include <vector>
#include <map>
#include <queue>
using namespace std;

int main() {
   int n;
   while(cin>>n&&n!=0) {
//number of edges
      int l;
      cin >> l;

      vector< vector<int> > edges(1000);

//insert edges to adjacency list
      for(int i=0; i<l; i++) {
	 int a, b;
	 cin >> a >> b;

	 edges[a].push_back(b);

      }


      //bipartite flag
      bool bipartite = true;

      //seen nodes
      map<int, int> color;

      //queue for bfs
      queue<int> test;

      //first node
      color[0] = 0;
      test.push(0);

      //bfs
      while(!test.empty()) {
	 //get first element
	 int num=test.front();
	 test.pop();

	 //color adjacent nodes
	 for(int i=0; i<edges[num].size(); i++) {
			
	    //if seen and not bipartite
	    if(color.count(edges[num][i])!=0
	       &&color[edges[num][i]]!= 1 - color[num]) {
	       bipartite=false;
	       break;
	    }

	    //if already seen but still bipartite
	    if(color.count(edges[num][i])!=0)
	       continue;

	    //color node
	    color[edges[num][i]] = 1 - color[num];
	    test.push(edges[num][i]);

	 }

      }


      //output answer
      if(bipartite)
	 cout << "BICOLORABLE." << endl;
      else
	 cout << "NOT BICOLORABLE." << endl;



   }


   return 0;
}
