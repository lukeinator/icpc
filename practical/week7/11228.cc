//Lukas Grasse - March 2015
//Problem: Transportation System
//
//Approach: use minimum spanning tree to find distance and 
//count for entire graph and just ones within radius distance.
//Railroads are then the total answers minus the state answers.
//
//Resources used: mst function and adapted main from code library.

#include <cmath>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cassert>
#include <algorithm>
using namespace std;

//unionfind from code library
class UnionFind
{
   struct UF { int p; int rank; };

public:
   UnionFind(int n) {          // constructor
      howMany = n;
      uf = new UF[howMany];
      for (int i = 0; i < howMany; i++) {
	 uf[i].p = i;
	 uf[i].rank = 0;
      }
   }

   ~UnionFind() {
      delete[] uf;
   }

   int find(int x) { return find(uf,x); }        // for client use
      
   bool merge(int x, int y) {
      int res1, res2;
      res1 = find(uf, x);
      res2 = find(uf, y);
      if (res1 != res2) {
	 if (uf[res1].rank > uf[res2].rank) {
	    uf[res2].p = res1;
	 }
	 else {
	    uf[res1].p = res2;
	    if (uf[res1].rank == uf[res2].rank) {
	       uf[res2].rank++;
	    }
	 }
	 return true;
      }
      return false;
   }
      
private:
   int howMany;
   UF* uf;

   int find(UF uf[], int x) {             // for internal use
      if (uf[x].p != x) {
	 uf[x].p = find(uf, uf[x].p);
      }
      return uf[x].p;
   }
};

//edge class from code library
class Edge {

public:
   Edge(int i=-1, int j=-1, double weight=0) {
      v1 = i;
      v2 = j;
      w = weight;
   }
   bool operator<(const Edge& e) const { return w < e.w; }

   int v1, v2;          /* two endpoints of edge                */
   double w;            /* weight, can be double instead of int */
};


//minimum spanning tree from code library
double mst(int n, int m, Edge elist[], int index[], int& size)
{
   UnionFind uf(n);

   sort(elist, elist+m);

   double w = 0.0;
   size = 0;
   for (int i = 0; i < m && size < n-1; i++) {
      int c1 = uf.find(elist[i].v1);
      int c2 = uf.find(elist[i].v2);
      if (c1 != c2) {
	 index[size++] = i;
	 w += elist[i].w;
	 uf.merge(c1, c2);
      }
   }

   return w;
}

int main(void)
{

   //read in cases
   int cases;
   cin >> cases;

   for(int a=0; a<cases; a++) {

      cout << fixed << setprecision(0);

      //read in cities and locations
      //n is cities, r is distance to be in same state
      int n, r;
      cin >> n >> r;
      double* x = new double[n];
      double* y = new double[n];
      int* index1 = new int[n];
      int* index2 = new int[n];

      for (int i = 0; i < n; i++)  cin >> x[i] >> y[i];
   
      //edges for states and edges for rails and states together
      Edge* statelist = new Edge[n*n];
      Edge* raillist = new Edge[n*n];

      int k1 = 0;
      int k2 = 0;

      //insert roads if below radius, insert everything to railroads
      for (int i = 0; i < n; i++) { 
	 for (int j = i+1; j < n; j++) { 
	 	
	    //if distance is within radius
	    if((x[i]-x[j])*(x[i]-x[j])+ (y[i]-y[j])*(y[i]-y[j])<r*r)
	       statelist[k1++] = Edge(i,j,hypot(x[i]-x[j], y[i]-y[j]) );
   		
	    //insert all to railroads
	    raillist[k2++] = Edge(i,j,hypot(x[i]-x[j], y[i]-y[j]) );
   	
	 }
      }

      //states distance and count
      double t1; 
      int s1;
      t1 = mst(n, k1, statelist, index1, s1);

      //states and railroads combined distance and count
      double t2;
      int s2;
      t2 = mst(n, k2, raillist, index2, s2);
   
      //cases
      cout << "Case #" << a+1 << ": " 
   
	 //total size - railroad size gives size of railroads
	 //which is 1 less than number of states
	   << s2 - s1 + 1 << " " 
   
	 //number of states and total - number of states for railroads
	   << t1 << " " << t2-t1  << endl;

   }
   return 0;
}
