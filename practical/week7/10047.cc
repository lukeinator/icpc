//Lukas Grasse - March 2015
//Problem: The Monocycle
//
//Approach: use bfs to see if the goal square can be reached
//
//Resources used: extra test cases from comment on forum
//http://online-judge.uva.es/board/viewtopic.php?f=9&t=4335

#include <iostream>
#include <queue>
#include <tuple>
#include <map>
using namespace std;

//struct for each square
struct square{
	
   int row;
	
   int col;
	
   //direction
   int dir;
	
   int color;

   //comparison
   friend bool operator <(const square& x, const square& y) {
      return std::tie(x.row, x.col, x.dir, x.color) < 
	 std::tie(y.row, y.col, y.dir, y.color);
   }

   //equality
   friend bool operator ==(const square &l, const square &r) { 
      return (l.row==r.row&&l.col==r.col&&l.dir==r.dir&&l.color==r.color); 
   }
};



int main() {

   int row, col;

   int cases=1;
   while(cin>>row>>col&&row!=0) {

      //space between cases
      if(cases>1)
	 cout << endl;

      //squares that cant be visited
      bool block[26][26] = {false};

      //start and end positions
      int srow, scol, trow, tcol;


      //read in grid and set start, end and block squares 
      for(int i=0; i<row; i++) {
	 for(int j=0; j<col; j++) {
	    char ch;
	    cin >> ch;

	    if(ch=='S') {
	       srow = i;
	       scol = j;
	    }

	    if(ch=='T') {
	       trow = i;
	       tcol = j;
	    }

	    if(ch=='#') {
	       block[i][j]=true;
	    }

	 }
      }

      //queue for bfs
      queue<square> board;
      square b;

      //directions clockwise from north
      int dr[4] = {-1, 0, 1, 0};
      int dc[4] = {0, 1, 0, -1};

      //starting square
      b.row = srow;
      b.col = scol;
      b.dir = 0;
      b.color = 0;

      //insert starting square
      board.push(b);

      //seen starting square
      map<square, int> seen;
      seen[b]=0;
	
      //found flag
      bool found=false;
      //solution
      int sol;

      while(!board.empty()) {
	 //get top element
	 square s = board.front();
	 board.pop();

	 //check for solution
	 if(s.row==trow&&s.col==tcol&&s.color==0) {
	    found=true;
	    sol=seen[s];
	    break;
	 }

	 //square for comparison
	 square test;
		
	 //check moving forward is in bounds
	 if(s.row+dr[s.dir]>=0&&s.row+dr[s.dir]<row&&
	    s.col+dc[s.dir]>=0&&s.col+dc[s.dir]<col) {

	    //set test to one square forward
	    test.row = s.row+dr[s.dir];
	    test.col = s.col+dc[s.dir];
	    test.dir = s.dir;
	    test.color = (s.color+1)%5;

	    //if not seen, insert to queue
	    if(seen.count(test)==0&&block[test.row][test.col]==false) {
	       board.push(test);
	       seen[test] = seen[s]+1;

	    }
	 }

	 // set test to turning left
	 test.row = s.row;
	 test.col = s.col;
	 test.dir = (s.dir+3)%4;
	 test.color = s.color;

	 //if not seen, insert into queue
	 if(seen.count(test)==0&&block[test.row][test.col]==false) {
	    board.push(test);
	    seen[test] = seen[s]+1;
	
	 }

	 //set test to turning right
	 test.row = s.row;
	 test.col = s.col;
	 test.dir = (s.dir+1)%4;
	 test.color = s.color;

	 //if not seen, insert into queue
	 if(seen.count(test)==0&&block[test.row][test.col]==false) {
	    board.push(test);
	    seen[test] = seen[s]+1;
	 }
	

      }

      //output case num
      cout << "Case #" << cases << endl;
      cases++;


      //output solution
      if(found)
	 cout << "minimum time = " << sol << " sec" << endl;
      else
	 cout << "destination not reachable" << endl;



   }

   return 0;
}
