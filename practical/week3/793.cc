//Lukas Grasse - Feb 2015
//Problem: Network connections
//
//Approach: Use unionfind to connect computers and query for
//successful and unsuccessful connections

// UnionFind class -- based on Howard Cheng's C code for UnionFind
// Modified to use C++ by Rex Forsyth, Oct 22, 2003
//
// Constuctor -- builds a UnionFind object of size n and initializes it
// find -- return index of x in the UnionFind
// merge -- updates relationship between x and y in the UnionFind
#include <iostream>
#include <sstream>
using namespace std;

class UnionFind
{
   struct UF { int p; int rank; };

public:
   UnionFind(int n) {          // constructor
      howMany = n;
      uf = new UF[howMany];
      for (int i = 0; i < howMany; i++) {
	 uf[i].p = i;
	 uf[i].rank = 0;
      }
   }

   ~UnionFind() {
      delete[] uf;
   }

   int find(int x) { return find(uf,x); }        // for client use
      
   bool merge(int x, int y) {
      int res1, res2;
      res1 = find(uf, x);
      res2 = find(uf, y);
      if (res1 != res2) {
	 if (uf[res1].rank > uf[res2].rank) {
	    uf[res2].p = res1;
	 }
	 else {
	    uf[res1].p = res2;
	    if (uf[res1].rank == uf[res2].rank) {
	       uf[res2].rank++;
	    }
	 }
	 return true;
      }
      return false;
   }
      
private:
   int howMany;
   UF* uf;

   int find(UF uf[], int x) {     // recursive funcion for internal use
      if (uf[x].p != x) {
	 uf[x].p = find(uf, uf[x].p);
      }
      return uf[x].p;
   }
};


int main() {

   //read in cases
   int cases;
   cin >> cases;

   //get rid of \n from cin
   string s;
   getline(cin, s);
	

   for(int a=0; a<cases; a++) {

      //get rid of extra line
      getline(cin, s);

      //connected and not connected
      int c=0, n=0;

      //unionfind
      UnionFind network(1000000);

      char ch;
      int x, y;

      //while we can read in a line and it isnt empty or too small
      while(getline(cin, s)&&s.length()>1) {
	
	 //istringstream to convert to number
	 istringstream iss(s);
	 iss>> ch >> x >> y;

	 //if query, check connection and update c or n
	 if(ch=='q') {
	    if(network.find(x)==network.find(y))
	       c++;
	    else
	       n++;
	    //connect computers in network
	 } else {
	    network.merge(x, y);
	 }

      }
      // output answer
      cout << c << "," << n;
      if(a<cases-1)
	 cout << endl;
      cout << endl;
   }


   return 0;
}
