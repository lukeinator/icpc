//Lukas Grasse - Feb 2015
//Problem: BlackBox
//
//Approach: Used two priority queues to keep track
//of median element. All elements are read into a
//normal queue and then added to the group of 
//priority queues as the get ints are read in.
//the priority queues are balanced using the
//balance function before the median element is accessed
#include <iostream>
#include <algorithm>
#include <queue>
#include <functional>
#include <vector>

using namespace std;

//balance function. balances elements so 
//bottom contains i-1 elements
void balance(priority_queue<int>& bottom, 
	     priority_queue< int, vector<int>, 
	     greater<int> >& top, int i) {

   //balance difference in largest 
   //bottom number and lowest top number
   while(!bottom.empty()&&!top.empty()&&bottom.top()>top.top()) {
      int t = top.top();
      int b = bottom.top();

      top.pop();
      bottom.pop();

      top.push(b);
      bottom.push(t);
   }

   //adjust for size i
   while(bottom.size()<i-1) {
      bottom.push(top.top());
      top.pop();
   }


}

int main() {

   int cases;
   cin >> cases;

   //loop through cases
   for(int a=0; a<cases; a++) {

      priority_queue<int> bottom;
      priority_queue<int, vector<int>, greater<int> > top;
      queue<int> nums;

      int m, n, count=0;

      cin >> m >> n;

      //read in m elements to normal queue
      for(int i=0; i<m; i++) {
	 int num;
	 cin >> num;
	 nums.push(num);
      }

      //loop through get nums
      for(int i=1; i<=n; i++) {
	 int get;
	 cin >> get;

	 //add correct num of elements
	 while(count<get) {
	    count++;
	    if(!nums.empty()) {
	       top.push(nums.front());
	       nums.pop();
	    }
	 }
		
	 //balance queues
	 balance(bottom, top, i);

	 //output i'th element
	 cout << top.top() << endl;

      }

      if(a<cases-1)
	 cout << endl;
   }

   return 0;
}
