//Lukas Grasse - Feb 2015
//Problem: hoax or what
//
//Approach: For each day, insert the bills
//to a multiset. Add difference between 
//beginning and end to sum, and delete from
//multiset. At end, output sum. 
#include <iostream>
#include <set>

using namespace std;

int main() {
//read in days
   int days;
   cin>>days;
   while(days!=0) {
      long long paid=0;

      multiset<int> bills;

		
		
      //loop through days
      for(int i=0; i<days; i++) {
	 int billNum;
	 cin >> billNum;
			
	 //add bills to multiset
	 for(int j=0; j<billNum; j++) {
	    int bill;
	    cin >> bill;
	    bills.insert(bill);
	 }
		
	 long long b, e;

	 //get largest and smallest bill
	 b = (*(bills.begin()));
	 e = (*(--bills.end()));

	 //delete largest and smallest bill from set
	 bills.erase(bills.begin());
	 bills.erase(--bills.end());

	 //add difference between largest and smallest bill
	 paid += (e-b);

      }
      //output sum
      cout << paid << endl;
		
      //read in next set of days
      cin >> days;
   }

   return 0;
}
