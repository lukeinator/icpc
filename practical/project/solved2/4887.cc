
//Lukas Grasse - April 2015
//Problem: Soccer
//
//Approach: Try all unplayed matches, and for each player, if ranking is better
//than best, or worse than worst update best and worst arrays

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

		int best[25];
		int worst[25];

//returns ending for number
string suffix(int a) {
	if(a==1)
		return "st";
	if(a==2)
		return "nd";
	if(a==3)
		return "rd";
	return "th";
}

//used to sort scores
vector< pair<int, int> > sorter;


//tries all possible outcomes of match, and updates best and worst
void calc(int score[20], int unsure[20][2], int u, int n) {

	//we tried a specific outcome
	if(u==0) {
		
		//insert all scores to sorter
		for(int i=0; i<n; i++)
			sorter[i] = make_pair(score[i], i);

		//sort vector
		sort(sorter.begin(), sorter.end());

		int i=1, j=1;

		int b=1000, w=0;

		//update best and worst
		for(int a=n-1; a>=0; a--) {
			
			//keep track of ties
			if(sorter[a].first<b) {
				b=sorter[a].first;
				i=j;
			}

			j++;

			//update best and worst
			best[sorter[a].second] = min(best[sorter[a].second], i);
			worst[sorter[a].second] = max(worst[sorter[a].second], i);

		}
		return;
	}

	//recursively try all possible outcomes for next match
	int e1 = unsure[u-1][0];
	int e2 = unsure[u-1][1];

	//win
	score[e1] += 3;
	calc(score, unsure, u-1, n);
	score[e1] -= 3;

	//lose
	score[e2] += 3;
	calc(score, unsure, u-1, n);
	score[e2] -= 3;

	//tie
	score[e1] += 1;
	score[e2] += 1;
	calc(score, unsure, u-1, n);
	score[e1] -= 1;
	score[e2] -= 1;

}

int main() {
	int n, m;


	bool space=false;

	//main loop
	while(cin>>n>>m&&n!=0&&m!=0) {
	fill(best, best+25, 0);
	fill(worst, worst+25, 0);

	//resize to num of teams
	sorter.resize(n);
	
		if(space)
		cout << endl;
		
		space=true;


		string teams[21];
		int games[1005];
		int score[1005];

		//map each name to index of team
		map<string, int> index;


		int unsure[1005][2];

		//read in team and set index and score
		for(int i=0; i<n; i++) {
			cin >> teams[i];
			score[i]=0;
			best[i]=1000;
			index[teams[i]] = i;

		}

		int u=0;

		//read in games
		for(int i=0; i<m; i++) {
			string a, x, b;
			cin >> a >> x >> b;

			//remove colon
			b = b.substr(0, b.length()-1);

			//read in scores
			int aI, bI;
			cin >> aI >> bI;

			//win
			if(aI>bI)
				score[index[a]]+=3;
			
			//loss
			else if(aI<bI)
				score[index[b]]+=3;
			
			//not played
			else if(aI==-1&&bI==-1) {
				unsure[u][0]=index[a];
				 unsure[u++][1]=index[b];
			
			//tie
			} else {
				score[index[a]]++;
				score[index[b]]++;
			}

		}

		//run simulation
		calc(score, unsure, u, n);

		//output answer
		for(int i=0; i<n; i++)
			cout << "Team " << teams[i] 
				<<  " can finish as high as " << best[i] << suffix(best[i])
				<< 	" place and as low as " <<  worst[i] << suffix(worst[i])
				<<  " place." << endl;


	}


	return 0;
}