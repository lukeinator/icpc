//Lukas Grasse - April 2015
//Problem: User Names
//
//Approach: Assign names based on rules, and check already made names
//each time rules are checked

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> unames;

string getUser(string first, string last, int maxLen) {
   string u;
   //format first
   for(int a=0; a<first.length(); a++)  {
      if(first[a]=='\''||first[a]=='-') {
	 first.erase(a, 1);
	 a--;
      }
      first[a] = tolower(first[a]);
   }
		
   //format last
   for(int a=0; a<last.length(); a++) {
      if(last[a]=='\''||last[a]=='-') {
	 last.erase(a, 1);
	 a--;
      }
      last[a] = tolower(last[a]);
   }
		

   //try default username
   u = first[0]+last.substr(0, maxLen-1);

   //if already taken
   if(count(unames.begin(), unames.end(), u)!=0) {
      for(int i=0; i<99; i++) {
	 //try first 9 names available from rule 4
	 if(i<9) {
	    if(u.length()==maxLen)
	       u = first[0]+last.substr(0, maxLen-2)+to_string(i+1);
	    else
	       u = first[0]+last.substr(0, maxLen-1)+to_string(i+1);
				
	    //try names available from rule 5
	 } else {
	    if(u.length()==maxLen)
	       u = first[0]+last.substr(0, maxLen-3)+to_string(i+1);
	    else if(u.length()==maxLen-1)
	       u = first[0]+last.substr(0, maxLen-2)+to_string(i+1);
	    else
	       u = first[0]+last.substr(0, maxLen-1)+to_string(i+1);
	 }

	 //add name to vector and return new username
	 if(count(unames.begin(), unames.end(), u)==0) {
	    unames.push_back(u);
	    return u;
	 }
      }
   }

   //add name to vector and return new username
   unames.push_back(u);
   return u;
}

int main() {

   int num, maxLen, cases=1;
	
   //get names and max length
   while(cin >> num >> maxLen&&num!=0&&maxLen!=0) {
      cout << "Case " << cases << endl;
      string name;

      //throw away
      getline(cin, name);

      for(int i=0; i<num; i++) {

	 //get name
	 getline(cin, name);

	 istringstream iss(name);

	 string first, last;

	 iss >> first;

	 //get rid of middle names
	 while(iss >> last);

	 //output username
	 cout << getUser(first, last, maxLen) << endl;
      }

      cases++;
      unames.clear();
   }
   return 0;
}
