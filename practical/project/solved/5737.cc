//Lukas Grasse - April 2015
//Problem: Pills 
//
//Approach: Recursively try taking halves or break up a whole
// and sum up the total
#include <iostream>
#include <map>
#include <utility>
using namespace std;

//ones already seen
map< pair<int, int>, long long > seen;


long long pills(int whole, int half) {
   //if no whole yet there is only one way to take pills
   if(whole==0)
      return 1;

   //if seen return answer
   if(seen.count(make_pair(whole, half))!=0)
      return seen[make_pair(whole, half)];

   long long sol=0;

   //take two halves in two days if we can and also
   //break a whole into two halves
   if(half>0)
      sol+=pills(whole, half-2);

   sol+=pills(whole-1, half+2);

   seen[make_pair(whole, half)] = sol;
   return sol;
}

int main() {

   int test;

   //output answer
   while(cin>>test&&test!=0) 
      cout << pills(test, 0) << endl;

   return 0;
}
