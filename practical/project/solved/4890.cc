//Lukas Grasse - April 2015
//Problem: Aronson
//
//Approach: find cardinal and ordinal string of 2 digit values
// and use this to figure out hundreds, and thousands, and
//use hundreds with thousands for hundreds of thousands.
//Then simulate sequence using function to get the ordinal
//string.
#include <iostream>
#include <string>
#include <map>
#include <vector>
using namespace std;

map<int, string> card;
map<int, string> ord;

void init();

//get two digit cardinal
string getCard(int a) {
   if(a>=20)
      return card[(a/10)*10] + card[a%10];
   else
      return card[a];
}

//get two digit ordinal
string getOrd(int a) {
   if(a>20&&a%10!=0)
      return card[(a/10)*10] + ord[a%10];
   else
      return ord[a];
}

//get hundreds cardinal
string getH(int a) {
   string s;

   if(a/100!=0)
      s += getCard(a/100) + getCard(100);

   a%=100;

   s += getCard(a);

   return s;
}

string getStr(int a) {
   string s;

   //thousands
   if(a/1000!=0){
      //if it ends at thousands use ordinal at end otherwise cardinal
      s += getH(a/1000) +(a%1000 != 0 ? getCard(1000) : getOrd(1000));
   }

   a%=1000;

   if(a/100!=0)
      //if it ends at hundreds use ordinal at end otherwise cardinal
      s += getCard(a/100) + (a%100 != 0 ? getCard(100) : getOrd(100));

   a%=100;

   //if there is remaining digits, they are ordinal
   s += getOrd(a);

   return s;
}




int main() {
   init();

   //start string
   string s = " tisthe";

   vector<int> aronson;

   //simulate sequence to 1000000
   for(int i=0; i<=1000000; i++) {
      if(s[i]=='t') {
	 s += getStr(i);
	 aronson.push_back(i);
      }
   }

   int num;

   //lookup answer from table and output
   while(cin>>num&&num!=0)
      cout << aronson[num-1] << endl;

   return 0;
}

//initialize cardinal and ordinal tables
void init() {
//cardinal
   card[1] = "one";
   card[2] = "two";
   card[3] = "three";
   card[4] = "four";
   card[5] = "five";
   card[6] = "six";
   card[7] = "seven";
   card[8] = "eight";
   card[9] = "nine";
   card[10] = "ten";
   card[11] = "eleven";
   card[12] = "twelve";
   card[13] = "thirteen";
   card[14] = "fourteen";
   card[15] = "fifteen";
   card[16] = "sixteen";
   card[17] = "seventeen";
   card[18] = "eighteen";
   card[19] = "nineteen";
   card[20] = "twenty";
   card[30] = "thirty";
   card[40] = "forty";
   card[50] = "fifty";
   card[60] = "sixty";
   card[70] = "seventy";
   card[80] = "eighty";
   card[90] = "ninety";
   card[100] = "hundred";
   card[1000] = "thousand";

//ordinal
   ord[1] = "first";
   ord[2] = "second";
   ord[3] = "third";
   ord[4] = "fourth";
   ord[5] = "fifth";
   ord[6] = "sixth";
   ord[7] = "seventh";
   ord[8] = "eighth";
   ord[9] = "ninth";
   ord[10] = "tenth";
   ord[11] = "eleventh";
   ord[12] = "twelfth";
   ord[13] = "thirteenth";
   ord[14] = "fourteenth";
   ord[15] = "fifteenth";
   ord[16] = "sixteenth";
   ord[17] = "seventeenth";
   ord[18] = "eighteenth";
   ord[19] = "nineteenth";
   ord[20] = "twentieth";
   ord[30] = "thirtieth";
   ord[40] = "fortieth";
   ord[50] = "fiftieth";
   ord[60] = "sixtieth";
   ord[70] = "seventieth";
   ord[80] = "eightieth";
   ord[90] = "ninetieth";
   ord[100] = "hundredth";
   ord[1000] = "thousandth";

}
