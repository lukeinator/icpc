//Lukas Grasse - April 2015
//Problem: Ropes
//
//Approach: Simulate each rope and see if it works.

#include <iostream>
#include <algorithm>
using namespace std;

int main() {

//number of pitches
   int pnum;
   while(cin>>pnum&&pnum!=0) {

//types of rope
      int ropes[3] = {50, 60, 70};

      int p[100];
      int sum=0;
      int best=0;

//find longest pitch, and sum pitch lengths
      for(int i=0; i<pnum; i++) {
	 cin >> p[i];
	 sum+=p[i];
	 best=max(best, p[i]);
      }

//for each rope
      for(int i=0; i<3; i++) {
	 int ans=(ropes[i]/best)+1;

	 //if we cant drape it down both sides of top or
	 //less than two people can go then ans is 0
	 //otherwise ans is good
	 if(ans<2||ropes[i]<2*sum)
	    ans=0;

	 cout << ans;

	 if(i<2)
	    cout << " ";
      }

      cout << endl;
   }
   return 0;
}
