
//Lukas Grasse - April 2015
//Problem: Parenthesis
//
//Approach: Convert expression to Reverse Polish Notation
//then use a stack to convert string back to infix, while
//only adding minimum brackets
//
//Resources used: 
//http://en.wikipedia.org/wiki/Reverse_Polish_notation
//http://en.wikipedia.org/wiki/Shunting-yard_algorithm
//http://stackoverflow.com/questions/113424/converting-reverse-polish-notation


#include <iostream>
#include <string>
#include <cctype>
#include <stack>
#include <map>
#include <algorithm>
using namespace std;

//shunting algorithm based on pseudocode from wikepedia
string shunting(string s) {
   string sol;

   stack<char> os;

   for(int i=0; i<s.length(); i++) {
      if(isalpha(s.at(i)))
	 sol =sol +  s.at(i);

      else if(s.at(i)=='*') {
	 while(!os.empty()&&os.top()=='*') {
	    sol = sol +  os.top();
	    os.pop();
	 }
	 os.push(s.at(i));

      } else if(s.at(i)=='+') {
	 while(!os.empty()&&(os.top()=='*'||os.top()=='+')) {
	    sol = sol +  os.top();
	    os.pop();
	 }
	 os.push(s.at(i));
      } else if(s.at(i)=='(') {
	 os.push(s.at(i));
      } else if(s.at(i)==')') {
	 while(!os.empty()&&os.top()!='(') {
	    sol = sol +  os.top();
	    os.pop();
	 }
	 os.pop();
      }
   }

   while(!os.empty()) {
      sol = sol +  os.top();
      os.pop();
   }
   return sol;
}

//checks whether there is a + sign that isnt in parenthesis
bool isMin(string s) {
   int count=0;

   //for each character
   for(int i=0; i<s.length(); i++) {
  
      if(s[i]=='(')
	 count++;

      if(s[i]==')')
	 count--;

      //only returns true if not in parenthesis
      if(s[i]=='+'&&count==0)
	 return true;
   }

   return false;
}


int main() {

   string s;
   while(getline(cin, s)) {

      //add * sign where needed as operator
      for(int i=0; i<s.size()-1; i++) {
	 if((isalpha(s.at(i))||s.at(i)==')')
	    &&(isalpha(s.at(i+1))||s.at(i+1)=='('))
	    s.insert(i+1, 1, '*');
      }

      //run shunting algorithm
      s = shunting(s);

      stack<string> str;
  
      //convert back to infix
      for(int i=0; i<s.length(); i++) {
      
	 //if not an operator push to stack
	 if(s[i]!='+'&&s[i]!='*')
	    str.push(s.substr(i, 1));

	 string a, b;

	 //if plus sign concatenate top two on stack and push
	 //back to stack
	 if(s[i]=='+') {
	    a=str.top();
	    str.pop();

	    b=str.top();
	    str.pop();

	    str.push(b + '+' + a);
	 }

	 //if * sign do the same as plus, but check if brackets are
	 //needed
	 if(s[i]=='*') {
	    a=str.top();
	    str.pop();

	    //add brackets if needed
	    if(isMin(a))
	       a = "(" + a + ")";


	    b=str.top();
	    str.pop();

	    if(isMin(b))
	       b = "(" + b + ")";

	    str.push(b + '*' + a);
	 }

      }

      string ans;

      //remove * symbols
      for(int i=0; i<str.top().length(); i++)
	 if(str.top()[i]!='*')
	    ans += str.top()[i];

      //output answer
      cout << ans <<endl;
   }

   return 0;
}
