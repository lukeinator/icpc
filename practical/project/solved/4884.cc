//Lukas Grasse - April 2015
//Problem: Chain Code
//
//Approach: Use picks theorem and area of a polygon, to find
//number of points inside chain code and add this with perimeter
//points to find answer
//
//Resources Used: Area of a Polygon from code library
//                Picks theorem from wikepedia
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>
#include <vector>
#include <utility>

using namespace std;

vector< pair<int, int> > offsets;

struct Point {
   double x, y;
};

double area_polygon(Point polygon[], int n)
{
   double sum = 0.0;

   for (int i = 0; i < n-1; i++) {
      sum += polygon[i].x * polygon[i+1].y - polygon[i].y * polygon[i+1].x;
   }
   sum += polygon[n-1].x * polygon[0].y - polygon[n-1].y * polygon[0].x;
   return sum/2.0;
}



int main(void)
{

   //offsets for chain code
   int dx[8] = {1, 1, 0, -1, -1, -1,  0, 1};
   int dy[8] = {0, 1, 1,  1,  0, -1, -1, -1};


   Point *polygon;
   string points;

   while (getline(cin, points)) {
      polygon = new Point[1000005];
      assert(polygon);

      int x=0, y=0;

      //first point at origin
      polygon[0].x = 0;
      polygon[0].y = 0;

      //create points for each perimeter location and add to polygon
      for (int i = 0; i < points.length(); i++) {
	 x = x+dx[points[i]-'0'];
	 y = y+dy[points[i]-'0'];

	 polygon[i+1].x = x;
	 polygon[i+1].y = y; 
      }

      //calculate inner points using picks theorem
      double ans = area_polygon(polygon, points.length()) + 1
	 - points.length()/2.0;

      //add on the perimiter nodes
      ans += points.length();

      //output solution to 0 decimal places
      cout << fixed << setprecision(0) << ans << endl;
      delete[] polygon;
   }
   return 0;
}
