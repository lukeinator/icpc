//Lukas Grasse - April 2015
//Problem: Post Office
//
//Approach: Read in dimensions and compare to rules to put
//into a category. Sizes are 100x to take care of decimal 
//in letter thickness.
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;

//dimensions of types of mailables
int dimensions[3][6] = {
   {12500, 9000, 25, 29000, 15500, 700},
   {12500, 9000, 25, 38000, 30000, 5000},
   {12500, 9000, 25, 0, 0, 0}
};

//test if letter
bool testLetter(int thickness, int height, int length) {
   //too small
   if(length<dimensions[0][0]
      ||height<dimensions[0][1]
      ||thickness<dimensions[0][2])
      return false;

   //too big
   if(length>dimensions[0][3]
      ||height>dimensions[0][4]
      ||thickness>dimensions[0][5])
      return false;

   //is letter
   return true;
	
}

//test if packet
bool testPacket(int thickness, int height, int length) {
   //too small
   if(length<dimensions[1][0]
      ||height<dimensions[1][1]
      ||thickness<dimensions[1][2])
      return false;

   //too big
   if(length>dimensions[1][3]
      ||height>dimensions[1][4]
      ||thickness>dimensions[1][5])
      return false;

   //also has one side bigger than letter
   if(length>dimensions[0][3]
      ||height>dimensions[0][4]
      ||thickness>dimensions[0][5])
      return true;


   return false;

}

//test if parcel
bool testParcel(int thickness, int height, int length) {
   //too small
   if(length<dimensions[2][0]
      ||height<dimensions[2][1]
      ||thickness<dimensions[2][2])
      return false;

   //girth too big
   if((2*thickness+2*height+length)>210000)
      return false;

   //one side must be bigger than letter
   if(length>dimensions[1][3]
      ||height>dimensions[1][4]
      ||thickness>dimensions[1][5])
      return true;

   return false;

}

int main() {

   double tests[3];

//read in dimensions and fix decimal
   while(cin>>tests[0]>>tests[1]>>tests[2]&&
	 !(tests[0]==0&&tests[1]==0&&tests[2]==0)) {
      int dimensions[3];

      for(int i=0; i<3; i++) {
	 dimensions[i] = static_cast<int>(tests[i]*100);
	
      }

      //sort so we know what side is what
      sort(dimensions, dimensions+3);

      //do tests
      if(testLetter(dimensions[0], dimensions[1], dimensions[2]))
	 cout << "letter" << endl;
      else if(testPacket(dimensions[0], dimensions[1], dimensions[2]))
	 cout << "packet" << endl;
      else if(testParcel(dimensions[0], dimensions[1], dimensions[2]))
	 cout << "parcel" << endl;
      else 
	 cout << "not mailable" << endl;

   }


   return 0;
}
