//Lukas Grasse - April 2015
//Problem: Iterated Difference
//
//Approach: Simulate up to 1000 changes, and if we get to number
//output answer
#include <iostream>
#include <cstdlib>
using namespace std;

int main() {

   int nums[20];
   int s;

   int cases=1;
   while(cin >> s&&s!=0) {


      for(int i=0; i<s; i++)
	 cin >> nums[i];

      bool found=false;
      int ans=0;

      //for 1000 plus starting config
      for(int i=0; i<1001; i++) {
		
	 //for each number
	 for(int j=0; j<s-1; j++) {
			
	    // if not found stop checking
	    if(nums[j]!=nums[j+1])
	       break;
			
	    //if at end and correct, we found answer
	    if(j==s-2) {
	       found=true;
	       ans=i;
	    }

	 }
	 //if found stop iterating
	 if(found)
	    break;

	 //generate next iteration
	 int old=nums[0];
	 for(int j=0; j<s-1; j++) 
	    nums[j] = abs(nums[j]-nums[j+1]);
		 
	 nums[s-1] = abs(nums[s-1]-old);
	
      }

      //output answer or not attained if we didnt reach it
      cout << "Case " << cases << ": ";

      if(!found)
	 cout << "not attained" << endl;
      else 
	 cout << ans << " iterations" << endl;

      cases++;
   }


   return 0;
}
