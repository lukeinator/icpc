//Lukas Grasse - April 2015
//Problem: Page Count
//
//Approach: use boolean array of pages and set them to true if
//there is a page there. Then, count all the pages set to true
#include <iostream>
#include <sstream>
using namespace std;

int main() {
   int pagesNum;
   while(cin>>pagesNum&&pagesNum!=0) {
      bool pages[1000] = {false};

      string s;
      //throw away
      getline(cin, s);

      //get line
      getline(cin, s);
      istringstream iss(s);

      //start page and possibly end page
      int page, page2;
      char ch;

      //try reading in with string stream
      //to see if one page or multiple
      while(iss>>page) {
	 iss>>ch;

	 //multiple. set to true
	 if(ch=='-') {
	    iss>>page2;
	    for(int i=page; i<=page2; i++) 
	       pages[i]=true;
	    iss >> ch;

	    //individual set to true
	 } else {
	    pages[page] = true;
	 }

      }

      //sum up pages
      int sum=0;
      for(int i=1; i<=pagesNum; i++) {
	 if(pages[i]){
	    sum++;
	 }
      }

      //output sum
      cout << sum << endl;

   }

   return 0;
}
