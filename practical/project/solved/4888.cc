//Lukas Grasse - April 2015
//Problem: Railroad
//
//Approach: Use dynamic programming to try taking train from 
// left or right if both are possible. Otherwise if there is only one
//option, do the option.
#include <iostream>
#include <map>
#include <tuple>
using namespace std;

//which ones already seen
map< tuple<int, int, int>, bool> seen;

bool compute(int n1pos, int n2pos, int testpos, int n1, int n2, 
	     int car1[1000],int car2[1000], int test[1000000]) {
	
   //got to end and found answer
   if(n1pos==n1&&n2pos==n2)
      return true;

   //already seen
   if(seen.count(make_tuple(n1pos, n2pos, testpos))!=0)
      return seen[make_tuple(n1pos, n2pos, testpos)];

   //neither option works
   if(car1[n1pos]!=test[testpos]&&car2[n2pos]!=test[testpos])
      return false;


   bool test1=false;
   bool test2=false;

   //try left
   if(car1[n1pos]==test[testpos])
      test1=compute(n1pos+1, n2pos, testpos+1, n1, n2, 
		    car1, car2, test);

   //try right
   if(car2[n2pos]==test[testpos])
      test2=compute(n1pos, n2pos+1, testpos+1, n1, n2, 
		    car1, car2, test);

   //mark as seen and return
   seen[make_tuple(n1pos, n2pos, testpos)]=(test1||test2);
   return (test1||test2);
}

int main() {
	
   int n1, n2;
   while(cin>>n1>>n2&&n1!=0&&n2!=0) {
      int car1[1000];
      int car2[1000];
      int test[1000000];

      //read in cars and test array
      for(int i=0; i< n1; i++)
	 cin >> car1[i];

      for(int i=0; i< n2; i++)
	 cin >> car2[i];

      for(int i=0; i< n1+n2; i++)
	 cin >> test[i];

      //output answer
      if(compute(0, 0, 0, n1, n2, car1, car2, test))
	 cout << "possible" << endl;
      else 
	 cout << "not possible" << endl;

      seen.clear();
   }

   return 0;
}
