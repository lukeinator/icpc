//Lukas Grasse - April 2015
//Problem: Stock Prices
//
//Approach: use multimap to sort stock prices and keep track of
//their indexes. Choose the k1 lowest and k2 highest and sort
//and output them.
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

//comp for reverse sort
bool comp(int i,int j) { return (i>j); }

int main() {

   int days, k1, k2;
   int cases=1;
   while(cin>>days>>k1>>k2&&days!=0) { 

      //map of price to day it happened
      multimap<int, int> stocks;

      //read in prices
      for(int i=0; i<days; i++) {
	 int p;
	 cin>>p;

	 stocks.insert(make_pair(p, i));
      }

      //start and end of map
      auto startPos = stocks.begin();
      auto endPos = stocks.rend();

      //output case num
      cout << "Case " << cases <<endl;
      cases++;

      vector<int> lo;
      vector<int> hi;

      //get lowest
      for(int i=0; i<k1; i++) 
	 lo.push_back((*(startPos++)).second+1);

      //get highest
      for(int i=0; i<k2; i++) 
	 hi.push_back((*(++endPos)).second+1);
		
      //sort lowest and highest
      sort(lo.begin(), lo.end());
      sort(hi.begin(), hi.end(), comp);

      //output lowest
      for(int i=0; i<lo.size(); i++) {
	 cout << lo[i];
	 if(i<lo.size()-1)
	    cout << " ";
      }

      cout << endl;
	
      //output highest
      for(int i=0; i<hi.size(); i++) {
	 cout << hi[i];

	 if(i<hi.size()-1)
	    cout << " ";
      }

      cout << endl;
   }

   return 0;
}
