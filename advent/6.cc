#include <iostream>
using namespace std;

int main() {
	string action;

	long long grid[1000][1000] = {0};

	while(cin>>action) {
		if(action=="turn") {
			string t;
			cin>>t;
			action += " " + t;
		}

		int x1,y1,x2,y2;
		char ch;
		string bad;

		cin >> x1 >> ch >> y1 >> bad >> x2 >> ch >> y2;

		for(int i=x1; i<= x2; i++) {
			for(int j=y1; j<=y2; j++) {
				if(action=="turn on")
					grid[i][j]++;
				else if(action=="turn off")
					grid[i][j]--;
				else if(action=="toggle")
					grid[i][j]+=2;

				if(grid[i][j]<0)
					grid[i][j]=0;
			}
			}

	}

	long long sum=0;

	for(int i=0; i<1000; i++)
		for(int j=0; j<1000; j++)
				sum+=grid[i][j];

	cout << sum << endl;

	return 0;
}