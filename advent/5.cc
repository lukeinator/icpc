#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main() {
	string s;
	int c=0;

	while(cin>>s) {
		bool nice=true;

		bool hasPairs=false;

		for(int i=0; i<s.length()-1; i++) {
				string comp = "";
				comp += s[i];
				comp += s[i+1];

				string test = s;

				test.erase(i, 2);

				if(test.substr(0, i-1).find(comp)!=string::npos||test.substr(i, test.length()-i).find(comp)!=string::npos) {
					cout << test << " " << comp << endl;
					hasPairs=true;
				}

		}

		if(!hasPairs)
			nice=false;

		bool hasSand=false;

		for(int i=0; i<s.length()-2; i++)
			if(s[i]==s[i+2])
				hasSand=true;

		if(!hasSand)
			nice=false;

		if(nice) {
			cout << s << endl;
			c++;
		}
	}

	cout << c << endl;

	return 0;
}