#include <iostream>
#include <algorithm>
using namespace std;

int main() {
   long long sum=0;

   int sides[3];

   char ch;

   while(cin>>sides[0]>>ch>>sides[1]>>ch>>sides[2]) {
      sort(sides, sides+3);

      sum += 2*sides[0]*sides[1] +  2*sides[2]*sides[1] +  2*sides[0]*sides[2];
      sum += sides[0]*sides[1];

   }
   cout << sum << endl;

   return 0;
}
