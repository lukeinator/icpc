#include <iostream>
using namespace std;

long long sumDig(long long num) {
	long long sum=0;

	while(num>0) {
		sum+=num%10;
		num/=10;
	}

	return sum;
}



int main()  {
	long long num;

	while(cin>>num&&num!=0) {
		while(num/10>0)
			num = sumDig(num);

		cout << num << endl;	

	
	}
	return 0;
	
}