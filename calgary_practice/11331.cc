#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <utility>
using namespace std;

map< pair<int, int>, bool> cowseen;

bool dfs(int num, int color, int counts[2],
	vector< vector<int> >& connections, map<int, int>& seen) {

	if(seen.count(num)!=0&&seen[num]==color)
		return true;

	else if(seen.count(num)!=0&&seen[num]!=color) {
		return false;
	}

	seen[num] = color;
	counts[color]++;

	bool test=true;

	for(int i=0; i<connections[num].size(); i++) {
		if(!dfs(connections[num][i], 
			(color+1)%2, counts, connections, seen))
			test=false;
	}
	return test;
}

bool dp(int cows, int cowsgoal, int row, 
		vector< pair<int, int> >& fields) {
	if(cowseen.count(make_pair(cows, row))!=0)
		return cowseen[make_pair(cows, row)];

	if(row==fields.size()&&cows==cowsgoal)
		return true;

	else if(row==fields.size()&&cows!=cowsgoal) 
		return false;

	return cowseen[make_pair(cows, row)] = (dp(cows+fields[row].first, cowsgoal, row+1, fields)||
				dp(cows+fields[row].second, cowsgoal, row+1, fields));

}

int main() {

	int cases;
	cin>>cases;

	for(int a=0; a<cases; a++) {

	vector< pair<int, int> > fields;

	map<int, int> seen;

	int cow, bull, numfield;

	cin >> bull >> cow >> numfield;

	vector< vector<int> > connections(bull+cow);
	for(int i=0; i<numfield; i++) {
		int a, b;
		cin >> a >> b;
		connections[a-1].push_back(b-1);
		connections[b-1].push_back(a-1);
	}

	bool bipartite=true;

	for(int i=0; i<connections.size(); i++) {
		if(seen.count(i)!=0)
			continue;

		int counts[2] = {0};

		if(!dfs(i, 0, counts, connections, seen)) {
			bipartite=false;
			break;
		}

			fields.push_back(make_pair(counts[0], counts[1]));

	}

	if(bipartite==false)
		cout << "no" << endl;

	else if(dp(0, cow, 0, fields))
		cout << "yes" << endl;

	else
		cout << "no" << endl;

	cowseen.clear();
	}


	return 0;
}