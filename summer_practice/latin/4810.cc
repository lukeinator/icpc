#include <iostream>
#include <sstream>
#include <cctype>
using namespace std;

bool isTaut(string s) {
	istringstream iss(s);

	string test;

	iss>>test;

	char ch = tolower(test[0]);

	while(iss>>test)
		if(ch!=tolower(test[0]))
			return false;

	return true;
}


int main() {
	string s;

	while(getline(cin, s)&&s[0]!='*') {
		cout << (isTaut(s) ? "Y" : "N");
		cout << endl;	
	}

	return 0;
}