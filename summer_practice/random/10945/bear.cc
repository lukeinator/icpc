#include <iostream>
#include <string>
#include <cctype>
#include <algorithm>
using namespace std;

int main() {

	string s;
	
	while(getline(cin, s)&&s!="DONE") {
		string str = "";

		for(char ch : s) 
			if(isalpha(ch))
				str += tolower(ch);


		string rev = str;
		reverse(rev.begin(), rev.end());

		if(str == rev)
			cout << "You won't be eaten!" << endl;
		else
			cout << "Uh oh.." << endl;

		
	}

	return 0;
}