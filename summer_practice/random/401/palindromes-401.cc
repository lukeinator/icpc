#include <iostream>
#include <algorithm>



using namespace std;

int main() {
	string s;


	string key[2] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789", "A   3  HIL JM O   2TUVWXY51SE Z  8 "};

	while(cin>>s) {

		string rev = s;
		
		reverse(rev.begin(), rev.end());
		bool isPalindrome = (s == rev);

		string mir = s;

		for(int i=0; i<mir.length(); i++) {
			char rep = key[1][key[0].find(mir[i])];

			//if(rep != ' ')
				mir[i] = rep;
		}

		reverse(mir.begin(), mir.end());

		bool isMirrored = (s == mir);

		cout << s;

		if(isMirrored&&isPalindrome)
			cout << " -- is a mirrored palindrome.";
		else if(isMirrored)
			cout << " -- is a mirrored string.";
		else if(isPalindrome)
			cout << " -- is a regular palindrome.";
		else
			cout << " -- is not a palindrome.";

		cout << endl << endl;

	}



	return 0;
}