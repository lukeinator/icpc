#include <iostream>
using namespace std;

int main() {
	int cases;
	cin>>cases;

	int a=1;
	while(cases--) {
		int t[3];
		char ch;
		cin >> t[0] >> ch >> t[1] >> ch >> t[2];

		bool cl[3][6] = {0};

		for(int i=0; i<3; i++) {
			for(int j=0; j<6; j++)
				cl[i][j] = (t[i]&(1<<j));
		}

		cout << a++ << " ";

		for(int i=5; i>=0; i--)
			for(int j=0; j<3; j++)
				cout << cl[j][i];

		cout << " ";

		for(int i=0; i<3; i++)
			for(int j=5; j>=0; j--)
				cout << cl[i][j];

	cout << endl;


	}
	return 0;
}