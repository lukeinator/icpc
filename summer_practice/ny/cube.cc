#include <iostream>
#include <queue>
#include <tuple>
using namespace std;

int main() {
	int cases;
	cin>>cases;

	int z=1;
	while(cases--) {
		int cube, area;
		cin >> cube;
		area = 0;

		int map[105][105][105];

		for(int i=0; i<105; i++)
			for(int j=0; j<105; j++)
				for(int k=0; k<105; k++)
					map[i][j][k]=0;

		int s1, s2, s3;
		queue< tuple<int, int, int> > order;
		for(int i=0; i<cube; i++) {
			int a, b, c;
			char ch;
			cin >> a >> ch >> b >> ch >> c;
			
			if(i==0) {
			s1=a+1;
			s2=b+1;
			s3=c+1;
		}

			map[a+1][b+1][c+1]=1;
			order.push(make_tuple(a+1, b+1, c+1));

		}
		

		int moves[6][3] = {
			{1, 0, 0},
			{-1, 0, 0},
			{0, 1, 0},
			{0, -1, 0},
			{0, 0, 1},
			{0, 0, -1}
		};

		map[s1][s2][s3]=-1;

		queue< tuple<int, int, int> > q;

		q.push(make_tuple(s1, s2, s3));

		while(!q.empty()) {
			tuple<int, int, int> t = q.front();
			q.pop();

			bool connected=false;

			int a, b, c;

			a = get<0>(t);
			b = get<1>(t);
			c = get<2>(t);

			map[a][b][c]=-1;
			for(int i=0; i<6; i++) {
				if(map[a+moves[i][0]][b+moves[i][1]][c+moves[i][2]]==1) {
					connected=true;
					//area-=2;
					q.push(make_tuple(a+moves[i][0], b+moves[i][1], c+moves[i][2]));

				} else if(map[a+moves[i][0]][b+moves[i][1]][c+moves[i][2]]==-1) {
					//area-=2;
					connected=true;
				} else {
					area++;
				}
				

			}

				//if(!connected)
				//	break;
		}
		int counter=0;
		int pos=-1;
		for(int i=0; i<105; i++)
			for(int j=0; j<105; j++)
				for(int k=0; k<105; k++) {
					if(map[i][j][k]==-1)
						counter++;
					else if(map[i][j][k]==1) {
						pos = counter;
						break;
					}
				}

cout <<z++ << " ";
//cout << map[s1][s2][s3];

		if(pos==-1)
			cout << area << endl;
		else {
			int track=0;
			while(!order.empty()) {
				track++;
			tuple<int, int, int> t = order.front();
			order.pop();

			

			int a, b, c;

			a = get<0>(t);
			b = get<1>(t);
			c = get<2>(t);

			if(map[a][b][c]>0)
				break;

			}
			cout << "NO " << track << endl;
		}


	}


	return 0;
}