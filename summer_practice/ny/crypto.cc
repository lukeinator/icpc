#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main() {
	int cases;
	cin>>cases;

		string key;
		string msg;
		getline(cin, msg);
	
	int i=1;
	while(cases--) {
		cout << i++ << " ";
		
		getline(cin, msg);
		getline(cin, key);

		for(int j=0; j<msg.length(); j++) {
			if(msg[j]==' ')
				cout << ' ';

			else
				cout << key[msg[j]-'A'];
		}
		cout << endl;
	}


return 0;
}