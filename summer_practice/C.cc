#include <iostream>
#include <algorithm>
using namespace std;

int test[2000][2000];

long long oddOdd(int board[2000][2000], int n, int m) {
	long long sum=0;

	

	for(int i=0; i<n; i++) 
		for(int j=0; j<m; j++) 
			test[i][j] = board[i][j];
		

	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(test[i][j]!=0) {
				if(test[i][j]%2!=(i+1)%2)
					return -1;

				else if(i>0&&test[i][j]<test[i-1][j])
					return -1;

				else if(j>0&&test[i][j]<test[i][j-1])
					return -1;
				else {
					sum += test[i][j];
					continue;
				}
			
			} else {
				int next = 0;

				if(i>0)
					next = max(test[i-1][j], next);
				
				if(j>0)
					next = max(test[i][j-1], next);

				next++;

				if(next%2!=(i+1)%2)
					next++;

				test[i][j]=next;
				sum += next;
			}
		}
	}


	return sum;
}

long long oddEven(int board[2000][2000], int n, int m) {
	long long sum=0;

	

	for(int i=0; i<n; i++) 
		for(int j=0; j<m; j++) 
			test[i][j] = board[i][j];
		

	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(test[i][j]!=0) {
				if(test[i][j]%2!=(j+1)%2)
					return -1;
				else if(i>0&&test[i][j]<test[i-1][j])
					return -1;

				else if(j>0&&test[i][j]<test[i][j-1])
					return -1;
				else {
					sum += test[i][j];
					continue;
				}
			
			} else {
				int next = 0;

				if(i>0)
					next = max(test[i-1][j], next);
				
				if(j>0)
					next = max(test[i][j-1], next);

				next++;

				if(next%2!=(j+1)%2)
					next++;

				test[i][j]=next;
				sum += next;
			}
		}
	}



	return sum;
}

long long evenOdd(int board[2000][2000], int n, int m) {
	long long sum=0;

	

	for(int i=0; i<n; i++) 
		for(int j=0; j<m; j++) 
			test[i][j] = board[i][j];
		

	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(test[i][j]!=0) {
				if(test[i][j]%2!=(j)%2)
					return -1;
				else if(i>0&&test[i][j]<test[i-1][j])
					return -1;

				else if(j>0&&test[i][j]<test[i][j-1])
					return -1;
				else {
					sum += test[i][j];
					continue;
				}
			
			} else {
				int next = 0;

				if(i>0)
					next = max(test[i-1][j], next);
				
				if(j>0)
					next = max(test[i][j-1], next);

				next++;

				if(next%2!=(j)%2)
					next++;

				test[i][j]=next;
				sum += next;
			}
		}
	}


	return sum;
}

long long evenEven(int board[2000][2000], int n, int m) {
	long long sum=0;

	

	for(int i=0; i<n; i++) 
		for(int j=0; j<m; j++) 
			test[i][j] = board[i][j];
		

	for(int i=0; i<n; i++) {
		for(int j=0; j<m; j++) {
			if(test[i][j]!=0) {
				if(test[i][j]%2!=(i)%2)
					return -1;
				else if(i>0&&test[i][j]<test[i-1][j])
					return -1;

				else if(j>0&&test[i][j]<test[i][j-1])
					return -1;
				else {
					sum += test[i][j];
					continue;
				}
			
			} else {
				int next = 0;

				if(i>0)
					next = max(test[i-1][j], next);
				
				if(j>0)
					next = max(test[i][j-1], next);

				next++;

				if(next%2!=(i)%2)
					next++;

				test[i][j]=next;
				sum += next;
			}
		}
	}


	return sum;
}

long long calc(int board[2000][2000], int n, int m) {
	
	long long ans = 20000000000000;

	long long tests[4];

	tests[0] = evenEven(board, n, m);
	tests[1] = evenOdd(board, n, m);
	tests[2] = oddEven(board, n, m);
	tests[3] = oddOdd(board, n, m);

	bool seen=false;

	for(int i=0; i<4; i++) {
		if(tests[i]!=-1) {
			seen=true;
			ans = min(ans, tests[i]);

		}
				
	}
	
	if(!seen)
		return -1;

	return ans;


}

	int board[2000][2000];

int main() {

	int n, m;
	cin >> n >> m;


	for(int i=0; i<n; i++)
		for(int j=0; j<m; j++)
			cin >> board[i][j];


	cout << calc(board, n, m) << endl;


	return 0;
}