#include <iostream>
using namespace std;

int main() {
	int cases;
	cin>>cases;
	while(cases--) {
		int n, m, x;
		cin>>n>>m>>x;

		if(n%m==x%m)
			cout << "YES" << endl;
		else
			cout <<"NO" <<endl;

	}
	return 0;
}