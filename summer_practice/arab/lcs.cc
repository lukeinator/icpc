#include <iostream>
#include <cassert>
using namespace std;

long long gcd(long long a, long long b)
{
  long long r;

  /* unnecessary if a, b >= 0 */
  if (a < 0) {
    a = -a;
  }
  if (b < 0) {
    b = -b;
  }

  while (b) {
    r = a % b;
    a = b;
    b = r;
  }
  assert(a >= 0);
  return a;
}

int main() {
	int cases;
	cin>>cases;

	while(cases--) {

		long long n1, f1, d1, n2, f2, d2;

		cin >> n1 >> f1 >> d1 >> n2 >> f2 >> d2;

		long long lcm = d1*d2/gcd(d1, d2);
		long long flcm = f1*f2/gcd(f1, f2);

		long long ans = min((n2*d2+f2), (n1*d1+f1))/lcm;
		
		ans -= (flcm-1)/lcm;

		cout<<  ans << endl;


	}


	return 0;
}