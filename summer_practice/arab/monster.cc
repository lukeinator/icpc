#include <iostream>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

int monster, cost;
int mmax, cmax;

set< tuple<int, int, int, int, int> >seen;

void calc(int players[5][3]) {
	if(monster <= 0) {
		if(cost<cmax) {
			cmax=cost;
		}

		if(cmax==cost)
			mmax=max(monster, mmax);

		return;
	}

	bool has=false;

	for(int i=0; i<5; i++)
		if(players[i][0]>0)
			has=true;

	if(!has)
		return;

	for(int i=0; i<5; i++) {
		if(players[i][0]>0) {
		
			monster -= players[i][1];
			cost += players[i][2];
			
			

			if(seen.count(make_tuple(players[0][0], players[1][0], players[2][0], players[3][0], players[4][0]))!=0)
				continue;

			seen.insert(make_tuple(players[0][0], players[1][0], players[2][0], players[3][0], players[4][0]));

			//seen.insert(players[i][0]);
			players[i][0]--;

			calc(players);

			monster += players[i][1];
			cost -= players[i][2];
			players[i][0]++;
		}

	}
}

int main() {
	int players [5][3];


	int cases;
	cin>>cases;
	while(cases--) {
		cost=0;
		cmax = 1000000000;
	    mmax = 0;

	    cin >> monster;

	    for(int i=0; i<5; i++)
	    	for(int j=0;j<3; j++)
	    		cin >> players[i][j];

	   calc(players);

	   cout  << cmax << " " << mmax << endl;

	}



	return 0;
}