#include <iostream>
#include <algorithm>
using namespace std;

int map[1005][1005];
int sums[1005][1005];

int main() {
	int cases;
	cin>>cases;
	while(cases--) {

		int n, m;
		cin>> n >> m;

		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++) {
				cin>>map[i][j];
			}
		}

		for(int i=n-1; i>=0; i--)
			for(int j=m-1; j>=0; j--) {
				if(i<n-1)
				map[i][j] += map[i+1][j];
		}

		for(int i=n-1; i>=0; i--)
			for(int j=m-1; j>=0; j--) {
				if(j<m-1)
				map[i][j] += map[i][j+1];
		}

		int maximum=-10000000;

		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++) {
				maximum =  max(maximum, map[i][j]);
			}
		}

		cout << maximum << endl;

	}

	return 0;
}