#include <iostream>
#include <cctype>
#include <algorithm>
using namespace std;
int exponent(int a) {
	int b=1;
	for(int i=0; i<a; i++)
		b= (b*2)%1000000007;
	return b;
}
int main() {
	int cases;
	cin>>cases;

	while(cases--) {
		string s;
		cin>>s;

		bool seen[27]={false};

		long long c1=1, c2=1;

		bool start=false;
		for(auto ch : s) {

			if(isalpha(ch)&&!start)
				start=true;

			if(isalpha(ch)&&!seen[ch-'a']) {
				seen[ch-'a']=true;
				c1= ((c1%1000000007)*(c2%1000000007))%1000000007;
				c2=1;
				continue;
			}

			if(isalpha(ch)&&seen[ch-'a'])
				c2=1;

			if(ch=='?'&&start)
				c2++;

		}
		cout << c1 << endl;
	}


	return 0;
}