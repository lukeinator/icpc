#include <iostream>
#include <algorithm>
using namespace std;

int findFirstGreaterThanOrEqual(int array[], int N, int X) {
int start = 0, end = N;
while (start < end) {
int middle = (start + end) / 2;
if (array[middle] > X) {
end = middle;
} else {
start = middle + 1;
}
}
return start;
}

int main() {
	int cases;
	cin>>cases;

	while(cases--) {
		int nums[100];

		for(int i=0; i<100; i++)
			nums[i]=i+1;

		int n, x, y;
		cin>>n>>x>>y;

		int pos=n-1;

		if(y==1) {
			while(findFirstGreaterThanOrEqual(nums, n, x)!=(lower_bound(nums, nums+n, x))-nums) {
				if(nums[pos]<x)
					nums[pos]=x-1;

				if(nums[pos]<9)
					nums[pos]++;
				else {
					pos--;
				}
			}

		} else if(y==2) {
			while(findFirstGreaterThanOrEqual(nums, n, x)==(lower_bound(nums, nums+n, x)-nums)) {
				if(nums[pos]<x)
					nums[pos]=x-1;

				if(nums[pos]<9)
					nums[pos]++;
				else {
					pos--;
				}
			}
		}
	
	for(int i=0; i<n; i++)
		cout << nums[i] << " ";

	cout << endl;

	}

	return 0;
}