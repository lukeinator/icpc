#include <iostream>

using namespace std;

int main() {
	int cases;
	cin>>cases;
	while(cases--) {
		int n, x, y;
		cin>>n>>x>>y;

		bool s=false, e=false;

		int num;
		cin>>num;

		s=(!(num==x));

		for(int i=0; i<n-1; i++)
			cin>>num;

		e=(!(num==y));

		if(s&&e)
			cout << "OKAY" <<endl;

		else if(!s&&!e)
			cout << "BOTH" << endl;

		else if(!s)
			cout << "EASY" << endl;
		else if(!e)
			cout << "HARD" << endl;

	}

	return 0;
}