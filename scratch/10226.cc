#include <iostream>
#include <map>
#include <iomanip>
using namespace std;

int main() {

	int cases;
	cin>>cases;
	string s;
	getline(cin, s);
	getline(cin, s);
	while(cases--) {
		map<string, int> species;
		string name;

		int count=0;
		while(getline(cin, name)&&name!="") {
			species[name]++;
			count++;
		}

		for(auto s : species)
			cout << fixed << setprecision(4) << s.first << " " << ((s.second*1.0)/count)*100 << endl;
		
		if(cases!=0)
			cout << endl;
	}


	return 0;
}