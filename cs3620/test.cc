

#include <iostream>
#include <iomanip>
#include "astack.h"

const std::string _end_screen = "--------------------------------------------------------------------------------\n";

int main()
{

   std::cout << "Test By: Emaadeddin Zaamout\nID:001163400\nAssignment 2 "
	     << "Dr.Shadat\n" << _end_screen;

   AStack _test(1000000);	


   //testcase 1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   std::cout << "                                   Testcase 1\n"
	     << "Objective: Test's basic 20 strings pushing and popping. On "
	     << "the Stack. String\nsizes are less than 10. The length of the"
	     << "stack is considered for each push pop\n";

   _test.push("HI");
   _test.push("HOW");
   _test.push("ARE");
   _test.push("YOU");
   _test.push("DOING");
   _test.push("TODAY?");
   _test.push("IT");
   _test.push("WAS");
   _test.push("SNOWING");
   _test.push("OUTSIDE");
   _test.push("TODAY!");
   _test.push("IT");
   _test.push("WAS");
   _test.push("COLD.");
   _test.push("I");
   _test.push("GOT");
   _test.push("AN");
   _test.push("EXAM");
   _test.push("TOMMOROW!");
   _test.push("OKAY");
   _test.push("BYE NOW!");

   for (size_t _i = 0; _i < 20; _i++)
   {
      std::cout << "stack length: " << std::setw(4) <<  _test.length();
      std::string _mystr = _test.pop();
      std::cout  << "  " << "string length: "  << std::setw(4) << _mystr.length() << "   " 
		 << "string: " << _mystr  << std::endl;

   }
   std::cout << _end_screen;

   //testcase 2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   std::cout << "                                   Testcase 1\n"
	     << "testing very big string of size 255\n";

   _test.push("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdeabcdefghijabcdefghijabcdefghijabcdefghijabcdefghij");
   std::cout << "stack length: " << std::setw(4) <<  _test.length();
   std::string _mystr = _test.pop();
   std::cout  << "  " << "string length: "  << std::setw(4) << _mystr.length() << "   " 
	      << "string: " << _mystr  << std::endl;




   std::cout << _end_screen;




   return 0;
}
