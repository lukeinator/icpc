//main.cc
//
//Lukas Grasse
//April 8, 2015
//
//main program used to test sliding solver and board tile classes
//by querying user for start and goal of 3x3 sliding puzzles
//and solving them

//includes
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <string>
#include <queue>
#include <fstream>
using namespace std;






int main() {
	string aa, bb;

	ifstream fin("data");

	while(fin>>aa>>bb) {

	int sum=0;
	for(int i=0; i<aa.length(); i++)
		for(int j=i+1; j<aa.length(); j++)
			if(aa[j]<aa[i])
				sum++;


	size_t e = aa.find('0');
	e = e/3;

	if((e+sum)%2!=0)
		continue;
	
	queue<int[10]> boards;

	for(int i=0; i<aa.length(); )

	boards.push(a);

	set<string> seen;

	map<string, string> moves;

	while(!boards.empty()) {


	string board = boards.front();
	boards.pop();

	if(seen.count(board)!=0)
		continue;

	seen.insert(board);

	if(board==b) {
		cout << a << " " << moves[board] << endl;
		break;
	}


	//find where empty spot is
	size_t loc = board.find('0');

	//if we can, generate string where empty
	//space is moved up
	if(loc>2) {
		string s = board;
		swap(s[loc], s[loc-3]);
		moves[s] = moves[board] + "U";
		boards.push(s);
	}

	//down
	//if we can, generate string where empty
	//space is moved down
	if(loc<6) {
		string s = board;
		swap(s[loc], s[loc+3]);
		moves[s] = moves[board] + "D";
		boards.push(s);
	}

	//left
	//if we can, generate string where empty
	//space is moved left
	if(loc%3>0) {
		string s = board;
		swap(s[loc], s[loc-1]);
		moves[s] = moves[board] + "L";
		boards.push(s);
	}


	//right
	//if we can, generate string where empty
	//space is moved right
	if(loc%3<2) {
		string s = board;
		swap(s[loc], s[loc+1]);
		moves[s] = moves[board] + "R";
		boards.push(s);
	}

	}


	}

	fin.close();
	return 0;
}