//BoardTile Implementation
//
//Lukas Grasse
//April 8, 2015
//
//BoardTile represents a Board state. 
//This class is used by SlidingSolver.


//includes
#include "Board_Tile.h"

#include <cstdlib>
using std::abs;

#include <algorithm>
using std::swap;

//Constructor
//
//Takes parameter for board string that defaults to
//123456780
BoardTile::BoardTile(string board) : board(board) {

}

//getNextBoards
//
//returns a vector of BoardTiles that contains 
//BoardTiles that can be reached from itself
vector<BoardTile> BoardTile::getNextBoards() {
	
	//vector of boards to return
	vector<BoardTile> boards;

	//find where empty spot is
	size_t loc = board.find('0');
	
	//up
	//if we can, generate string where empty
	//space is moved up
	if(loc>2) {
		string s = board;
		swap(s[loc], s[loc-3]);

		BoardTile up(s);
		up.setMoves(moves+'U');
		boards.push_back(up);
	}

	//down
	//if we can, generate string where empty
	//space is moved down
	if(loc<6) {
		string s = board;
		swap(s[loc], s[loc+3]);

		BoardTile down(s);
		down.setMoves(moves+'D');
		boards.push_back(down);
	}

	//left
	//if we can, generate string where empty
	//space is moved left
	if(loc%3>0) {
		string s = board;
		swap(s[loc], s[loc-1]);

		BoardTile left(s);
		left.setMoves(moves+'L');
		boards.push_back(left);
	}


	//right
	//if we can, generate string where empty
	//space is moved right
	if(loc%3<2) {
		string s = board;
		swap(s[loc], s[loc+1]);

		BoardTile right(s);
		right.setMoves(moves+'R');
		boards.push_back(right);
	}

	return boards;
}


//getCount
//
//returns the number of moves board is from start
int BoardTile::getCount() const {
	if(moves=="none")
		return 0;
	
	return moves.length();
}

//getManhattanDist
//
//returns estimated ManhattanDist of itself to 
//parameter string s
int BoardTile::getManhattanDist(string s) const {
	//destination BoardTile
	BoardTile b(s);

	//sum of manhattan dist
	int sum=0;

	//get source and destination strings
	string src = this->getBoard(); 
	string dest = b.getBoard();

	//sum up manhattan distance of each number to 
	//its correct location
	for(int i=0; i<9; i++) {
		
		//skip empty space
		if(src[i]=='0')
			continue;

		//find location of correct position of i
		int dPos = dest.find(src[i]);

		//sum up horizontal difference + vertical difference
		sum += abs(i/3 - dPos/3) + abs(i%3-dPos%3);
	}

	return sum;
}

//getBoard
//
//returns board as a string
string BoardTile::getBoard() const {
	return board;
}

//getMoves
//
//returns moves as a string
string BoardTile::getMoves() const {
	return moves;
}

//setMoves
//
//sets moves string value to parameter string m
void BoardTile::setMoves(string m) {
	moves = m;
}

//less than comparison operator
bool BoardTile::operator <(const BoardTile &b) const {
	return board<b.getBoard();
}