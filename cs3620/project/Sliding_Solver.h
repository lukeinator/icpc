//Sliding Solver Header
//
//Lukas Grasse
//April 8, 2015
//
//Sliding Solver takes a starting string in the constructor.
//It has a member function that will then solve the puzzle to any
//destination string passed to the function. 
//

//includes
#include "Board_Tile.h"

#include <set>
using std::set;

#include <string>
using std::string;

//macro guard
#ifndef SLIDING_SOLVER
#define SLIDING_SOLVER

//class
class SlidingSolver {

public:
	//constructor for SlidingSolver
	SlidingSolver(string s);

	//solvePuzzle function
	//
	//takes solution string and returns a BoardTile representing the
	//solution. If no solution is found, the BoardTile at the 
	//starting position is returned.
	BoardTile solvePuzzle(string s);

private:
	//starting position string
	string start;

	//keep track of states we've seen
	set<string> seen;

};

#endif