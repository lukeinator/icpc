//BoardTile Header
//
//Lukas Grasse
//April 8, 2015
//
//BoardTile represents a Board state. 
//This class is used by SlidingSolver.


//includes
#include <string>
using std::string;

#include <vector>
using std::vector;

#ifndef BOARD_TILE
#define BOARD_TILE


class BoardTile {

public:
	//Constructor
	//
	//Takes parameter for board string that defaults to
	//123456780
	BoardTile(string board="123456780");

	//getNextBoards
	//
	//returns a vector of BoardTiles that contains 
	//BoardTiles that can be reached from itself
	vector<BoardTile> getNextBoards();

	//getCount
	//
	//returns the number of moves board is from start
	int getCount() const;

	//getManhattanDist
	//
	//returns estimated ManhattanDist of itself to 
	//parameter string s
	int getManhattanDist(string s) const;

	//getBoard
	//
	//returns board as a string
	string getBoard() const;

	//getMoves
	//
	//returns moves as a string
	string getMoves() const;

	//setMoves
	//
	//sets moves string value to parameter string m
	void setMoves(string m);

	//less than comparison operator
	bool operator <(const BoardTile &b) const;

private:
	//board string
	string board;

	//moves string
	string moves;

};

#endif