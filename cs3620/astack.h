//Lukas Grasse - Feb 2015                                                       
//id: 001172543
//
//cspc 3620 Assignment 2
//charStack - question 6
//
//Assert function and charStack general idea from:
//     Data Structures and Algorithm Analysis
//          by Clifford A. Shaffer
#include <iostream>

//for strncmp
#include <string>
using namespace std;

//default size for charStack
#define defaultSize 255




//charStack class. A stack of strings stored in 
//an array
class AStack {


public:
  // Constructor
  //
  //Constructor for charStack class
  AStack(int size = defaultSize) { 
    maxSize = size; 
    top = 0; 
    strCount = 0;
    listArray = new char[size]; 
  }

  // Destructor
  //
  //Destructor for charStack class
  ~AStack() {
    delete [] listArray;
  } 
  
  // Reinitialize
  //
  //Reinitializes stack
  void clear() { 
    top = 0; 
    strCount = 0;
  }          
  
  //push
  //
  // Put "it" string on stack
  void push(const char it[]) {         

    
    //size of string to insert
    int size=0;
    char ch;

    //keep reading c-str
    while(it[size]!='\0') {
      //insert string and increment top
      listArray[top++] = it[size];
      
      //increment size
      size++;
    }

    //store size of c-str in array
    listArray[top++] = size;

    strCount++;
  }

  //pop
  //
  // Pop top string
  char* pop() {                

    
    //c-str to return
    char* str = new char[256];
    //size of str
    int size=0;

    //beginning of str in array
    int start = top-listArray[top-1]-1;
    
    //copy chars from array to string
    for(int i=start; i<top-1; i++) {
      str[size++] = listArray[i];
    }
 
    //move top to new position
    top -= size+1;
   
    //add null char to c-str
    str[size++] = '\0';

    strCount--;

    return str;
  }

  //topValue
  //
  // Return top string
  const char* topValue() const {     
    //c-str to return
    char* str = new char[256];
    //size of str
    int size=0;

    //beginning of str in array
    int start = top-listArray[top-1]-1;
    
    //copy chars from array to string
    for(int i=start; i<top-1; i++) {
      str[size++] = listArray[i];
    }

    //add null char for c-str
    str[size++] = '\0';

    return str;
  }

  //length
  //
  // Return number of strings in stack
  int length() const { 
    return strCount; 
  }  


  private:
    // Maximum size of stack
  int maxSize;

  //top position
  int top;

  //number of strings in array
  int strCount;

  // Array holding stack strings
 char *listArray;

};

/*

int main() {

  //create chstack
  charStack chstack;

  cout << "Tests:" << endl;

  //push some elements
  chstack.push("abc");
  chstack.push("hello");
  chstack.push("world");

  //test pop
  char* testPop = chstack.pop();
  Assert(strncmp(testPop, "world", 5)==0, "pop failed"); 
  cout << "1) pop works" << endl << endl;
  delete [] testPop;

  //test topValue
  const char* testTopValue = chstack.topValue();
  Assert(strncmp(testTopValue, "hello", 5)==0, "topValue failed"); 
  cout << "2) topValue works" << endl << endl;
  delete [] testTopValue;

  //test length
  Assert(chstack.length()==2, "length failed"); 
  cout << "3) length works" << endl << endl;

  //test pop again
  char* testPop2 = chstack.pop();
  Assert(strncmp(testPop2, "hello", 5)==0, "pop failed"); 
  cout << "4) pop still works" << endl << endl;
  delete [] testPop2;

  //test top again
  const char* testTopValue2 = chstack.topValue();
  Assert(strncmp(testTopValue2, "abc", 3)==0, "topValue failed"); 
  cout << "5) topValue still works" << endl << endl;
  delete [] testTopValue2;

  //test clear
  chstack.clear();
  Assert(chstack.length()==0, "clear failed"); 
  cout << "6) clear works" << endl << endl;

  //test push again
  chstack.push("123");
  const char* testPush = chstack.topValue();
  Assert(strncmp(testPush, "123", 3)==0, "push failed"); 
  cout << "7) push still works" << endl << endl;
  delete [] testPush;

  //test size again
  Assert(chstack.length()==1, "size wrong"); 
  cout << "8) size still right" << endl;

  return 0;
}
*/
