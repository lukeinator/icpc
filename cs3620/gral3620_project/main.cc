//main.cc
//
//Lukas Grasse
//April 8, 2015
//
//main program used to test sliding solver and board tile classes
//by querying user for start and goal of 3x3 sliding puzzles
//and solving them

//includes
#include <iostream>
using namespace std;

#include "Board_Tile.h"
#include "Sliding_Solver.h"

using namespace std;

//read start and goal strings from user
bool inputs(string& a, string&b) {
	cout << "Please enter a Start Board (empty line to quit): " << endl;
	
	if(!getline(cin, a)||a.length()!=9)
		return false;

	cout << endl << "Please enter a Goal Board: " << endl; 
	if(!getline(cin, b)||b.length()!=9)
		return false;

	return true;
}

int main() {
	string a, b;



	while(inputs(a, b)) {
	
	SlidingSolver solver(a);
	BoardTile answer = solver.solvePuzzle(b);

	cout  << endl << "----------------------------" << endl;
	cout << "Start Board:" << endl;
	cout << a << endl << endl;

	cout << "Goal Board:" << endl;
	cout << b << endl << endl;

	cout << "Number of Moves:" << endl;
	cout << answer.getCount() << endl << endl;

	cout << "Solution:" << endl;
	cout << answer.getMoves() << endl;
	cout << "----------------------------" << endl  << endl;

	}

	return 0;
}