//Sliding Solver Implementation
//
//Lukas Grasse
//April 8, 2015
//
//Sliding Solver takes a starting string in the constructor.
//It has a member function that will then solve the puzzle to any
//destination string passed to the function. 
//
//Resources used:
//http://stackoverflow.com/questions/20826078/priority-queue-comparison

//includes
#include "Sliding_Solver.h"

#include <utility>
using std::pair;

#include <queue>          
using std::priority_queue;

#include <vector>         
using std::vector;

#include <utility>
using std::make_pair;


//typedef for comparison function for priority queue
typedef bool (*comp)(pair<int, BoardTile> a,pair<int, BoardTile> b);

//comparison operator for priority queue
bool compare(pair<int, BoardTile> a,pair<int, BoardTile> b)
{
   return (a.first>b.first);
}

//constructor for SlidingSolver
SlidingSolver::SlidingSolver(string s) : start(s) {

}

//solvePuzzle function
//
//takes solution string and returns a BoardTile representing the
//solution. If no solution is found, the BoardTile at the 
//starting position is returned.
BoardTile SlidingSolver::solvePuzzle(string sol) {

	//start out with empty seen
	seen.clear();

	//starting position board
	BoardTile startBoard(start);

	//priority queue made of a pair of int and BoardTile
	//the priority queue comparison is made on the int
	priority_queue< pair<int, BoardTile>, 
					vector< pair<int, BoardTile> >, 
					comp> boards(compare);

	//estimate distance from start to solution
	int startDist = startBoard.getCount() 
				+ startBoard.getManhattanDist(sol);

	
	//push starting board
	boards.push(make_pair(startDist, startBoard));


	while(!boards.empty()) {
		
		//get current element
		BoardTile curr = boards.top().second;
		boards.pop();

		//if we've seen it skip to next
		if(seen.count(curr.getBoard())!=0) 
			continue;
		
		//mark as seen
		seen.insert(curr.getBoard());

		//if we've found solution return
		if(curr.getBoard()==sol)
			return curr;

		//get next boards
		vector<BoardTile> next = curr.getNextBoards();

		//add states that can be reached from curr
		for(unsigned int i=0; i<next.size(); i++) {
			
			//estimate distance from state to solution
			startDist = curr.getCount() + next[i].getManhattanDist(sol);
			
			//insert into queue
			boards.push(make_pair(startDist, next[i]));
		}
	
	}

	//if no solution was found return start.
	startBoard.setMoves("none");
	return startBoard;
}