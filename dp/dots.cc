#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <algorithm>
using namespace std;

int n;
vector< pair<int, int> > clothes;
int seen[10005][105];

int calc(int pos, int remW) {
	if(seen[pos][remW]!=-1)
		return seen[pos][remW];

	if(remW==0)
		return seen[pos][remW] = 0;

	if(pos==n)
		return seen[pos][remW] = 0;

	if(clothes[pos].first>remW)
		return seen[pos][remW] = calc(pos+1, remW);

	return seen[pos][remW] = max(calc(pos+1, remW), clothes[pos].second + calc(pos+1, remW-clothes[pos].first));
}

int main() {

	int m;
	while(cin>>m>>n) {
		clothes.clear();
		
		for(int i=0; i<10005; i++)
			for(int j=0; j<105; j++)
				seen[i][j]=-1;

		int i=n;
		while(i--) {
			int a, b;
			cin>>a>>b;

			clothes.push_back(make_pair(a, b));
		}

		if(m>2000)
			m+=200;
		cout << calc(0, m) << endl;



	}

	return 0;
}