#include <iostream>
#include <map>
#include <string>
using namespace std;
typedef long long ll;

map<string, ll> seen;


ll compute(ll change, ll max) {

if(change==0) {
	return 1;

}
else if(change<0) {
	return 0;

} 


ll num=0;



if(max>=50) { 
	string s = to_string(change-50) + " 50";
	if(seen.count(s)==0)
		seen[s] = compute(change-50, 50);
	num += seen[s];
}
if(max>=25) { 
	string s = to_string(change-25) + " 25";
	if(seen.count(s)==0)
		seen[s] = compute(change-25, 25);
	num += seen[s];
 }
 if(max>=10) { 
	string s = to_string(change-10) + " 10";
	if(seen.count(s)==0)
		seen[s] = compute(change-10, 10);
	num += seen[s];
}
if(max>=5) { 
	string s = to_string(change-5) + " 5";
	if(seen.count(s)==0)
		seen[s] = compute(change-5, 5);
	num += seen[s];
}
if(max>=1) { 	 
	string s = to_string(change-1) + " 1";
	if(seen.count(s)==0)
		seen[s] = compute(change-1, 1);
	num += seen[s];
}


return num;
}

int main() {
ll num;

while(cin>>num) {


ll ans = compute(num, 50);

if(ans==1)
	cout << "There is only 1 way to produce " << num << " cents change." << endl;

else
	cout << "There are " << ans << " ways to produce " << num << " cents change." << endl; 


}


	return 0;
}