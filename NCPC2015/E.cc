#include <iostream>
#include <queue>
#include <utility>
#include <algorithm>
using namespace std;

typedef pair<int, int> pii;


int main() {

   int n, k;
   cin >> n >> k;

   pii shows[100005];

   for(int i=0; i<n; i++) {
      int x, y;
      cin >> x >> y;

      shows[i] = pii(y, x);
   }

   queue< pii > tr;

   int count=0;

   sort(shows, shows+n);


   for(int i=0; i<k; i++)
      tr.push(shows[i]);
   
   for(int i=k; i<n; i++) {
      if(tr.size()!=0&&tr.front().first<=shows[i].second) {
	 tr.pop();
	 count++;
      }
      
      if(tr.size()<k) {
	 tr.push(shows[i]);
      }

   }

   cout << count+tr.size() << endl;

   return 0;
}
