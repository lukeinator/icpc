#include <iostream>
#include <utility>
#include <algorithm>
using namespace std;

typedef pair<int,int> pii;

int main() {
   int g;
   cin >> g;
   pii goblins[100005];

   for(int i=0; i<g; i++) {
      int x, y;
      cin>>x>>y;
      goblins[i]=pii(x,y);
   }

   int m;
   cin>>m;

   pii sprinklers[20005];
   int sRad[20005];
   for(int i=0; i<m; i++) {
      int x, y;
      cin>>x>>y;
      sprinklers[i] = pii(x, y);
      cin>>sRad[i];
   }

   bool seen[100005]={false};
   
   sort(goblins, goblins+g);

   for(int i=0; i<m; i++) {
      int left = sprinklers[i].first-sRad[i];
      int right = sprinklers[i].first+sRad[i];

      for(int j = left; j<=right; j++) {
	 auto bottom = lower_bound(goblins, goblins+g, pii(j, sprinklers[i].second-sRad[i]));
	 auto top = upper_bound(goblins, goblins+g, pii(j, sprinklers[i].second+sRad[i]));

	 for(auto k = bottom; k!=top; k++) {
	    if((k->first-sprinklers[i].first)*(k->first-sprinklers[i].first)+
	       (k->second-sprinklers[i].second)*(k->second-sprinklers[i].second)<=sRad[i]*sRad[i])
	       seen[(k-goblins)]=true;
	 }
	 
      }
   }

   int sum=0;
   for(int i=0; i<g; i++)
      if(seen[i]==false)
	 sum++;

   cout << sum << endl;
   return 0;
}
