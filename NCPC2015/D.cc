#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int main() {
   int n, k;
   cin >> n >> k;

   int ns[100000];

   for(int i=0; i<n; i++)
      cin >> ns[i];

   vector<int> nums(ns, ns+n);
   
   int t=1;
   for(int i=0; i<n; i++) {
      int req = (lower_bound(nums.begin()+i, nums.end(), nums[i]+1000)-(nums.begin()+i));

      int needed;

      if(req%k==0)
	 needed=req/k;
      else
	 needed=req/k+1;
      t=max(t, needed);
   }

   cout << t << endl;
   return 0;
}
